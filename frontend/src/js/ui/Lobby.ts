
class Lobby extends Controller {

	visible: boolean = true;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.listen(["game.start", "game.close"]);
	}

	update(event?: Event): void {
		if (event && event.type === "game.start") {
			this.visible = false;
		}
		if (event && event.type === "game.close") {
			this.visible = true;
		}

		this.dom.showThen(this.visible);
	}

	toString(): string {
		return "Lobby()";
	}

}