
class GameStatus extends Controller {

	kills: number;
	blocks: number;
	towers: number;
	mined: number;
	score: number;
	rating: number;
	totalPlayers: number;

	gsMinedBlock: HTMLElement;
	gsTowersBlock: HTMLElement;
	gsKillsBlock: HTMLElement;
	gsBlocksBlock: HTMLElement;
	gsScoreBlock: HTMLElement;
	gsRankBlock: HTMLElement;

	gsMined: HTMLElement;
	gsTowers: HTMLElement;
	gsKills: HTMLElement;
	gsBlocks: HTMLElement;
	gsScore: HTMLElement;
	gsRank: HTMLElement;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.clear();
		this.listen(["game.score.capture", "game.score.towerCapture", "game.score.mineCapture", "game.score.kill", "game.score.rating", "game.close", "game.start"]);
	}

	clear() {
		this.kills = 0;
		this.blocks = 0;
		this.towers = 0;
		this.mined = 0;
		this.score = 0;
		this.rating = 0;
		this.totalPlayers = 0;
	}

	update(event?: Event): void {
		if (event && event.type === "game.score.capture") {
			var value = event["score"];
			this.blocks += value;
			this.score += value;
		}
		if (event && event.type === "game.score.towerCapture") {
			var value = event["score"];
			this.towers += 1;
			this.score += value;
		}
		if (event && event.type === "game.score.mineCapture") {
			var value = event["score"];
			console.log("captured!", value);
			this.mined += value;
		}
		if (event && event.type === "game.score.kill") {
			this.kills += 1;
			this.score += event["score"];
		}
		if (event && event.type === "game.score.rating") {
			this.rating = event["rating"];
			this.totalPlayers = event["totalPlayers"];
		}
		if (event && event.type === "game.close") {
			this.clear();
		}
		this.render();
	}

	render() {
		var game = this.parent as Game;
		var isBase = game.gameType === "base";
		var isOriginal = game.gameType === "original";
		var isTowers = game.gameType === "towers";
		var isMines = game.gameType === "mines";
		var isPvp = game.gameType === "pvp";

		this.gsMinedBlock.showThen(isMines);
		this.gsTowersBlock.showThen(isTowers);
		this.gsKillsBlock.showThen(isBase || isOriginal || isTowers || isMines);
		this.gsBlocksBlock.showThen(isBase || isTowers || isMines);
		this.gsScoreBlock.showThen(isBase || isTowers  || isMines);
		this.gsRankBlock.showThen(isBase || isOriginal || isTowers  || isMines);

		this.gsMined.innerHTML = this.mined.toString();
		this.gsTowers.innerHTML = this.towers.toString();
		this.gsKills.innerHTML = this.kills.toString();
		this.gsBlocks.innerHTML = this.blocks.toString();
		this.gsScore.innerHTML = this.score.toString();
		this.gsRank.innerHTML = this.rating + " / " + this.totalPlayers;
	}

}