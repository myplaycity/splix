
class Game extends Controller {

	login: Login = null;

	visible: boolean = false;
	renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer = null;
	tickTimer: Timer = null;
	inputTimer: Timer = null;

	client: Client = null;
	screen: GameScreen = null;
	scene: Scene = null;
	camera: Camera = null;
	player: Player = null;
	players: Players = null;
	towers: Towers = null;
	field: SplixField = null;

	gameTop: HTMLElement;
	gameMinimap: HTMLElement;
	gameSystemStat: HTMLElement;
	gameType: string;

	static colors: number = 100;

	viewQuadPadding: number;

	static logger = new Logger("Game");

	static keyCodes = {
		left: 37,
		right: 39,
		up: 38,
		down: 40
	};

	static cellSize = 50;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.onKeyDown = this.onKeyDown.bind(this);
		this.on("game.start", this.onStart);
		this.on("game.close", this.onClose);
		this.on("game.gameover", this.onGameOver);
	}

	update(event?: Event): void {
		this.dom.showThen(this.visible);
	}

	onStart() {
		Game.logger.info("starting game");
		this.visible = true;
		this.login = event.source;
		setTimeout(this.prepareStart.bind(this), 50);
	}

	onClose() {
		Game.logger.info("closing game");
		this.releaseResources();
		this.visible = false;
		this.login = null;
	}

	onGameOver() {
		Game.logger.info("gameover");
		this.releaseResources();
	}

	close() {
		this.dispatch("game.close");
	}

	toString(): string {
		return "Game()";
	}

	prepareStart() {
		Game.logger.info("prepare start game");
		this.gameType = this.login.gameType;
		SplixApi.getBestServer(this.login.gameType, this.connectClient.bind(this));
	}

	connectClient(server: GameserverData) {
		Game.logger.info("best server is", server.toString());
		this.client = new Client(this, server);
		this.createRenderer();
	}

	prepareScene(data: ServerData.Handshake) {
		Game.logger.info("prepare scene");

		this.scene = new Scene(this);
		this.players = new Players(this);
		this.towers = new Towers(this);
		this.field = new SplixField(this, data.sizeX, data.sizeY, data.quadSize);
		this.camera = new Camera(this);

		var gameMinimap = this.gameMinimap.__controller as GameMinimap;
		gameMinimap.setup(this.field.quadsX, this.field.quadsY);

		this.viewQuadPadding = data.viewQuadPadding;
	}

	prepareSetupPlayer(data: ServerData.SetupPlayer) {
		Game.logger.info("prepare setup player");

		var pos = new Vector(data.posX * Game.cellSize, data.posY * Game.cellSize);
		var dir = new Vector(data.dirX, data.dirY);

		this.player = new Player(this, data.playerId, data.color, data.nick, pos, dir);
		this.players.add(this.player);

		this.camera.startup(this.player.position);
		this.bindEvents();
		this.tickTimer = Timer.startRAF(this.tick.bind(this));
		this.inputTimer = Timer.startFPS(10, this.sendPlayerInput.bind(this));
	}

	createRenderer() {
		var size = this.dom.getSize();
		this.screen = new GameScreen(size.width, size.height);

		Game.logger.info("create renderer %s x %s", size.width, size.height);
		this.renderer = PIXI.autoDetectRenderer(size.width, size.height, { transparent: true });
		this.dom.appendChild(this.renderer.view);

		this.renderer.view.style.position = "absolute";
		this.renderer.view.style.left = "0px";
		this.renderer.view.style.top = "0px";
	}

	bindEvents() {
		document.addEventListener("keydown", this.onKeyDown, false);
	}

	unbindEvents() {
		document.removeEventListener("keydown", this.onKeyDown, false);
	}

	onKeyDown(event: KeyboardEvent) {
		var keyCode = event.which;
		var prevent = false;

		if (keyCode === Game.keyCodes.up) {
			this.player.wantMove(1);
			prevent = true;
		}
		if (keyCode === Game.keyCodes.right) {
			this.player.wantMove(2);
			prevent = true;
		}
		if (keyCode === Game.keyCodes.down) {
			this.player.wantMove(3);
			prevent = true;
		}
		if (keyCode === Game.keyCodes.left) {
			this.player.wantMove(4);
			prevent = true;
		}

		if (prevent === true) {
			event.stopPropagation();
			event.preventDefault();
		}
	}

	releaseResources() {
		Game.logger.info("release game resources");
		this.unbindEvents();

		if (this.tickTimer) {
			this.tickTimer.stop();
			this.tickTimer = null;
		}
		if (this.inputTimer) {
			this.inputTimer.stop();
			this.inputTimer = null;
		}
		if (this.client) {
			this.client.close();
			this.client = null;
		}
		if (this.scene) {
			this.scene.destroy();
			this.scene = null;
		}
		if (this.players) {
			this.players.clear();
			this.players = null;
		}
		if (this.renderer) {
			this.renderer.view.remove();
			this.renderer.destroy();
			this.renderer = null;
		}
	}

	tick(timer: Timer) {
		this.players.tick(timer);
		this.camera.update(this.player.position);
		this.scene.tick(timer);
		this.field.tick();
		this.renderer.render(this.scene.stage);

		var gameMinimap = this.gameMinimap.__controller as GameMinimap;
		gameMinimap.updateMyPoint(this.player.position.x, this.player.position.y, this);

		this.updateSystemStat();
	}

	updateSystemStat() {
		var fps = this.tickTimer.fps.toFixed();
		var latency = this.client.latency.toFixed();
		this.gameSystemStat.innerHTML = "fps: " + fps + ", latency: " + latency + " ms, server: " + this.client.serverAdress;
	}

	sendPlayerInput(timer: Timer) {
		if (!this.player.inputSended) {
			this.client.send(ClientMessage.moves[this.player.wantDirection]);
			this.player.inputSended = true;
		}
	}

}