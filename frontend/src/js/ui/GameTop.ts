
class GameTop extends Controller {

	lineIndex: number = 0;
	top10: ServerData.Top10Line[];

	gameTopTemplateBase: HTMLElement;
	gameTopTemplateOriginal: HTMLElement;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.listen(["game.close"]);
		this.top10 = [];
	}

	update(event?: Event) {
		if (event && event.type === "game.close") {
			this.clear();
		}
		var render = "";

		var game = this.parent as Game;
		var gameType = game.gameType;
		var tpl: Template = null;

		if (gameType === "original" || gameType === "towers" || gameType === "mines") {
			tpl = new Template(this.gameTopTemplateOriginal.innerHTML);
		} else {
			tpl = new Template(this.gameTopTemplateBase.innerHTML);
		}

		for (var i = 0; i < this.top10.length; i++) {
			var line = this.top10[i];
			var rgb = Color.hsvToRgb(line.color / Game.colors, 1.0, 1.0);

			if (gameType === "original" || gameType === "towers" || gameType === "mines") {
				var score = (line.score * 0.01).toFixed(2) + "%";
				var maxScoreK = this.findMaxScorePercent();
				render += tpl.render({
					nick: line.nick,
					score: score,
					bg: "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")",
					width: line.score * 0.01 * maxScoreK + "%"
				});
			} else {
				var maxScoreK = this.findMaxScore();
				render += tpl.render({
					position: line.position + 1,
					nick: line.nick,
					score: line.score,
					bg: "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")",
					width: line.score * maxScoreK + "%"
				});
			}
		}

		this.dom.innerHTML = render;
		this.dom.showThen(this.top10.length > 0);
	}

	findMaxScorePercent(): number {
		if (this.top10.length > 0) {
			return 100 / (this.top10[0].score * 0.01);
		} else {
			return 1;
		}
	}

	findMaxScore(): number {
		if (this.top10.length > 0) {
			return 100 / (this.top10[0].score);
		} else {
			return 1;
		}
	}

	addLine(data: ServerData.Top10Line) {
		data.position = this.lineIndex++;
		this.top10.push(data);
	}

	end() {
		this.lineIndex = 0;
		this.update();
		this.top10.length = 0;
	}

	clear() {
		this.top10.length = 0;
		this.lineIndex = 0;
		this.update();
	}

}