
class GameExp extends Controller {

	index: number = 0;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.on("game.score.capture", this.onScore);
		this.on("game.score.kill", this.onScore);
		this.on("game.score.towerCapture", this.onScore);
	}

	onScore() {
		this.addScore(event["score"]);
	}

	addScore(score: number) {
		var id = this.index++;
		var tpl = "<div class=\"Game-exp-item\" id=\"gameExpItem" + id + "\"><span class=\"Game-exp-item-gold\">+" + score + "</span> score</div>";

		this.dom.append(tpl);

		setTimeout((function() {
			var item = this.dom.findOne("#gameExpItem" + id);
			item && item.remove();
		}).bind(this), 1500);
	}

}