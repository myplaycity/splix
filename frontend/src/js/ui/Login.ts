
class Login extends Controller {

	name: string = "";
	userHash: string = "";
	gameType: string = "";

	nameInput: HTMLInputElement;

	toString(): string {
		return "Login()";
	}

	startGame(type: string): void {
		this.name = this.nameInput.value;
		this.gameType = type;
		this.dispatch("game.start");
	}

	auth(): void {
		this.userHash = localStorage.getItem("userHash") || null;

		if (!this.userHash) {
			SplixApi.auth(function(userData: UserData) {
				this.userHash = userData.hash;
				localStorage.setItem("userHash", this.userHash);
			});
		}
	}

}