
class GameOver extends Controller {

	visible: boolean = false;
	gameoverBlock: HTMLElement;

	gameoverOriginalTemplate: HTMLElement;
	gameoverTowersTemplate: HTMLElement;
	gameoverMinesTemplate: HTMLElement;

	parent: Game;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.on("game.gameover", this.show);
		this.on("game.close", this.hide);
	}

	update(): void {
		this.dom.showThen(this.visible);
	}

	show(event: Event) {
		this.visible = true;

		var data = event["data"] as ServerData.GameOver;

		if (this.parent.gameType === "original") {
			this.renderOriginal(data);
		} else if (this.parent.gameType === "towers") {
			this.renderTowers(data);
		} else if (this.parent.gameType === "mines") {
			this.renderMines(data);
		}
	}

	renderOriginal(data: ServerData.GameOver) {
		var tpl = new Template(this.gameoverOriginalTemplate.innerHTML);
		var fieldPercent = Math.floor(data.fieldPercent * 0.01);

		this.gameoverBlock.innerHTML = tpl.render({
			fieldPercent: fieldPercent,
			fieldPercentTotal: fieldPercent * data.k1,
			kills: data.kills,
			killsTotal: data.kills * data.k2,
			total: data.total,
			k1: data.k1,
			k2: data.k2,
		});
	}

	renderTowers(data: ServerData.GameOver) {
		var tpl = new Template(this.gameoverTowersTemplate.innerHTML);
		var fieldPercent = Math.floor(data.fieldPercent * 0.01);

		this.gameoverBlock.innerHTML = tpl.render({
			fieldPercent: fieldPercent,
			fieldPercentTotal: fieldPercent * data.k1,
			kills: data.kills,
			killsTotal: data.kills * data.k2,
			towers: data.towers,
			towersTotal: data.towers * data.k3,
			total: data.total,
			k1: data.k1,
			k2: data.k2,
			k3: data.k3
		});
	}

	renderMines(data: ServerData.GameOver) {
		var tpl = new Template(this.gameoverMinesTemplate.innerHTML);
		var fieldPercent = Math.floor(data.fieldPercent * 0.01);

		this.gameoverBlock.innerHTML = tpl.render({
			fieldPercent: fieldPercent,
			fieldPercentTotal: fieldPercent * data.k1,
			kills: data.kills,
			killsTotal: data.kills * data.k2,
			mined: data.mined,
			minedTotal: data.mined * data.k3,
			total: data.total,
			k1: data.k1,
			k2: data.k2,
			k3: data.k3
		});
	}

	hide() {
		this.visible = false;
	}

	close() {
		this.dispatch("game.close");
	}

}