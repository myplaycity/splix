
class GameMinimap extends Controller {

	map: number[][];
	sizeX: number;
	sizeY: number;

	lineIndex: number;

	myPoint: HTMLElement;
	mapImg: HTMLCanvasElement;

	constructor(dom: HTMLElement, parent: Controller) {
		super(dom, parent);
		this.listen(["game.close"]);
	}

	setup(x: number, y: number) {
		this.sizeX = x;
		this.sizeY = y;
		this.clear();
	}

	clear() {
		this.lineIndex = -1;
		this.map = new Array(this.sizeY);
		for (var i = 0; i < this.sizeY; i++) {
			this.map[i] = new Array(this.sizeX);
			for (var j = 0; j < this.sizeX; j++) {
				this.map[i][j] = 0;
			}
		}
		this.update();
	}

	update(event?: Event) {
		if (event && event.type === "game.close") {
			this.clear();
		}
		this.renderMapToImage();
	}

	renderMapToImage() {
		var mapSize = 150;
		var cellSize = mapSize / this.sizeX;

		var canvas = this.mapImg;
		var ctx = canvas.getContext("2d");
		canvas.width = mapSize;
		canvas.height = mapSize;

		// clear
		ctx.fillStyle = "#444";
		ctx.fillRect(0, 0, mapSize, mapSize);

		// fill
		ctx.fillStyle = "#888";
		for (var y = 0; y < this.sizeY; y++) {
			for (var x = 0; x < this.sizeX; x++) {
				if (this.map[y][x] > 0) {
					ctx.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
				}
			}
		}
	}

	updateMyPoint(x: number, y: number, game: Game) {
		x = x / Game.cellSize / game.field.sizeX * 100;
		y = y / Game.cellSize / game.field.sizeY * 100;

		this.myPoint.style.left = x + "%";
		this.myPoint.style.top = y + "%";
	}

	addLine() {
		this.lineIndex++;
		this.clearLine();
	}

	clearLine() {
		for (var x = 0; x < this.sizeX; x++) {
			this.map[this.lineIndex][x] = 0;
		}
	}

	fill(start: number, end: number) {
		for (var x = start; x <= end; x++) {
			this.map[this.lineIndex][x] = 1;
		}
	}

	stop() {
		this.lineIndex = -1;
		this.update();
	}

}