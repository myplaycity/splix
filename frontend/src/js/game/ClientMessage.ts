
class ClientMessage {

	static types = {
		Ping: 0,
		MoveTop: 1,
		MoveRight: 2,
		MoveBottom: 3,
		MoveLeft: 4,
		UserInfo: 5
	};

	static ping = (new Int8Array(1)).fill(ClientMessage.types.Ping).buffer;

	// cached buffers to send player moves
	static moves: { [index: number]: ArrayBuffer } = {
		1: (new Int8Array(1)).fill(ClientMessage.types.MoveTop).buffer,
		2: (new Int8Array(1)).fill(ClientMessage.types.MoveRight).buffer,
		3: (new Int8Array(1)).fill(ClientMessage.types.MoveBottom).buffer,
		4: (new Int8Array(1)).fill(ClientMessage.types.MoveLeft).buffer
	};

	static userInfo(userName: string, userHash: string): ArrayBuffer {
		var nameLen = userName.length;
		var buffer = new ArrayBuffer(1 + 4 + 1 + nameLen * 2 + 1 + 32 * 2);
		var view = new DataView(buffer);

		view.setInt8(0, ClientMessage.types.UserInfo);
		view.setInt8(1, nameLen);

		var start = 2;
		for (var i = 0; i < nameLen; i++) {
			view.setUint16(start, userName.charCodeAt(i) || 0);
			start += 2;
		}

		for (var i = 0; i < 32; i++) {
			view.setUint16(start, userHash.charCodeAt(i) || 0);
			start += 2;
		}
		return buffer;
	}

}