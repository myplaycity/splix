
class Towers {

	game: Game = null;
	list: Tower[];

	constructor(game: Game) {
		this.game = game;
		this.list = new Array<Tower>();
	}

	add(tower: Tower) {
		this.list.push(tower);
	}

	remove(towerId: number) {
		var tower = this.find(towerId);
		if (tower) {
			tower.destroy();
			this.list.remove(tower);
		}
	}

	clear() {
		this.list.length = 0;
	}

	find(towerId: number): Tower {
		for (var i = 0; i < this.list.length; i++) {
			if (this.list[i].id === towerId) {
				return this.list[i];
			}
		}
		return null;
	}

}
