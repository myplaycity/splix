
class SplixFieldPoint {

	x: number;
	y: number;
	quad: SplixQuad;
	color: number = 0;
	oldColor: number = 0;

	status: number;
	statusTicks: number = -1;
	maxStatusTicks: number = -1;

	static NORMAL = 1;
	static ANIMATING = 2;

	sprite: PIXI.Sprite = null;

	static textures: { [index: number]: PIXI.Texture };

	static generateTextures() {
		var canvas = document.createElement("canvas");
		var ctx = canvas.getContext("2d");
		var cellSize = 50;
		var colors = Game.colors;

		canvas.width = cellSize * colors;
		canvas.height = cellSize;

		for (var i = 0; i < colors; i++) {
			var hue = i / colors;

			ctx.fillStyle = "#808080";
			ctx.fillRect(cellSize * i, 0, cellSize, cellSize);

			var rgbShadow = Color.hsvToRgb(hue, 1, 0.6).join(",");
			ctx.fillStyle = "rgba(" + rgbShadow + ", 1)";
			ctx.fillRect(cellSize * i, 0, cellSize, cellSize);

			var rgbLight = Color.hsvToRgb(hue, 1, 0.8).join(",");
			ctx.fillStyle = "rgba(" + rgbLight + ", 1)";
			ctx.fillRect(cellSize * i, 0, cellSize - 3, cellSize - 3);
		}

		var baseTexture = PIXI.BaseTexture.fromCanvas(canvas);
		SplixFieldPoint.textures = {};
		for (var i = 0; i < colors; i++) {
			SplixFieldPoint.textures[i + 1] = new PIXI.Texture(
				baseTexture,
				new PIXI.Rectangle(cellSize * i, 0, cellSize, cellSize)
			);
		}
	}

	constructor(quad: SplixQuad, x: number, y: number) {
		this.x = x;
		this.y = y;
		this.quad = quad;
		this.status = SplixFieldPoint.NORMAL;
	}

	createSprite() {
		var dx = Game.cellSize;
		var dy = Game.cellSize;
		this.sprite = new PIXI.Sprite(SplixFieldPoint.textures[0]);
		this.sprite.anchor.set(0.5, 0.5);
		this.sprite.position.set(this.x * dx, this.y * dy);
		this.quad.container.addChild(this.sprite);
	}

	toString(): string {
		return "SplixFieldPoint(" + this.x + ", " + this.y + ")";
	}

	startAnimate() {
		this.statusTicks = MathUtils.randInt(1, 5);
	}

	tick() {
		if (this.statusTicks > 0) {
			this.statusTicks -= 1;

			if (this.statusTicks === 0) {
				if (this.status === SplixFieldPoint.NORMAL) {
					this.status = SplixFieldPoint.ANIMATING;
					this.statusTicks = MathUtils.randInt(20, 50);
					this.maxStatusTicks = this.statusTicks;

					if (this.color > 0) {
						this.sprite.visible = true;
					}

				} else if (this.status === SplixFieldPoint.ANIMATING) {
					this.renderAnimation(); // final animation tick
					this.status = SplixFieldPoint.NORMAL;
					this.statusTicks = -1;
					this.maxStatusTicks = -1;

					if (this.color === 0) {
						this.sprite.visible = false;
					}
				}
			}

			if (this.status === SplixFieldPoint.ANIMATING) {
				this.renderAnimation();
			}
		}
	}

	renderAnimation() {
		// change texture
		var timeToChange = this.statusTicks < this.maxStatusTicks * 0.5;
		var t = this.statusTicks / this.maxStatusTicks;

		// animate size
		if (this.color > 0 && this.oldColor > 0) {
			if (timeToChange) {
				this.sprite.texture = SplixFieldPoint.textures[this.color];
			}
			var s = 50 - Math.sin(t * Math.PI) * 50;
			this.sprite.width = s;
			this.sprite.height = s;

		} else if (this.color > 0 && this.oldColor === 0) {
			this.sprite.texture = SplixFieldPoint.textures[this.color];
			var s = 50 - 50 * t;
			this.sprite.width = s;
			this.sprite.height = s;

		} else if (this.color === 0 && this.oldColor > 0) {
			var s = 50 * t;
			this.sprite.width = s;
			this.sprite.height = s;
		}
	}

	setColor(color: number) {
		if (color > 0 && !this.sprite) {
			this.createSprite();
		}
		if (color !== this.color) {
			this.oldColor = this.color;
			this.color = color;
			this.startAnimate();
		}
	}

}

SplixFieldPoint.generateTextures();