
class Tower {

	game: Game;
	id: number;
	position: Vector;
	sprite: PIXI.Sprite;
	quad: SplixQuad;

	static texture = PIXI.Texture.fromImage("/img/game/tower.png");

	constructor(game: Game, towerId: number, quadId: number, position: Vector) {
		this.game = game;
		this.id = towerId;
		this.position = position;
		this.quad = game.field.quads[quadId];
		this.quad.addTower(this);
		this.createGraphics();
	}

	createGraphics() {
		this.sprite = new PIXI.Sprite(Tower.texture);
		this.sprite.anchor.set(0.25, 0.25);
		this.sprite.width = Game.cellSize * 2;
		this.sprite.height = Game.cellSize * 2;
		this.sprite.position.set(this.position.x, this.position.y);
		this.quad.container.addChild(this.sprite);
	}

	destroy() {
		if (this.sprite) {
			this.sprite.parent.removeChild(this.sprite);
			this.sprite.destroy();
			this.sprite = null;
		}
		this.quad = null;
		this.game = null;
		this.position = null;
	}

}
