
class ServerMessage {

	client: Client;
	buffer: ArrayBuffer;
	view: DataView;
	bytePos: number;

	static types = {
		PONG: 1,
		HANDSHAKE: 2,
		SETUP_PLAYER: 3,
		MOVE_DIRECTION: 4,
		NEW_PLAYER: 5,
		DIE_PLAYER: 6,
		REVEAL_QUAD: 7,
		QUAD_FILL: 8,
		REVEAL_TAIL: 9,
		TAIL_START: 10,
		TAIL_CLEAR: 11,
		CHANGE_QUAD: 13,
		HIDE_PLAYER: 14,
		EARN_SCORE_CAPTURE: 15,
		EARN_SCORE_KILL: 16,
		RATING: 17,
		REVEAL_QUAD_STOP: 18,
		TOP10_LINE: 19,
		TOP10_STOP: 20,
		MINIMAP_LINE: 21,
		MINIMAP_FILL: 22,
		MINIMAP_STOP: 23,
		GAME_OVER: 24,
		NEW_TOWER: 25,
		REMOVE_TOWER: 26,
		EARN_SCORE_TOWER_CAPTURE: 27,
		NEW_MINE: 28,
		REMOVE_MINE: 29,
		EARN_SCORE_MINE_CAPTURE: 30,
		MINE_CHARGE: 31
	};

	constructor(buffer: ArrayBuffer, client: Client) {
		this.buffer = buffer;
		this.client = client;
	}

	toString(): string {
		return "ServerMessage()";
	}

	decodeAndRun() {
		this.view = new DataView(this.buffer),
		this.bytePos = 0;

		var length = this.buffer.byteLength;
		var types = ServerMessage.types;

		while (this.bytePos < length) {
			var messType = this.int8();

			if (messType === types.PONG) {
				this.readPong();
			} else if (messType === types.HANDSHAKE) {
				this.readHandshake();
			} else if (messType === types.SETUP_PLAYER) {
				this.readSetupPlayer();
			} else if (messType === types.MOVE_DIRECTION) {
				this.readMoveDirection();
			} else if (messType === types.NEW_PLAYER) {
				this.readNewPlayer();
			} else if (messType === types.DIE_PLAYER) {
				this.readDiePlayer();
			} else if (messType === types.REVEAL_QUAD) {
				this.readRevealQuad();
			} else if (messType === types.QUAD_FILL) {
				this.readQuadFill();
			} else if (messType === types.REVEAL_TAIL) {
				this.readRevealTail();
			} else if (messType === types.TAIL_START) {
				this.readTailStart();
			} else if (messType === types.TAIL_CLEAR) {
				this.readTailClear();
			} else if (messType === types.CHANGE_QUAD) {
				this.readChangeQuad();
			} else if (messType === types.HIDE_PLAYER) {
				this.readHidePlayer();
			} else if (messType === types.EARN_SCORE_CAPTURE) {
				this.readEarnScoreCapture();
			} else if (messType === types.EARN_SCORE_KILL) {
				this.readEarnScoreKill();
			} else if (messType === types.RATING) {
				this.readRating();
			} else if (messType === types.REVEAL_QUAD_STOP) {
				this.readRevealQuadStop();
			} else if (messType === types.TOP10_LINE) {
				this.readTop10Line();
			} else if (messType === types.TOP10_STOP) {
				this.readTop10Stop();
			} else if (messType === types.MINIMAP_LINE) {
				this.readMinimapLine();
			} else if (messType === types.MINIMAP_FILL) {
				this.readMinimapFill();
			} else if (messType === types.MINIMAP_STOP) {
				this.readMinimapStop();
			} else if (messType === types.GAME_OVER) {
				this.readGameOver();
			} else if (messType === types.NEW_TOWER) {
				this.readNewTower();
			} else if (messType === types.REMOVE_TOWER) {
				this.readRemoveTower();
			} else if (messType === types.EARN_SCORE_TOWER_CAPTURE) {
				this.readEarnScoreTowerCapture();
			} else if (messType === types.NEW_MINE) {
				this.readNewMine();
			} else if (messType === types.REMOVE_MINE) {
				this.readRemoveMine();
			} else if (messType === types.EARN_SCORE_MINE_CAPTURE) {
				this.readEarnScoreMineCapture();
			} else if (messType === types.MINE_CHARGE) {
				this.readMineCharge();
			} else {
				throw new Error("cant decode server message with type " + messType);
			}
		}
	}

	// read data atoms

	private int8(): number {
		var data = this.view.getInt8(this.bytePos);
		this.bytePos += 1;
		return data;
	}

	private int16(): number {
		var data = this.view.getInt16(this.bytePos);
		this.bytePos += 2;
		return data;
	}

	private int32(): number {
		var data = this.view.getInt32(this.bytePos);
		this.bytePos += 4;
		return data;
	}

	private readString(length: number): string {
		var view = this.view;
		var utf16 = new ArrayBuffer(length * 2);
		var utf16View = new Uint16Array(utf16);
		var start = this.bytePos;
		var pos = 0;

		for (var i = 0; i < length; i++) {
			var char = view.getUint16(start + i * 2);
			if (char > 0) {
				utf16View[pos] = char;
				pos++;
			}
		}

		utf16View = utf16View.slice(0, pos);
		this.bytePos += length * 2;
		return String.fromCharCode.apply(null, utf16View);
	}

	// read message types

	private readPong() {
		this.client.onMessagePong();
	}

	private readHandshake() {
		this.client.onMessageHandshake({
			sizeX: this.int16(),
			sizeY: this.int16(),
			quadSize: this.int16(),
			viewQuadPadding: this.int16()
		} as ServerData.Handshake);
	}

	private readSetupPlayer() {
		var data = {
			playerId: this.int32(),
			color: this.int8(),
			posX: this.int16(),
			posY: this.int16(),
			dirX: this.int8(),
			dirY: this.int8(),
			nickLen: this.int8()
		} as ServerData.NewPlayer;
		data.nick = this.readString(data.nickLen);
		this.client.onMessageSetupPlayer(data);
	}

	private readMoveDirection() {
		this.client.onMessageMoveDirection({
			playerId: this.int32(),
			posX: this.int16(),
			posY: this.int16(),
			moveAngle: this.int8()
		} as ServerData.MoveDirection);
	}

	private readNewPlayer() {
		var data = {
			playerId: this.int32(),
			color: this.int8(),
			posX: this.int16(),
			posY: this.int16(),
			dirX: this.int8(),
			dirY: this.int8(),
			nickLen: this.int8()
		} as ServerData.NewPlayer;
		data.nick = this.readString(data.nickLen);
		this.client.onMessageNewPlayer(data);
	}

	private readDiePlayer() {
		this.client.onMessageDiePlayer(this.int32());
	}

	private readRevealQuad() {
		this.client.onMessageRevealQuad(this.int16());
	}

	private readQuadFill() {
		this.client.onMessageQuadFill({
			color: this.int8(),
			start: this.int8(),
			end: this.int8()
		} as ServerData.FieldFill);
	}

	private readRevealTail() {
		var playerId = this.int32();
		var count = this.int16();
		var points = [];
		for (var i = 0; i < count; i++) {
			points.push(new Vector(this.int16(), this.int16()));
		}

		this.client.onMessageRevealTail({
			playerId: playerId,
			count: count,
			points: points
		} as ServerData.RevealTail);
	}

	private readTailStart() {
		this.client.onMessageTailStart({
			playerId: this.int32(),
			posX: this.int16(),
			posY: this.int16(),
		} as ServerData.TailStart);
	}

	private readTailClear() {
		this.client.onMessageTailClear(this.int32());
	}

	private readChangeQuad() {
		this.client.onMessageChangeQuad(this.int16());
	}

	private readHidePlayer() {
		this.client.onMessageHidePlayer(this.int32());
	}

	private readEarnScoreCapture() {
		this.client.onMessageEarnScoreCapture(this.int16());
	}

	private readEarnScoreKill() {
		this.client.onMessageEarnScoreKill(this.int16());
	}

	private readRating() {
		this.client.onMessageRating(this.int16(), this.int16());
	}

	private readRevealQuadStop() {
		this.client.onRevealQuadStop();
	}

	private readTop10Line() {
		var data = {
			score: this.int32(),
			color: this.int8(),
			nickLen: this.int8()
		} as ServerData.Top10Line;
		data.nick = this.readString(data.nickLen);
		this.client.onMessageTop10Line(data);
	}

	private readTop10Stop() {
		this.client.onMessageTop10Stop();
	}

	private readMinimapLine() {
		this.client.onMessageMinimapLine();
	}

	private readMinimapFill() {
		this.client.onMessageMinimapFill(this.int8(), this.int8());
	}

	private readMinimapStop() {
		this.client.onMessageMinimapStop();
	}

	private readGameOver() {
		var type = this.int8();
		var data: ServerData.GameOver;

		if (type === 1) {
			data = {
				fieldPercent: this.int32(),
				kills: this.int32(),
				total: this.int32(),
				k1: this.int32(),
				k2: this.int32()
			};

		} else if (type === 2) {
			data = {
				fieldPercent: this.int32(),
				kills: this.int32(),
				towers: this.int32(),
				total: this.int32(),
				k1: this.int32(),
				k2: this.int32(),
				k3: this.int32()
			};

		} else if (type === 3) {
			data = {
				fieldPercent: this.int32(),
				kills: this.int32(),
				mined: this.int32(),
				total: this.int32(),
				k1: this.int32(),
				k2: this.int32(),
				k3: this.int32()
			};

		} else if (type === 4) {
			// some code ...
		}

		this.client.onMessageGameOver(data);
	}

	private readNewTower() {
		var towerId = this.int32();
		var quadId = this.int32();
		var x = this.int16();
		var y = this.int16();
		this.client.onMessageNewTower(towerId, quadId, x, y);
	}

	private readRemoveTower() {
		var id = this.int32();
		this.client.onMessageRemoveTower(id);
	}

	private readEarnScoreTowerCapture() {
		this.client.onMessageEarnScoreTowerCapture(this.int16());
	}

	private readNewMine() {
		var towerId = this.int32();
		var quadId = this.int32();
		var x = this.int16();
		var y = this.int16();
		var charges = this.int16();
		this.client.onMessageNewMine(towerId, quadId, x, y, charges);
	}

	private readRemoveMine() {
		var id = this.int32();
		this.client.onMessageRemoveMine(id);
	}

	private readEarnScoreMineCapture() {
		this.client.onMessageEarnScoreMineCapture(this.int16());
	}

	private readMineCharge() {
		this.client.onMessageMineCharge(this.int32(), this.int16());
	}

}

declare module ServerData {

	export interface Handshake {
		sizeX: number;
		sizeY: number;
		quadSize: number;
		viewQuadPadding: number;
	}

	export interface SetupPlayer {
		playerId: number;
		color: number;
		posX: number;
		posY: number;
		dirX: number;
		dirY: number;
		nickLen: number;
		nick: string;
	}

	export interface MoveDirection {
		playerId: number;
		posX: number;
		posY: number;
		moveAngle: number;
	}

	export interface NewPlayer {
		playerId: number;
		color: number;
		posX: number;
		posY: number;
		dirX: number;
		dirY: number;
		nickLen: number;
		nick: string;
	}

	export interface FieldFill {
		color: number;
		start: number;
		end: number;
	}

	export interface RevealTail {
		playerId: number;
		count: number;
		points: Vector[];
	}

	export interface TailStart {
		playerId: number;
		posX: number;
		posY: number;
	}

	export interface Top10Line {
		position: number;
		score: number;
		nickLen: number;
		nick: string;
		color: number;
	}

	export interface GameOver {
		fieldPercent?: number;
		kills?: number;
		towers?: number;
		total?: number;
		mined?: number;
		k1?: number;
		k2?: number;
		k3?: number;
	}

}
