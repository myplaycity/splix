
class Vector {

	public x: number = 0.0;
	public y: number = 0.0;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	clone(): Vector {
		return new Vector(this.x, this.y);
	}

	set(x: number, y: number): Vector {
		this.x = x;
		this.y = y;
		return this;
	}

	setFrom(v: Vector): Vector {
		this.x = v.x;
		this.y = v.y;
		return this;
	}

	toString() {
		return "Vector(" + this.x.toFixed(2) + ", " + this.y.toFixed(2) + ")";
	}

	equals(v: Vector) {
		return this.x === v.x && this.y === v.y;
	}

	add(v: Vector): Vector {
		this.x = this.x + v.x;
		this.y = this.y + v.y;
		return this;
	}

	addScaled(v: Vector, a: number): Vector {
		this.x = this.x + v.x * a;
		this.y = this.y + v.y * a;
		return this;
	}

	sub(v: Vector): Vector {
		this.x = this.x - v.x;
		this.y = this.y - v.y;
		return this;
	}

	scale(a: number): Vector {
		this.x = this.x * a;
		this.y = this.y * a;
		return this;
	}

	invert(): Vector {
		this.x = this.x * -1.0;
		this.y = this.y * -1.0;
		return this;
	}

	size(): number {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	normalize(): Vector {
		let len = 1.0 / (this.size() || 1.0);
		this.x = this.x * len;
		this.y = this.y * len;
		return this;
	}

	setSize(size: number): Vector {
		let len = 1.0 / (this.size() || 1.0) * size;
		this.x = this.x * len;
		this.y = this.y * len;
		return this;
	}

	dist(v: Vector): number {
		return Math.sqrt((this.x - v.x) * (this.x - v.x) + (this.y - v.y) * (this.y - v.y));
	}

	smoothWith(dst: Vector, smoothing: number): Vector {
		this.x = (dst.x * smoothing) + (this.x * (1.0 - smoothing));
		this.y = (dst.y * smoothing) + (this.y * (1.0 - smoothing));
		return this;
	}

}
