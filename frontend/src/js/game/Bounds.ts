
class Bounds {

	public x1: number = 0.0;
	public y1: number = 0.0;
	public x2: number = 0.0;
	public y2: number = 0.0;

	constructor(x1: number, y1: number, x2: number, y2: number) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	static fromPoint(vector: Vector, indent?: number): Bounds {
		var b = new Bounds(vector.x, vector.y, vector.x, vector.y);
		if (indent !== undefined) {
			b.indent(indent);
		}
		return b;
	}

	clone(): Bounds {
		return new Bounds(this.x1, this.y1, this.x2, this.y2);
	}

	set(x1: number, y1: number, x2: number, y2: number): Bounds {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		return this;
	}

	setFrom(bounds: Bounds): Bounds {
		this.x1 = bounds.x1;
		this.y1 = bounds.y1;
		this.x2 = bounds.x2;
		this.y2 = bounds.y2;
		return this;
	}

	clamp(minX: number, minY: number, maxX: number, maxY: number): Bounds {
		if (this.x1 < minX) { this.x1 = minX; }
		if (this.y1 < minY) { this.y1 = minY; }
		if (this.x2 > maxX) { this.x2 = maxX; }
		if (this.y2 > maxY) { this.y2 = maxY; }
		return this;
	}

	equals(bounds: Bounds) {
		return bounds.x1 === this.x1 && bounds.y1 === this.y1 && bounds.x2 === this.x2 && bounds.y2 === this.y2;
	}

	indent(value: number): Bounds {
		this.x1 -= value;
		this.y1 -= value;
		this.x2 += value;
		this.y2 += value;
		return this;
	}

	hasPointXY(x: number, y: number): boolean {
		return (x >= this.x1 && x <= this.x2 && y >= this.y1 && y <= this.y2);
	}

}
