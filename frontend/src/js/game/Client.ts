
interface WebSocketEvent {
	data: ArrayBuffer
}

class Client {

	private static logger = new Logger("Client");

	game: Game;
	socket: WebSocket;
	server: GameserverData;
	serverAdress: string;

	latency: number; // latency in milliseconds
	latencyTimer: Timer;
	latencySendTime: number;

	constructor(game: Game, server: GameserverData) {
		this.game = game;
		this.server = server;
		this.serverAdress = server.ws;

		this.socket = new WebSocket(server.ws);
		this.socket.binaryType = "arraybuffer";
		this.socket.onopen = this.onConnect.bind(this);
		this.socket.onclose = this.onClose.bind(this);
		this.socket.onmessage = this.onMessage.bind(this);
		this.socket.onerror = this.onError.bind(this);
	}

	close() {
		if (this.socket !== null) {
			this.socket.close();
			this.socket = null;
		}
	}

	toString(): string {
		return "Client()";
	}

	// websocket events

	onConnect(event: WebSocketEvent) {
		Client.logger.info("connected to: " + this.serverAdress);
		this.sendUserInfo(this.game.login.name, this.game.login.userHash);
		this.latencyTimer = Timer.startFPS(0.5, this.checkLatency.bind(this));
		this.latency = 0;
	}

	onClose(event: WebSocketEvent) {
		Client.logger.info("closing connection to: " + this.serverAdress);
		this.socket = null;
		this.latencyTimer.stop();
		this.latencyTimer = null;
	}

	onMessage(event: WebSocketEvent) {
		var message = new ServerMessage(event.data, this);
		message.decodeAndRun();
	}

	onError(error: Error) {
		Client.logger.error("websocket error", error);
		this.close();
	}

	// sending

	send(message: ArrayBuffer) {
		this.socket.send(message);
	}

	sendUserInfo(userName: string, userHash: string) {
		this.send(ClientMessage.userInfo(userName, userHash));
	}

	// server message reaction

	onMessagePong() {
		this.latency = (Date.now() - this.latencySendTime) * 0.5;
	}

	onMessageHandshake(data: ServerData.Handshake) {
		this.game.prepareScene(data);
	}

	onMessageSetupPlayer(data: ServerData.SetupPlayer) {
		this.game.prepareSetupPlayer(data);
	}

	onMessageMoveDirection(data: ServerData.MoveDirection) {
		var player = this.game.players.find(data.playerId);
		if (player) {
			player.syncMoveDirection(data);
		}
	}

	onMessageNewPlayer(data: ServerData.NewPlayer) {
		var player = this.game.players.find(data.playerId);
		if (player) {
			player.reveal(data);
		} else {
			var pos = new Vector(data.posX * Game.cellSize, data.posY * Game.cellSize);
			var dir = new Vector(data.dirX, data.dirY);
			player = new Player(this.game, data.playerId, data.color, data.nick, pos, dir);
			this.game.players.add(player);
		}
	}

	onMessageDiePlayer(playerId: number) {
		var player = this.game.players.find(playerId);
		if (player) {
			player.die();
		}
	}

	onMessageRevealQuad(quadId: number) {
		this.game.field.revealQuad(quadId);
	}

	onMessageQuadFill(data: ServerData.FieldFill) {
		this.game.field.fillQuad(data);
	}

	onMessageRevealTail(data: ServerData.RevealTail) {
		var player = this.game.players.find(data.playerId);
		if (player) {
			player.revealTail(data);
		}
	}

	onMessageTailStart(data: ServerData.TailStart) {
		var player = this.game.players.find(data.playerId);
		if (player) {
			player.startTail(data);
		}
	}

	onMessageTailClear(playerId: number) {
		var player = this.game.players.find(playerId);
		if (player) {
			player.clearTail();
		}
	}

	onMessageChangeQuad(quadId: number) {
		this.game.field.setQuadCenter(quadId);
	}

	onMessageHidePlayer(playerId: number) {
		var player = this.game.players.find(playerId);
		if (player) {
			player.hide();
		}
	}

	onMessageEarnScoreCapture(score: number) {
		this.game.dispatch("game.score.capture", { score: score });
	}

	onMessageEarnScoreKill(score: number) {
		this.game.dispatch("game.score.kill", { score: score });
	}

	onMessageRating(rating: number, totalPlayers: number) {
		this.game.dispatch("game.score.rating", { rating: rating, totalPlayers: totalPlayers });
	}

	onRevealQuadStop() {
		this.game.field.revealQuadStop();
	}

	onMessageTop10Line(data: ServerData.Top10Line) {
		var gameTop = this.game.gameTop.__controller as GameTop;
		gameTop.addLine(data);
	}

	onMessageTop10Stop() {
		var gameTop = this.game.gameTop.__controller as GameTop;
		gameTop.end();
	}

	onMessageMinimapLine() {
		var gameMinimap = this.game.gameMinimap.__controller as GameMinimap;
		gameMinimap.addLine();
	}

	onMessageMinimapFill(start: number, end: number) {
		var gameMinimap = this.game.gameMinimap.__controller as GameMinimap;
		gameMinimap.fill(start, end);
	}

	onMessageMinimapStop() {
		var gameMinimap = this.game.gameMinimap.__controller as GameMinimap;
		gameMinimap.stop();
	}

	onMessageGameOver(data: ServerData.GameOver) {
		if (this.game.gameType === "base") {
			this.game.dispatch("game.close");
		} else {
			Client.logger.info("receive gameover:", data);
			this.game.dispatch("game.gameover", { data: data });
		}
	}

	onMessageNewTower(towerId: number, quadId: number, posX: number, posY: number) {
		var pos = new Vector(posX * Game.cellSize, posY * Game.cellSize);
		var tower = new Tower(this.game, towerId, quadId, pos);
		this.game.towers.add(tower);
	}

	onMessageRemoveTower(towerId: number) {
		this.game.towers.remove(towerId);
	}

	onMessageEarnScoreTowerCapture(score: number) {
		this.game.dispatch("game.score.towerCapture", { score: score });
	}

	onMessageNewMine(mineId: number, quadId: number, posX: number, posY: number, charges: number) {
		var mine = this.game.towers.find(mineId);
		if (!mine) {
			var pos = new Vector(posX * Game.cellSize, posY * Game.cellSize);
			this.game.towers.add(new Mine(this.game, mineId, quadId, pos, charges));
		}
	}

	onMessageRemoveMine(mineId: number) {
		this.game.towers.remove(mineId);
	}

	onMessageEarnScoreMineCapture(score: number) {
		this.game.dispatch("game.score.mineCapture", { score: score });
	}

	onMessageMineCharge(mineId: number, chargesRemain: number) {
		var foundMine = this.game.towers.find(mineId) as Mine;
		if (foundMine) {
			foundMine.setCharges(chargesRemain);
		}
	}

	// latency checking

	checkLatency() {
		this.send(ClientMessage.ping);
		this.latencySendTime = Date.now();
	}

}
