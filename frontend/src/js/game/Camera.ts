
class Camera {

	private game: Game;
	public zoom: number;
	public center: Vector;
	public base: Vector;
	public position: Vector;
	public viewBounds: Bounds;

	constructor(game: Game) {
		this.game = game;
		this.zoom = 1.0;
		this.startup(new Vector(0.0, 0.0));
	}

	toString(): string {
		return "Camera()";
	}

	startup(center: Vector) {
		var zoom = this.zoom;
		var halfWidth = this.game.screen.width * 0.5 / zoom;
		var halfHeight = this.game.screen.height * 0.5 / zoom;

		this.center = new Vector(center.x, center.y);
		this.base = new Vector(-halfWidth, -halfHeight);
		this.position = this.center.clone().add(this.base);
		this.viewBounds = new Bounds(
			this.position.x - halfWidth,
			this.position.y - halfHeight,
			this.position.x + halfWidth,
			this.position.y + halfHeight
		);
	}

	private displayRatioCorrection(): number {
		var w = this.game.screen.width * 0.5;
		var h = this.game.screen.height * 0.5;
		return Math.sqrt(w*w + h*h) / 650.0;
	}

	update(center: Vector) {
		var defZoom = 0.8;
		this.zoom = this.zoom.smoothWith(defZoom * this.displayRatioCorrection(), 0.05);

		var halfWidth = this.game.screen.width * 0.5 / this.zoom;
		var halfHeight = this.game.screen.height * 0.5 / this.zoom;

		this.center.smoothWith(center, 0.1);
		this.base.set(-halfWidth, -halfHeight);
		this.position.setFrom(this.center).add(this.base);
		this.viewBounds.set(
			this.position.x - halfWidth,
			this.position.y - halfHeight,
			this.position.x + halfWidth,
			this.position.y + halfHeight,
		);
	}

}