
class SplixQuad {

	field: SplixField;
	id: number;
	x: number;
	y: number;
	quadSize: number;
	bounds: Bounds;
	points: SplixFieldPoint[] = null;
	container: PIXI.Container = null;
	towers: Tower[];

	constructor(field: SplixField, id: number, x: number, y: number, quadSize: number) {
		this.field = field;
		this.id = id;
		this.x = x;
		this.y = y;
		this.quadSize = quadSize;
		this.bounds = new Bounds(x * quadSize, y * quadSize, (x + 1) * quadSize - 1, (y + 1) * quadSize - 1);

		this.container = new PIXI.Container();
		field.container.addChild(this.container);

		this.towers = new Array<Tower>();
	}

	toString(): string {
		return "SplixQuad()";
	}

	createPoints() {
		var size = this.quadSize;
		this.points = new Array(size * size);
		for (var y = 0; y < size; y++) {
			for (var x = 0; x < size; x++) {
				this.points[y * size + x] = new SplixFieldPoint(this, x + this.bounds.x1, y + this.bounds.y1);
			}
		}
	}

	fillByBuf(buf: number[]) {
		if (!this.points) {
			this.createPoints();
		}

		for (var i = 0; i < buf.length; i++) {
			var point = this.points[i];
			if (point.color != buf[i]) {
				this.points[i].setColor(buf[i]);
			}
		}
	}

	show() {
		this.container.visible = true;
		// todo: stop point animation;
	}

	hide() {
		this.container.visible = false;
		// todo: stop point animation;
	}

	tick() {
		if (this.container.visible && this.points && this.points.length > 0) {
			for (var i = 0; i < this.points.length; i++) {
				this.points[i].tick();
			}
		}
	}

	addTower(tower: Tower) {
		this.towers.push(tower);
	}

	clearTowers() {
		var towers = this.field.game.towers.list;
		if (this.towers.length > 0) {
			for (var i = 0; i < this.towers.length; i++) {
				var tower = this.towers[i];
				tower.destroy();
				towers.remove(tower);
			}
		}
		this.towers.length = 0;
	}

}