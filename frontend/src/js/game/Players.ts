
class Players {

	game: Game = null;
	list: Player[];
	destroyedPlayers: Player[];

	constructor(game: Game) {
		this.game = game;
		this.list = new Array();
		this.destroyedPlayers = new Array();
	}

	toString(): string {
		return "GamePlayers()";
	}

	add(player: Player) {
		this.list.push(player);
	}

	remove(player: Player) {
		this.list.remove(player);
	}

	clear() {
		this.list.length = 0;
	}

	find(playerId: number): Player {
		for (var i = 0; i < this.list.length; i++) {
			if (this.list[i].id === playerId) {
				return this.list[i];
			}
		}
		return null;
	}

	tick(timer: Timer) {
		for (var i = 0; i < this.list.length; i++) {
			var player = this.list[i];
			if (!player.tick(timer)) {
				this.destroyedPlayers.push(player);
			}
		}
		if (this.destroyedPlayers.length > 0) {
			this.list.remove.apply(this.list, this.destroyedPlayers);
			this.destroyedPlayers.length = 0;
		}
	}

}