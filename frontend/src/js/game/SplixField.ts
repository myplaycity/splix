
class SplixField {

	game: Game = null;
	sizeX: number;
	sizeY: number;
	revealingQuadId: number;
	quadSize: number;
	quadsX: number;
	quadsY: number;

	quads: SplixQuad[];
	quadFillBuf: Array<number>;

	static bgTexture = PIXI.Texture.fromImage("/img/game/point.png");
	container: PIXI.Container = null;
	background: PIXI.extras.TilingSprite = null;

	oldViewQuadArea: Bounds;
	viewQuadArea: Bounds;

	constructor(game: Game, sizeX: number, sizeY: number, quadSize: number) {
		this.game = game;
		this.sizeX = sizeX;
		this.sizeY = sizeY;

		this.quadSize = quadSize;
		this.quadsX = Math.round(sizeX / quadSize);
		this.quadsY = Math.round(sizeY / quadSize);
		this.oldViewQuadArea = new Bounds(-1, -1, -1, -1);
		this.viewQuadArea = new Bounds(-1, -1, -1, -1);
		this.quadFillBuf = new Array(this.quadSize * this.quadSize);

		this.createFieldBackground();
		this.createContainer();
		this.createQuads();
	}

	createFieldBackground() {
		this.background = new PIXI.extras.TilingSprite(SplixField.bgTexture, this.sizeX * Game.cellSize, this.sizeY * Game.cellSize);
		this.background.position.set(-Game.cellSize * 0.5, -Game.cellSize * 0.5);
		this.background.tilePosition.set(0, 0);
		this.game.scene.stage.addChild(this.background);
	}

	createContainer() {
		this.container = new PIXI.Container();
		this.game.scene.stage.addChild(this.container);
	}

	createQuads() {
		this.quads = new Array(this.quadsX * this.quadsY);
		for (var y = 0; y < this.quadsY; y++) {
			for (var x = 0; x < this.quadsX; x++) {
				var id = y * this.quadsX + x;
				this.quads[id] = new SplixQuad(this, id, x, y, this.quadSize);
			}
		}
	}

	toString(): string {
		return "SplixField(" + this.sizeX + ", " + this.sizeY + ")";
	}

	revealQuad(quadId: number) {
		for (var i = 0; i < this.quadFillBuf.length; i++) {
			this.quadFillBuf[i] = 0;
		}
		this.revealingQuadId = quadId;
	}

	fillQuad(data: ServerData.FieldFill) {
		for (var i = data.start; i <= data.end; i++) {
			this.quadFillBuf[i] = data.color
		}
	}

	revealQuadStop() {
		var quad = this.quads[this.revealingQuadId];
		quad.clearTowers();
		quad.show();
		quad.fillByBuf(this.quadFillBuf);
	}

	getQuad(x: number, y: number) {
		if (x >= 0 && x < this.quadsX && y >= 0 && y < this.quadsY) {
			return this.quads[y * this.quadsX + x];
		}
		return null;
	}

	setQuadCenter(quadId: number) {
		var quad = this.quads[quadId];
		var pad = this.game.viewQuadPadding;

		this.oldViewQuadArea.setFrom(this.viewQuadArea);
		this.viewQuadArea.set(quad.x - pad, quad.y - pad, quad.x + pad, quad.y + pad);

		for (var y = this.oldViewQuadArea.y1; y <= this.oldViewQuadArea.y2; y++) {
			for (var x = this.oldViewQuadArea.x1; x <= this.oldViewQuadArea.x2; x++) {
				var q = this.getQuad(x, y);
				if (!this.viewQuadArea.hasPointXY(x, y) && q) {
					q.hide();
				}
			}
		}
	}

	tick() {
		for (var i = 0; i < this.quads.length; i++) {
			this.quads[i].tick();
		}
	}

}