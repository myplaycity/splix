
class Mine extends Tower {

	static textures: { [index: number]: PIXI.Texture };

	charges: number;

	static generateTextures() {
		var baseTexture = PIXI.BaseTexture.fromImage("/img/game/mines.png");
		var cellSize = 63;

		Mine.textures = {};
		for (var i = 0; i < 4; i++) {
			Mine.textures[i] = new PIXI.Texture(
				baseTexture,
				new PIXI.Rectangle(cellSize * i, 0, cellSize, cellSize)
			);
		}
	}

	constructor(game: Game, towerId: number, quadId: number, position: Vector, charges: number) {
		super(game, towerId, quadId, position);
		this.setCharges(charges);
	}

	createGraphics() {
		this.sprite = new PIXI.Sprite(Mine.textures[0]);
		this.sprite.anchor.set(0.25, 0.25);
		this.sprite.width = Game.cellSize * 2;
		this.sprite.height = Game.cellSize * 2;
		this.sprite.position.set(this.position.x, this.position.y);
		this.quad.container.addChild(this.sprite);
	}

	setCharges(charges: number) {
		this.charges = charges;
		if (charges >= 0 && charges < 4) {
			this.sprite.texture = Mine.textures[4 - charges];
		}
	}

}

Mine.generateTextures();