
class Scene {

	game: Game = null;
	stage: PIXI.Container;

	static logger = new Logger("Scene");

	constructor(game: Game) {
		this.game = game;
		this.stage = new PIXI.Container();
	}

	toString(): string {
		return "Scene()";
	}

	destroy() {
		this.stage.destroy();
	}

	tick(timer: Timer) {
		this.applyCamera(this.game.camera);
	}

	applyCamera(camera: Camera) {
		var zoom = camera.zoom;
		var cam = camera.position;

		this.stage.position.set(-cam.x * zoom, -cam.y * zoom);
		this.stage.scale.set(zoom, zoom);
	}

}