
interface CanvasRenderingContext2D {
	filter: string;
}

class Player {

	game: Game;
	id: number;
	color: number;
	nick: string;
	tailColor: number;
	position: Vector;
	wantDirection: number;
	moveDirection: Vector;
	inputSended: boolean;
	status: number;
	statusTicks: number;
	tail: Vector[];

	sprite: PIXI.Sprite;
	tailGraphics: PIXI.Graphics;
	nickTexture: PIXI.Texture;
	nickSprite: PIXI.Sprite;

	static STATUS_DESTROYED: number = 0;
	static STATUS_PLAYING: number = 1;
	static STATUS_DIED: number = 2;
	static STATUS_HIDDEN: number = 3;

	static DIED_TICKS: number = 30;

	static textures: { [index: number]: PIXI.Texture };

	static generateTextures() {
		var canvas = document.createElement("canvas");
		var ctx = canvas.getContext("2d");
		var cellSize = 60;
		var r = 55;
		var colors = Game.colors;

		canvas.width = cellSize * colors;
		canvas.height = cellSize;

		for (var i = 0; i < colors; i++) {
			var hue = i / colors;

			var rgbShadow = Color.hsvToRgb(hue, 1.0, 0.4).join(",");
			ctx.fillStyle = "rgba(" + rgbShadow + ", 1)";
			ctx.filter = "blur(2px)";
			ctx.beginPath();
			ctx.arc(cellSize * (i + 0.5) + 1, cellSize * 0.5 + 1, r * 0.5 - 1, 0, 2 * Math.PI);
			ctx.fill();

			var grad = ctx.createRadialGradient(cellSize * (i + 0.3), cellSize * 0.3, 0, cellSize * (i + 0.5), cellSize * 0.5, r * 0.5);
			grad.addColorStop(1.0, "rgba(" + Color.hsvToRgb(hue, 1.0, 0.6).join(",") + ", 1)");
			grad.addColorStop(0.7, "rgba(" + Color.hsvToRgb(hue, 1.0, 0.8).join(",") + ", 1)");
			grad.addColorStop(0.0, "rgba(" + Color.hsvToRgb(hue, 1.0, 1.0).join(",") + ", 1)");
			ctx.fillStyle = grad;

			ctx.filter = "none";
			ctx.beginPath();
			ctx.arc(cellSize * (i + 0.5) - 1, cellSize * 0.5 - 1, r * 0.5 - 1, 0, 2 * Math.PI);
			ctx.fill();
		}

		var baseTexture = PIXI.BaseTexture.fromCanvas(canvas);
		Player.textures = {};
		for (var i = 0; i < colors; i++) {
			Player.textures[i + 1] = new PIXI.Texture(
				baseTexture,
				new PIXI.Rectangle(cellSize * i, 0, cellSize, cellSize)
			);
		}
	}


	constructor(game: Game, id: number, color: number, nick: string, position: Vector, direction: Vector) {
		this.game = game;
		this.id = id;
		this.color = color;
		this.nick = nick;
		this.tailColor = this.getTailColor(color);
		this.position = position;
		this.wantDirection = 1;
		this.moveDirection = direction;
		this.inputSended = true;
		this.status = Player.STATUS_PLAYING;
		this.statusTicks = -1;

		this.tail = [];

		this.createGraphics();
	}

	toString(): string {
		return "Player()";
	}

	createGraphics() {
		this.tailGraphics = new PIXI.Graphics();
		this.game.scene.stage.addChild(this.tailGraphics);

		this.sprite = new PIXI.Sprite(Player.textures[this.color]);
		this.sprite.anchor.set(0.5, 0.5);
		this.sprite.position.set(this.position.x, this.position.y);
		this.game.scene.stage.addChild(this.sprite);

		this.nickTexture = this.createNickTexture(this.nick);
		this.nickSprite = new PIXI.Sprite(this.nickTexture);
		this.nickSprite.anchor.set(0.5, 0.5);
		this.nickSprite.position.set(this.position.x, this.position.y + 40);
		this.game.scene.stage.addChild(this.nickSprite);
	}

	createNickTexture(nick: string): PIXI.Texture {
		var canvas = document.createElement("canvas");
		var ctx = canvas.getContext("2d");
		var width = 140;
		var height = 20;
		var lineHeight = 16;
		canvas.width = width;
		canvas.height = height;

		ctx.font = lineHeight + "px Arial";
		var nickSize = ctx.measureText(nick).width;
		var startPos = (width - nickSize) * 0.5;
		ctx.fillStyle = "#555";
		ctx.fillText(nick, startPos + 1, lineHeight + 1);
		ctx.fillStyle = "#EEEEEE";
		ctx.fillText(nick, startPos, lineHeight);

		var baseTexture = PIXI.BaseTexture.fromCanvas(canvas);
		return new PIXI.Texture(baseTexture, new PIXI.Rectangle(0, 0, width, height));
	}

	tick(timer: Timer): boolean {
		if (this.status === Player.STATUS_PLAYING) {
			var fps = 10;
			var cellUpdateTime = 2;
			var speed = fps / cellUpdateTime * 0.001 * Game.cellSize;
			this.position.addScaled(this.moveDirection, speed * timer.tickDelta);
			this.sprite.position.set(this.position.x, this.position.y);
			this.nickSprite.position.set(this.position.x, this.position.y + 40);
			if (this.tail.length >= 2) {
				this.tail[this.tail.length - 1].setFrom(this.position);
			}
			this.drawTail();
		} else if (this.status === Player.STATUS_DIED) {
			this.statusTicks -= 1;
			if (this.statusTicks <= 0) {
				this.destroy();
			}
			this.sprite.alpha -= 1 / Player.DIED_TICKS;
			this.tailGraphics.alpha -= 1 / Player.DIED_TICKS;
		}
		return this.status !== Player.STATUS_DESTROYED;
	}

	drawTail() {
		if (this.tail.length > 1) {
			var gr = this.tailGraphics;
			var start = this.tail[0];
			gr.clear();
			gr.lineStyle(20, this.tailColor, 1);
			gr.moveTo(start.x, start.y);
			for (var i = 1; i < this.tail.length; i++) {
				var t = this.tail[i];
				gr.lineTo(t.x, t.y);
			}
			gr.endFill();
		}
	}

	getTailColor(color: number): number {
		var hue = (color - 1) / Game.colors;
		var rgb = Color.hsvToRgb(hue, 1, 0.7);
		return rgb[2] + rgb[1]*256 + rgb[0]*256*256;
	}

	wantMove(dir: number) {
		this.wantDirection = dir;
		this.inputSended = false;

		// todo: if player is my, instant change direction, to dont show lag!
	}

	syncMoveDirection(data: ServerData.MoveDirection) {
		var syncPos = new Vector(data.posX * Game.cellSize, data.posY * Game.cellSize);
		var lagDist = syncPos.dist(this.position);
		this.position.setFrom(syncPos);

		var moveAngle = data.moveAngle;
		if (moveAngle === 1) {
			this.moveDirection.set(0, -1);
		} else if (moveAngle === 2) {
			this.moveDirection.set(1, 0);
		} else if (moveAngle === 3) {
			this.moveDirection.set(0, 1);
		} else if (moveAngle === 4) {
			this.moveDirection.set(-1, 0);
		}

		this.position.addScaled(this.moveDirection, lagDist * 0.2);
		if (this.tail.length >= 2) {
			var prevPoint = this.tail[this.tail.length - 2];
			if (!prevPoint.equals(syncPos)) {
				this.tail.splice(this.tail.length - 1, 0, syncPos);
			}
		}
	}

	die() {
		this.status = Player.STATUS_DIED;
		this.statusTicks = Player.DIED_TICKS;
	}

	destroy() {
		this.status = Player.STATUS_DESTROYED;
		this.game.scene.stage.removeChild(this.sprite);
		this.game.scene.stage.removeChild(this.nickSprite);
		this.game.scene.stage.removeChild(this.tailGraphics);

		this.sprite.destroy();
		this.nickSprite.destroy();
		this.tailGraphics.destroy();
	}

	revealTail(data: ServerData.RevealTail) {
		this.clearTail();
		for (var i = 0; i < data.count; i++) {
			var point = data.points[i];
			this.tail.push(point.scale(Game.cellSize));
		}
		this.tail.push(this.position.clone());
	}

	startTail(data: ServerData.TailStart) {
		this.clearTail();
		this.tail.push(new Vector(data.posX * Game.cellSize, data.posY * Game.cellSize));
		this.tail.push(this.position.clone());
	}

	clearTail() {
		this.tail.length = 0;
		this.tailGraphics.clear();
	}

	hide() {
		this.sprite.visible = false;
		this.nickSprite.visible = false;
		this.tailGraphics.visible = false;
		this.status = Player.STATUS_HIDDEN;
	}

	reveal(data: ServerData.NewPlayer) {
		this.color = data.color;
		this.position.set(data.posX * Game.cellSize, data.posY * Game.cellSize);
		this.wantDirection = 1;
		this.moveDirection.set(data.dirX, data.dirY);
		this.status = Player.STATUS_PLAYING;
		this.statusTicks = -1;

		this.sprite.visible = true;
		this.nickSprite.visible = true;
		this.tailGraphics.visible = true;
		this.clearTail();
	}

}

Player.generateTextures();