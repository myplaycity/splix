
class Template {

	__func: Function = null;

	constructor(html: string) {
		this.make(html);
	}

	make(html: string) {
		var re = /{([^}]+)?}/g,
			code = "var r=[];\n",
			cursor = 0,
			match;

		var add = function(line: string, js: boolean): Function {
			if (js) {
				code += "r.push(" + line + ");\n";
			} else {
				code += line != "" ? "r.push(\"" + line.replace(/"/g, "\\\"") + "\");\n" : "";
			}
			return add;
		}

		while (match = re.exec(html)) {
			add(html.slice(cursor, match.index), false)(match[1], true);
			cursor = match.index + match[0].length;
		}

		add(html.substr(cursor, html.length - cursor), false);
		code += "return r.join(\"\");";
		this.__func = new Function(code.replace(/[\r\t\n]/g, ""));
	}

	render(data: Data) {
		return this.__func.apply(data);
	}

}
