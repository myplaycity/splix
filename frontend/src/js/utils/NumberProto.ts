
interface Number {
	clamp(min: number, max: number): number;
	clampInt(min: number, max: number): number;
	smoothWith(dst: number, smoothing: number): number;
	randIntTo(a: number): number;
	randFloatTo(a: number): number;
}

Number.prototype.clamp = function(min: number, max: number): number {
	if (this < min) return min;
	if (this > max) return max;
	return this;
}

Number.prototype.clampInt = function(min: number, max: number): number {
	if (this < min) return min;
	if (this > max) return max;
	return Math.floor(this);
}

Number.prototype.smoothWith = function(dst: number, smoothing: number): number {
	return (dst * smoothing) + (this * (1.0 - smoothing));
};

