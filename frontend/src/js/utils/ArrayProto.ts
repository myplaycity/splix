
interface Array<T> {
	remove(item: T): Array<T>;
	has(item: T): boolean;
	pushUnique(item: T): Array<T>;
}


Array.prototype.remove = function(item) {
	var index = this.indexOf(item);
	if (index !== -1) this.splice(index, 1);
	return this;
};

Array.prototype.has = function(item) {
	return this.indexOf(item) > -1;
};

Array.prototype.pushUnique = function(item) {
	if (!this.has(item)) {
		this.push(item);
	}
	return this;
};