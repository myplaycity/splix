
// improve HTMLElement prototype

interface HTMLElement {
	addClass(className: string): HTMLElement;
	removeClass(className: string): HTMLElement;
	hasClass(className: string): boolean;
	toggleClass(className: string, value: boolean): HTMLElement;
	showThen(value: boolean): HTMLElement;

	attr(name: string, value?: string): string|HTMLElement;

	bind(eventName: string, func: (event: Event) => any): HTMLElement;

    find(selector: string): NodeList;
    findOne(selector: string): HTMLElement;

    append(htmlString: string): HTMLElement;
    isDescendantOf(parentElement: HTMLElement): boolean;
    findParentWithClass(className: string): HTMLElement;
    offset(): [number, number];

    getSize(): HTMLElementSize;
}


HTMLElement.prototype.addClass = function(className: string): HTMLElement {
	this.classList.add(className);
	return this;
}

HTMLElement.prototype.removeClass = function(className: string): HTMLElement {
	this.classList.remove(className);
	return this;
}

HTMLElement.prototype.hasClass = function(className: string): boolean {
	return this.classList.contains(className);
}

HTMLElement.prototype.toggleClass = function(className: string, value: boolean): HTMLElement {
	if (value) {
		this.classList.add(className);
	} else {
		this.classList.remove(className);
	}
	return this;
}

HTMLElement.prototype.showThen = function(value: boolean): HTMLElement {
	if (value) {
		this.classList.remove("_hidden");
	} else {
		this.classList.add("_hidden");
	}
	return this;
}

HTMLElement.prototype.attr = function(name: string, value?: string): any {
	if (value !== undefined) {
		this.setAttribute(name, value);
		return this;
	} else {
		return this.getAttribute(name);
	}
}

HTMLElement.prototype.bind = function(eventName: string, callback: (event: Event) => any) {
	this.addEventListener(eventName, callback, false);
	return this;
}

HTMLElement.prototype.find = function(selector: string): NodeList {
	return this.querySelectorAll(selector);
}

HTMLElement.prototype.findOne = function(selector: string): HTMLElement {
	return this.querySelector(selector);
}

HTMLElement.prototype.append = function(htmlString: string): HTMLElement {
	var node = document.createElement("DIV");
	node.innerHTML = htmlString;
	var nodes = node.children;
	var length = nodes.length;

	for (var i = 0; i < length; i++) {
		this.appendChild(nodes[0]);
	}
	return this;
}

HTMLElement.prototype.isDescendantOf = function(parentElement: HTMLElement): boolean {
	var node = this.parentElement;
	while (node != null) {
		if (node == parentElement) {
			return true;
		}
		node = node.parentElement;
	}
	return false;
}

HTMLElement.prototype.findParentWithClass = function(className: string): HTMLElement {
	var node = this;
	while (node) {
		if (node.hasClass(className)) {
			return node;
		}
		node = node.parentElement;
	}
	return null;
}

HTMLElement.prototype.offset = function(): [number, number] {
	var top = 0,
		left = 0,
		elem = this;

	while (elem) {
		top = top + parseFloat(elem.offsetTop);
		left = left + parseFloat(elem.offsetLeft);
		elem = elem.offsetParent;
	}
	return [ Math.round(left), Math.round(top) ];
}

interface HTMLElementSize {
	width: number;
	height: number;
}

HTMLElement.prototype.getSize = function(): HTMLElementSize {
	return {
		width: this.clientWidth,
		height: this.clientHeight
	};
}