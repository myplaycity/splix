
interface Event {
	[name: string]: any;
	source: any;
}


interface Data {
	[name: string]: any;
}

interface HTMLElement {
	__controller: Controller;
}

interface EventCallback {
	(event: Event): any;
}


class Controller {

	dom: HTMLElement = null;
	[name: string]: any;
	parent: Controller = null;

	constructor(dom: HTMLElement, parent: Controller) {
		this.dom = dom;
		this.parent = parent;
		this.init();
		dom.__controller = this;
	}

	init(): void {
		var nodesWithId = this.dom.find("[id]");
		for (var i = 0; i < nodesWithId.length; i++) {
			var node = nodesWithId[i] as HTMLElement;
			var id = node.getAttribute("id");
			this[id] = node;
		}

		var nodesWithName = this.dom.find("[name]");
		for (var i = 0; i < nodesWithName.length; i++) {
			var node = nodesWithName[i] as HTMLElement;
			var name = node.getAttribute("name");
			this[name] = node;
		}
	}

	update(event?: Event): void {
	}

	dispatch(name: string, data?: Data): void {
		var event = new Event(name);
		if (data !== undefined) {
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					event[key] = data[key];
				}
			}
		}
		event.source = this;
		document.dispatchEvent(event);
	}

	listen(eventNames: string[]): void {
		var updateCall = this.update.bind(this);
		for (var i = 0; i < eventNames.length; i++) {
			document.addEventListener(eventNames[i], updateCall, false);
		}
	}

	on(eventName: string, action: EventCallback) {
		var eventCallback = (function(event: Event) {
			if (action.call(this, event) !== false) {
				this.update();
			}
		}).bind(this);
		document.addEventListener(eventName, eventCallback, false);
	}

	static mountUI(): void {
		var nodes = document.querySelectorAll("[controller]");
		var win = window as Data;

		for (var i = 0; i < nodes.length; i++) {
			var node = nodes[i] as HTMLElement;
			var nodeName = node.getAttribute("id");
			var controllerClass = node.getAttribute("controller");

			if (win[controllerClass] === undefined) {
				throw new Error("undeclared controller " + controllerClass + " on element " + node.toString());
			}

			var parent = Controller.findParentController(node);
			win[nodeName] = new win[controllerClass](node, parent);
		}
	}

	static findParentController(node: HTMLElement): Controller {
		while (node) {
			if (node.__controller) {
				return node.__controller;
			}
			node = node.parentElement;
		}
		return null;
	}

}
