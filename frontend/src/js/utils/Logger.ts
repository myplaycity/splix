
class Logger {

	name: string = "";

	constructor(name: string) {
		this.name = name;
	}

	getTime() {
		return "2016-10-10 00:00";
	}

	info(message: string, ...objects: Object[]) {
		var time = this.getTime();
		var args = [
			// "%c" + time + " %c[" + this.name + "]%c " + message,
			"%c[" + this.name + "]%c " + message,
			// "color: #555555;",
			"color: #005FFF;",
			"color: #222222;"
		];
		args.push.apply(args, objects);
		console.log.apply(console, args);
	}

	debug(message: string, ...objects: Object[]) {
		var time = this.getTime();
		var args = [
			// "%c" + time + " %c[" + this.name + "]%c " + message,
			"%c[" + this.name + "]%c " + message,
			// "color: #555555;",
			"color: #FF6400; font-weight: bold",
			"color: #FF6400; font-weight: bold"
		];
		args.push.apply(args, objects);
		console.log.apply(console, args);
	}

	error(message: string, error?: Error) {
		var time = this.getTime();
		var args = [
			"%c" + time + " %c[" + this.name + "]%c " + message,
			"color: #555555;",
			"color: #FF0800; font-weight: bold",
			"color: #FF0800; font-weight: bold"
		];
		if (error) {
			args.push(error.stack);
		}
		console.log.apply(console, args);
	}

}
