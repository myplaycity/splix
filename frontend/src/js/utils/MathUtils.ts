
class MathUtils {

	static randInt(a: number, b: number): number {
		return Math.floor(Math.random() * (b - a + 1)) + a;
	}

	static randFloat(a: number, b: number): number {
		return Math.random() * (b - a) + a;
	}

}