
class Timer {

	enabled: boolean = false;
	delta: number = 0;
	callback: (timer: Timer) => any = null;
	interval: number = null;

	time: number = 0;
	tickDelta: number = 0;
	ticks: number = 0;
	fps: number = 0;

	constructor(delta: number, callback: (timer: Timer) => any) {
		this.delta = delta;
		this.callback = callback;
		this.run = this.run.bind(this);
		this.start();
	}

	start() {
		this.enabled = true;
		this.time = Date.now();

		if (this.delta > 0) {
			this.interval = setInterval(this.run, this.delta);
		} else if (this.delta === 0) {
			requestAnimationFrame(this.run);
		}
	}

	run() {
		if (!this.enabled) {
			return false;
		}

		var time = Date.now();
		this.tickDelta = time - this.time;
		this.time = time;
		this.ticks++;
		this.checkFPS(this.tickDelta);
		this.callback(this);

		if (this.delta === 0 && this.enabled === true) {
			requestAnimationFrame(this.run);
		}
	}

	stop() {
		this.enabled = false;
		if (this.interval !== null) {
			clearInterval(this.interval);
		}
	}

	checkFPS(delta: number) {
		if (delta > 0) {
			var smoothing = 0.05;
			this.fps = (1000 / delta * smoothing) + (this.fps * (1.0 - smoothing));
		}
	}

	static startRAF(callback: (timer: Timer) => any) {
		return new Timer(0, callback);
	}

	static startFPS(fps: number, callback: (timer: Timer) => any) {
		return new Timer(1000 / fps, callback);
	}

	static startTimeout(delta: number, callback: (timer: Timer) => any) {
		return new Timer(delta, callback);
	}

}
