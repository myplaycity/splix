
class GameserverData {

	adress: string;
	ws: string;
	name: string;
	players: number;

	constructor(apiServerString: string) {
		var data = JSON.parse(apiServerString);
		this.adress = data.adress;
		this.name = data.name;
		this.ws = data.ws;
		this.players = data.players;
	}

	toString(): string {
		return "GameserverData(name: " + this.name + ", ws: " + this.ws + ", players: " + this.players + ")";
	}

}