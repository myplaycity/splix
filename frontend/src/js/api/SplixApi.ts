

interface AdressCallback {
    (server: GameserverData): any;
}

interface AuthCallback {
	(userData: UserData): any;
}

interface UserData {
	id: number,
	hash: string,
	name: string
}

interface ApiCallback {
	(responseText: string): any;
}


class SplixApi {

	static apiAdress = "__API__";

	private static request(url: string, callback: ApiCallback) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", encodeURI(SplixApi.apiAdress + url), true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				callback(xhr.responseText);
			} else {
				console.error(xhr.responseText);
			}
		};
		xhr.onerror = function(err: ErrorEvent) {
			console.error(err);
		};
		xhr.send();
	}

	static getBestServer(serverType: string, success: AdressCallback) {
		SplixApi.request("/balancer/bestServer?serverType=" + serverType, function(responseText: string) {
			var result = new GameserverData(responseText);
			success(result);
		});
	}

	static auth(success: AuthCallback) {
		SplixApi.request("/api/auth", function(responseText: string) {
			var response = JSON.parse(responseText);
			success(response.user as UserData);
		});
	}

}