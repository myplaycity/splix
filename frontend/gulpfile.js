var
	gulp = require("gulp"),
	runSequence = require("run-sequence"),
	concat = require("gulp-concat"),
	less = require("gulp-less"),
	clean = require("gulp-clean"),
	replace = require("gulp-replace"),
	batchReplace = require("gulp-batch-replace"),
	livereload = require("gulp-livereload"),
	mustache = require("gulp-mustache"),
	autoprefixer = require("gulp-autoprefixer"),
	rename = require("gulp-rename"),
	ts = require("gulp-typescript");

/**
 * Gulp will use this timestamp to make xx.{time}.css xx.{time}.js minified files
 * For cache busting browser after deploy.
 *
 * Alse, timestamp used to get fresh gamedata from api.
 */

var timestamp = Math.floor(Date.now() * 0.001);

function swallowError(error) {
	console.log(error.toString());
	this.emit("end");
}

// DEPLOY CONFIG use this to change way of deploy

var deployMode = null;
var deployConfig = {
	local: {
		replace: [
			["__API__", "http://local.splix.io"]
		]
	},
	dev: {
		replace: [
			["__API__", "http://dev.splix.sliz.io"]
		]
	}
}
var env = {};


// SUBTASKS

gulp.task("clean", function () {
	return gulp.src("public", { read: false })
		.pipe(clean());
});

gulp.task("make-js", function() {
	return gulp.src([
			"src/js/graphics/*",
			"src/js/utils/*",
			"src/js/api/*",
			"src/js/ui/*",
			// "src/js/game/*"
			"src/js/game/Bounds.ts",
			"src/js/game/Vector.ts",
			"src/js/game/Camera.ts",
			"src/js/game/ClientMessage.ts",
			"src/js/game/ServerMessage.ts",
			"src/js/game/Client.ts",
			"src/js/game/Player.ts",
			"src/js/game/Players.ts",
			"src/js/game/Scene.ts",
			"src/js/game/GameScreen.ts",
			"src/js/game/SplixField.ts",
			"src/js/game/SplixFieldPoint.ts",
			"src/js/game/SplixQuad.ts",
			"src/js/game/Tower.ts",
			"src/js/game/Towers.ts",
			"src/js/game/Mine.ts"
		])
		.pipe(ts({
			noImplicitAny: true,
			out: "main." + timestamp + ".js"
		}))
		.pipe(batchReplace(env.replace))
		.pipe(gulp.dest("public/js"));
});

gulp.task("make-vendor-js", function() {
	return gulp.src([
			"src/js/vendor/*",
		])
		.pipe(concat("vendor." + timestamp + ".js"))
		.pipe(gulp.dest("public/js"));
});

gulp.task("make-css", function () {
	return gulp.src("src/css/Index.less")
		.pipe(less({ paths: ["src/css"] }))
		.on("error", swallowError)
		.pipe(autoprefixer({
			browsers: ["last 2 versions"],
			cascade: false
		}))
		.on("error", swallowError)
		.pipe(rename("index." + timestamp + ".css"))
		.pipe(gulp.dest("public/css"));
});

gulp.task("make-img", function () {
	return gulp.src("src/img/**/*")
		.pipe(gulp.dest("public/img"));
});

gulp.task("make-html", function () {
	gulp.src("src/*.html")
		.pipe(mustache({ timestamp: timestamp }))
		.pipe(batchReplace(env.replace))
		.pipe(gulp.dest("public"));
});


// main deploy tasks ====================================

var deployTasks = ["make-js", "make-vendor-js", "make-css", "make-img", "make-html"];

gulp.task("default", function () {
	env = deployConfig.local;
	runSequence("clean", deployTasks, "watch");
});

gulp.task("dev", function () {
	env = deployConfig.dev;
	runSequence("clean", deployTasks);
});

// special watch task ====================================

gulp.task("watch", function() {
	gulp.watch("src/**/*.*", function() {
		console.log("\033c");
		console.log("deploy...");
	});

	gulp.watch("src/css/**/*.*", ["make-css"]);
	gulp.watch("src/js/**/*.*", ["make-js"]);
	gulp.watch("src/img/**/*.*", ["make-img"]);
	gulp.watch("src/**/*.html", ["make-html"]);

	// setup livereload
	var timer = null;
	var updateLivereload = function() {
		var reload_args = arguments;
		if (timer) clearTimeout(timer);
		if (!gulp.isRunning) {
			timer = setTimeout(function() {
				livereload.reload();
			}, 200);
		}
	}
	livereload.listen();

	// watch on public change
	gulp.watch("public/**/*.*").on("change", updateLivereload);
});
