
package engine.games.mines;


import engine.utils.Logger;
import engine.player.Player;
import engine.player.PlayerView;


public class PlayerViewMines extends PlayerView {

	private static final Logger logger = new Logger("PlayerViewMines", Logger.DEBUG);

	public PlayerViewMines(Player player) {
		super(player);
	}

	@Override
	protected final void revealField() {
		newViewedPlayers.clear();

		for (int y = viewQuadArea.y1; y <= viewQuadArea.y2; y++) {
			for (int x = viewQuadArea.x1; x <= viewQuadArea.x2; x++) {
				QuadMines quad = (QuadMines) player.room.field.getQuad(x, y);
				if (player.isConnected) {
					boolean thisQuadWasUnseen = (quadChanged && !oldViewQuadArea.has(x, y));
					if (quad.fillChanged || thisQuadWasUnseen) {
						revealQuadFill(quad);
						revealMines(quad, player);
					}
				}
				newViewedPlayers.addAll(quad.players);
			}
		}
	}

	private void revealMines(QuadMines quad, Player player) {
		if (quad.towers.size() > 0) {
			for (Mine tower : quad.towers) {
				player.connection.state.newMine(tower);
			}
		}
	}

}