
package engine.games.mines;


import engine.utils.Logger;
import engine.player.Player;
import engine.room.Room;


public class PlayerMines extends Player {

	private static final Logger logger = new Logger("PlayerMines", Logger.DEBUG);

	public PlayerMines(int id, Room room) {
		super(id, room);
		score = new PlayerScoreMines(this);
		view = new PlayerViewMines(this);
	}

	@Override
	public void gameover() {
		if (isConnected) {
			PlayerScoreMines score = (PlayerScoreMines) this.score;
			connection.state.gameOverMines(score.capturedPercent, score.kills, score.mined, score.total, score.k1, score.k2, score.k3);
			connection.sendState();
		}
	}

	@Override
	protected void travel() {
		if (cellMoveValue == Player.HALF_MOVE && targetCell != null) {
			if (targetCell.traveler != null) {
				if (targetCell.traveler != this) {
					targetCell.traveler.die();
					score.addKill();
				} else if (targetCell.traveler == this) {
					die();
				}
			}

			// die if cell have tower
			if (((CellMines) targetCell).tower != null) {
				die();
			}

			if (targetCell.owner == this && nowCell.owner != this) {
				int capturedSpace = room.field.travelCapture(this, territoryBounds);
				score.addCapture(capturedSpace);
				tail.clear();

			} else if (targetCell.owner != this) {
				targetCell.traveler = this;
				territoryBounds.extend(targetCell.x, targetCell.y);
			}
		}
	}

}