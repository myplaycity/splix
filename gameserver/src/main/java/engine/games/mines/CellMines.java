
package engine.games.mines;


import engine.room.Cell;
import engine.room.Quad;
import engine.player.Player;


public class CellMines extends Cell {

	Mine tower = null;

	public CellMines(int x, int y, Quad quad) {
		super(x, y, quad);
	}

	public void setMine(Mine tower) {
		this.tower = tower;
	}

	@Override
	public void capture(Player newOwner) {
		owner = newOwner;
		color = newOwner.color;
		quad.fillChanged = true;

		if (tower != null) {
			tower.capturedBy((PlayerMines) newOwner);
		}
	}

}