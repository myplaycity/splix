
package engine.games.mines;


import java.util.List;
import java.util.ArrayList;
import engine.utils.Logger;
import engine.player.PlayerScore;
import engine.player.Player;


public class PlayerScoreMines extends PlayerScore {

	private static final Logger logger = new Logger("PlayerScoreMines", Logger.DEBUG);

	public int capturedPercent;
	public int mined;
	public int k1;
	public int k2;
	public int k3;

	private List<Mine> mines;
	private List<Mine> removedMines;

	public PlayerScoreMines(Player player) {
		super(player);
		capturedPercent = 0;
		mined = 0;
		k1 = player.room.settings.scorePerCapturedPercent;
		k2 = player.room.settings.scorePerKill;
		k3 = player.room.settings.scorePerMine;
		mines = new ArrayList<Mine>();
		removedMines = new ArrayList<Mine>();
	}

	@Override
	public void addCapture(int captured) {
		blocks += captured;
		capturedPercent = blocks * 10000 / player.room.field.space;
		if (player.isConnected) {
			player.connection.state.earnScoreCapture(captured);
		}
		recalcTotal();
	}

	public void addMine(Mine mine) {
		mines.add(mine);
		logger.log("add mine", mine);
	}

	public void removeMine(Mine mine) {
		mines.remove(mine);
		logger.log("remove mine", mine);
	}

	@Override
	public final void tick() {
		ticks += 1;
		if (player.isConnected) {
			if (ticks % fps == 0) {
				sendRating();
			}
			if (ticks % (fps * 2) == 0) { // every 5 seconds start send top10 table
				startSendTop10();
			}
			if (top10Sending >= 0) {
				sendTop10();
			}
		}

		if (ticks % (fps * player.room.settings.mineTime) == 0) {
			lootMines();
		}
	}

	private void lootMines() {
		int capturedMines = mines.size();
		if (capturedMines > 0) {
			for (int i = capturedMines - 1; i >= 0; i--) {
				Mine mine = mines.get(i);
				mine.takeCharge();
				if (player.isConnected) {
					player.connection.state.mineCharge(mine.id, mine.charges);
				}
			}
			mined += capturedMines;
		}
		recalcTotal();
		if (player.isConnected && capturedMines > 0) {
			player.connection.state.earnScoreMineCapture(capturedMines);
		}
	}

	@Override
	public void sendTop10() {
		if (top10Sending > 9 || top10Sending > player.room.players.size() - 1) {
			stopSendTop10();
			return;
		}
		Player top10player = player.room.players.get(top10Sending);
		player.connection.state.top10LineOriginal(top10player, top10player.score.compareTopValue());
		top10Sending += 1;
	}

	@Override
	public void recalcTotal() {
		total = capturedPercent * k1 / 100  + kills * k2 + mined * k3;
	}

	@Override
	public int compareTopValue() {
		return capturedPercent;
	}

}