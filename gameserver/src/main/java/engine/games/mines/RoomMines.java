
package engine.games.mines;


import engine.utils.Logger;
import engine.room.Room;
import engine.room.Players;
import engine.settings.Settings;


public class RoomMines extends Room {

	private static final Logger logger = new Logger("RoomMines", Logger.NORMAL);

	public final Mines towers;

	public RoomMines(Settings settings) throws Exception {
		super(settings);
		players = new PlayersMines(this);
		field = new FieldMines(this, settings.sizeX, settings.sizeY, settings.quadSize);
		towers = new Mines((FieldMines) field, settings.maxMines);
	}

	@Override
	public boolean tick(long ticks, double time, double tickDelta) {
		if (ticks % (fps * 5) == 0) {
			logger.log("tick: " +  ticks + ", free quads: " + getFreeQuads());
		}

		boolean isFirstSecondTick = ticks % fps == 0;

		try {
			players.includeNewPlayers();
			players.updateView();
			field.updated();
			bots.balance();
			towers.balance();
			players.tick(ticks, time, tickDelta);
			players.sendState();
			players.sortByScores();

			if (isFirstSecondTick) { // every second send player top status
				saveStatus();
			}

		} catch (Exception exc) {
			logger.error("room tick exception: ", exc);
		}

		return true;
	}

}