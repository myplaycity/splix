
package engine.games.mines;


import java.util.ArrayList;
import engine.room.Quad;
import engine.room.Field;


public class QuadMines extends Quad {

	public final ArrayList<Mine> towers;

	public QuadMines(Field field, int id, int x, int y, int quadSize) {
		super(field, id, x, y, quadSize);
		towers = new ArrayList<Mine>();
	}

	public final void addMine(Mine tower) {
		towers.add(tower);
	}

	public final void removeMine(Mine tower) {
		towers.remove(tower);
	}

}