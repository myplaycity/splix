
package engine.games.mines;


import engine.utils.Logger;
import java.util.ArrayList;
import engine.math.Bounds;
import engine.player.Player;


public class Mine {

	private static final Logger logger = new Logger("Mine", Logger.DEBUG);

	private Mines mines;
	public final int id;
	public final int x;
	public final int y;
	public Bounds bounds;
	public QuadMines quad;

	public Player owner;
	public int charges;

	public Mine(Mines mines, int id, int x, int y) {
		this.mines = mines;
		this.id = id;
		this.x = x;
		this.y = y;
		bounds = new Bounds(x, y, x + 1, y + 1);
		charges = mines.field.room.settings.mineCharges;
		attachToPoints();
		attachToQuad();
	}

	public String toString() {
		return "Mine(" + id + ", x: " + x + ", y: " + y + ", bounds: " + bounds + ")";
	}

	public void attachToPoints() {
		bounds.eachPoint((int x, int y) -> {
			mines.field.getPoint(x, y).setMine(this);
		});
	}

	public void releasePoints() {
		bounds.eachPoint((int x, int y) -> {
			mines.field.getPoint(x, y).setMine(null);
		});
	}

	public void attachToQuad() {
		QuadMines quad = (QuadMines) mines.field.getQuadByPoint(x, y);
		quad.addMine(this);
		this.quad = quad;
	}

	public void releaseQuad() {
		quad.removeMine(this);
		quad = null;
	}

	public void capturedBy(PlayerMines player) {
		if (owner != player) {
			if (owner != null) {
				((PlayerScoreMines) owner.score).removeMine(this);
			}
			owner = player;
			((PlayerScoreMines) owner.score).addMine(this);
		}
	}

	public void takeCharge() {
		charges -= 1;
		if (charges == 0) {
			exhaust();
		}
	}

	public void exhaust() {
		if (owner != null) {
			((PlayerScoreMines) owner.score).removeMine(this);
		}
		releasePoints();
		releaseQuad();
		mines.remove(this);
	}

}