
package engine.games.mines;


import java.util.HashSet;
import java.util.ArrayList;
import engine.math.Point;
import engine.math.MathUtils;
import engine.utils.Logger;
import engine.player.Player;


public class Mines {

	private static final Logger logger = new Logger("Mines", Logger.DEBUG);

	private ArrayList<Mine> list;
	public FieldMines field;
	public int index = 1;
	public int count = 0;
	public int maxCount = 0;

	public Mines(FieldMines field, int maxCount) {
		this.field = field;
		this.maxCount = maxCount;
		list = new ArrayList<Mine>(maxCount);
	}

	public String toString() {
		return "Mines(count: " + count + ", maxCount: " + maxCount + ")";
	}

	public void create() {
		// int padding = 5;
		// int randomX = MathUtils.randInt(padding, field.sizeX - padding);
		// int randomY = MathUtils.randInt(padding, field.sizeY - padding);

		Point randomPos = field.pickFreePosition(2);

		Mine mine = new Mine(this, index++, randomPos.x, randomPos.y);
		list.add(mine);
		count++;

		broadcastNewMine(mine);
	}

	public void remove(Mine mine) {
		list.remove(mine);
		count--;
		broadcastRemovedMine(mine);
	}

	public void balance() {
		if (count < maxCount) {
			create();
		}
	}

	public void broadcastNewMine(Mine mine) {
		HashSet<Player> viewers = new HashSet<Player>();
		field.getViewers(mine.x, mine.y, viewers);
		for (Player viewer : viewers) {
			if (viewer.isConnected) {
				viewer.connection.state.newMine(mine);
			}
		}
	}

	public void broadcastRemovedMine(Mine mine) {
		HashSet<Player> viewers = new HashSet<Player>();
		field.getViewers(mine.x, mine.y, viewers);
		for (Player viewer : viewers) {
			if (viewer.isConnected) {
				viewer.connection.state.removeMine(mine);
			}
		}
	}

}