
package engine.games.towers;


import engine.utils.Logger;
import java.util.ArrayList;
import engine.math.Bounds;


public class Tower {

	private static final Logger logger = new Logger("Tower", Logger.DEBUG);

	private Towers towers;
	public final int id;
	public final int x;
	public final int y;
	public Bounds bounds;
	public QuadTowers quad;

	public Tower(Towers towers, int id, int x, int y) {
		this.towers = towers;
		this.id = id;
		this.x = x;
		this.y = y;
		bounds = new Bounds(x, y, x + 1, y + 1);
		attachToPoints();
		attachToQuad();
	}

	public String toString() {
		return "Tower(" + id + ", x: " + x + ", y: " + y + ", bounds: " + bounds + ")";
	}

	public void attachToPoints() {
		bounds.eachPoint((int x, int y) -> {
			towers.field.getPoint(x, y).setTower(this);
		});
	}

	public void releasePoints() {
		bounds.eachPoint((int x, int y) -> {
			towers.field.getPoint(x, y).setTower(null);
		});
	}

	public void attachToQuad() {
		QuadTowers quad = (QuadTowers) towers.field.getQuadByPoint(x, y);
		quad.addTower(this);
		this.quad = quad;
	}

	public void releaseQuad() {
		quad.removeTower(this);
		quad = null;
	}

	public void capturedBy(PlayerTowers player) {
		releasePoints();
		releaseQuad();
		towers.remove(this);
		((PlayerScoreTowers) player.score).addTowerCapture();
	}

}