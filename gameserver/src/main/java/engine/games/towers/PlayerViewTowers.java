
package engine.games.towers;


import engine.utils.Logger;
import engine.player.Player;
import engine.player.PlayerView;


public class PlayerViewTowers extends PlayerView {

	private static final Logger logger = new Logger("PlayerViewTowers", Logger.DEBUG);

	public PlayerViewTowers(Player player) {
		super(player);
	}

	@Override
	protected final void revealField() {
		newViewedPlayers.clear();

		for (int y = viewQuadArea.y1; y <= viewQuadArea.y2; y++) {
			for (int x = viewQuadArea.x1; x <= viewQuadArea.x2; x++) {
				QuadTowers quad = (QuadTowers) player.room.field.getQuad(x, y);
				if (player.isConnected) {
					boolean thisQuadWasUnseen = (quadChanged && !oldViewQuadArea.has(x, y));
					if (quad.fillChanged || thisQuadWasUnseen) {
						revealQuadFill(quad);
						revealTowers(quad, player);
					}
				}
				newViewedPlayers.addAll(quad.players);
			}
		}
	}

	private void revealTowers(QuadTowers quad, Player player) {
		if (quad.towers.size() > 0) {
			for (Tower tower : quad.towers) {
				player.connection.state.newTower(tower);
			}
		}
	}

}