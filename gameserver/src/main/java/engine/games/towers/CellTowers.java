
package engine.games.towers;


import engine.room.Cell;
import engine.room.Quad;
import engine.player.Player;


public class CellTowers extends Cell {

	Tower tower = null;

	public CellTowers(int x, int y, Quad quad) {
		super(x, y, quad);
	}

	public void setTower(Tower tower) {
		this.tower = tower;
	}

	@Override
	public void capture(Player newOwner) {
		owner = newOwner;
		color = newOwner.color;
		quad.fillChanged = true;

		if (tower != null) {
			tower.capturedBy((PlayerTowers) newOwner);
		}
	}

}