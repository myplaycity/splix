
package engine.games.towers;


import engine.utils.Logger;
import engine.player.PlayerScore;
import engine.player.Player;


public class PlayerScoreTowers extends PlayerScore {

	private static final Logger logger = new Logger("PlayerScoreTowers", Logger.DEBUG);

	public int capturedPercent;
	public int towers;
	public int k1;
	public int k2;
	public int k3;

	public PlayerScoreTowers(Player player) {
		super(player);
		capturedPercent = 0;
		towers = 0;
		k1 = player.room.settings.scorePerCapturedPercent;
		k2 = player.room.settings.scorePerKill;
		k3 = player.room.settings.scorePerTower;
	}

	@Override
	public void addCapture(int captured) {
		blocks += captured;
		capturedPercent = blocks * 10000 / player.room.field.space;
		if (player.isConnected) {
			player.connection.state.earnScoreCapture(captured);
		}
		recalcTotal();
	}

	public void addTowerCapture() {
		towers += 1;
		if (player.isConnected) {
			player.connection.state.earnScoreTowerCapture(k3);
		}
		recalcTotal();
	}

	@Override
	public void sendTop10() {
		if (top10Sending > 9 || top10Sending > player.room.players.size() - 1) {
			stopSendTop10();
			return;
		}
		Player top10player = player.room.players.get(top10Sending);
		player.connection.state.top10LineOriginal(top10player, top10player.score.compareTopValue());
		top10Sending += 1;
	}

	@Override
	public void recalcTotal() {
		total = capturedPercent * k1 / 100  + kills * k2 + towers * k3;
	}

	@Override
	public int compareTopValue() {
		return capturedPercent;
	}

}