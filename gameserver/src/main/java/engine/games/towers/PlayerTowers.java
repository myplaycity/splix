
package engine.games.towers;


import engine.utils.Logger;
import engine.player.Player;
import engine.room.Room;


public class PlayerTowers extends Player {

	private static final Logger logger = new Logger("PlayerTowers", Logger.DEBUG);

	public PlayerTowers(int id, Room room) {
		super(id, room);
		score = new PlayerScoreTowers(this);
		view = new PlayerViewTowers(this);
	}

	@Override
	public void gameover() {
		if (isConnected) {
			PlayerScoreTowers score = (PlayerScoreTowers) this.score;
			connection.state.gameOverTowers(score.capturedPercent, score.kills, score.towers, score.total, score.k1, score.k2, score.k3);
			connection.sendState();
		}
	}

	@Override
	protected void travel() {
		if (cellMoveValue == Player.HALF_MOVE && targetCell != null) {
			if (targetCell.traveler != null) {
				if (targetCell.traveler != this) {
					targetCell.traveler.die();
					score.addKill();
				} else if (targetCell.traveler == this) {
					die();
				}
			}

			// die if cell have tower
			if (((CellTowers) targetCell).tower != null) {
				die();
			}

			if (targetCell.owner == this && nowCell.owner != this) {
				int capturedSpace = room.field.travelCapture(this, territoryBounds);
				score.addCapture(capturedSpace);
				tail.clear();

			} else if (targetCell.owner != this) {
				targetCell.traveler = this;
				territoryBounds.extend(targetCell.x, targetCell.y);
			}
		}
	}

}