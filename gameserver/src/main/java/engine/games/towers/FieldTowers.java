
package engine.games.towers;


import java.util.HashSet;
import engine.utils.Logger;
import engine.math.MathUtils;
import engine.room.Field;
import engine.room.Room;
import engine.room.Quad;
import engine.player.Player;


public class FieldTowers extends Field {

	private static final Logger logger = new Logger("FieldTowers", Logger.NORMAL);

	public FieldTowers(Room room, int sizeX, int sizeY, int quadSize) throws Exception {
		super(room, sizeX, sizeY, quadSize);
	}

	@Override
	protected void createPoints() {
		points = new CellTowers[sizeX * sizeY];
		logger.log("generating points: ", sizeX * sizeY);
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				points[y * sizeX + x] = new CellTowers(x, y, getQuadByPoint(x, y));
			}
		}
	}

	@Override
	protected void createQuads() {
		quads = new Quad[quadsX * quadsY];
		logger.log("generating quads: ", quadsX * quadsY);
		for (int y = 0; y < quadsY; y++) {
			for (int x = 0; x < quadsX; x++) {
				int id = y * quadsX + x;
				quads[id] = new QuadTowers(this, id, x, y, quadSize);
			}
		}
	}

	@Override
	public final CellTowers getPoint(int x, int y) {
		if (x >= 0 && x < sizeX && y >= 0 && y < sizeY) {
			return (CellTowers) points[y * sizeX + x];
		}
		return null;
	}

	public void getViewers(int cx, int cy, HashSet<Player> outViewers) {
		int pad = room.settings.viewQuadPadding;
		Quad centerQuad = getQuadByPoint(cx, cy);

		int x1 = MathUtils.clamp(centerQuad.x - pad, 0, quadsX - 1);
		int x2 = MathUtils.clamp(centerQuad.x + pad, 0, quadsX - 1);
		int y1 = MathUtils.clamp(centerQuad.y - pad, 0, quadsY - 1);
		int y2 = MathUtils.clamp(centerQuad.y + pad, 0, quadsY - 1);

		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				outViewers.addAll(getQuad(x, y).viewers);
			}
		}
	}

}