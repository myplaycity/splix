
package engine.games.towers;


import engine.utils.Logger;
import engine.room.Room;
import engine.room.Players;
import engine.settings.Settings;


public class RoomTowers extends Room {

	private static final Logger logger = new Logger("RoomTowers", Logger.NORMAL);

	public final Towers towers;

	public RoomTowers(Settings settings) throws Exception {
		super(settings);
		players = new PlayersTowers(this);
		field = new FieldTowers(this, settings.sizeX, settings.sizeY, settings.quadSize);
		towers = new Towers((FieldTowers) field, settings.maxTowers);
	}

	@Override
	public boolean tick(long ticks, double time, double tickDelta) {
		if (ticks % (fps * 5) == 0) {
			logger.log("tick: " +  ticks + ", free quads: " + getFreeQuads());
		}

		boolean isFirstSecondTick = ticks % fps == 0;

		try {
			players.includeNewPlayers();
			players.updateView();
			field.updated();
			bots.balance();
			towers.balance();
			players.tick(ticks, time, tickDelta);
			players.sendState();
			players.sortByScores();

			if (isFirstSecondTick) { // every second send player top status
				saveStatus();
			}

		} catch (Exception exc) {
			logger.error("room tick exception: ", exc);
		}

		return true;
	}

}