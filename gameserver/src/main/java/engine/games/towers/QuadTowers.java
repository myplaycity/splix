
package engine.games.towers;


import java.util.ArrayList;
import engine.room.Quad;
import engine.room.Field;


public class QuadTowers extends Quad {

	public final ArrayList<Tower> towers;

	public QuadTowers(Field field, int id, int x, int y, int quadSize) {
		super(field, id, x, y, quadSize);
		towers = new ArrayList<Tower>();
	}

	public final void addTower(Tower tower) {
		towers.add(tower);
	}

	public final void removeTower(Tower tower) {
		towers.remove(tower);
	}

}