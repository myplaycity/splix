
package engine.games.towers;


import java.util.HashSet;
import java.util.ArrayList;
import engine.math.Point;
import engine.math.MathUtils;
import engine.utils.Logger;
import engine.player.Player;


public class Towers {

	private static final Logger logger = new Logger("Towers", Logger.DEBUG);

	private ArrayList<Tower> list;
	public FieldTowers field;
	public int index = 1;
	public int count = 0;
	public int maxCount = 0;

	public Towers(FieldTowers field, int maxCount) {
		this.field = field;
		this.maxCount = maxCount;
		list = new ArrayList<Tower>(maxCount);
	}

	public String toString() {
		return "Towers(count: " + count + ", maxCount: " + maxCount + ")";
	}

	public void create() {
		// int padding = 5;
		// int randomX = MathUtils.randInt(padding, field.sizeX - padding);
		// int randomY = MathUtils.randInt(padding, field.sizeY - padding);

		Point randomPos = field.pickFreePosition(2);

		Tower tower = new Tower(this, index++, randomPos.x, randomPos.y);
		list.add(tower);
		logger.log("created ", tower);
		count++;

		broadcastNewTower(tower);
	}

	public void remove(Tower tower) {
		list.remove(tower);
		count--;
		broadcastRemovedTower(tower);
	}

	public void balance() {
		if (count < maxCount) {
			create();
		}
	}

	public void broadcastNewTower(Tower tower) {
		HashSet<Player> viewers = new HashSet<Player>();
		field.getViewers(tower.x, tower.y, viewers);
		for (Player viewer : viewers) {
			if (viewer.isConnected) {
				viewer.connection.state.newTower(tower);
			}
		}
	}

	public void broadcastRemovedTower(Tower tower) {
		HashSet<Player> viewers = new HashSet<Player>();
		field.getViewers(tower.x, tower.y, viewers);
		for (Player viewer : viewers) {
			if (viewer.isConnected) {
				viewer.connection.state.removeTower(tower);
			}
		}
	}

}