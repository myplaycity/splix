
package engine.games.original;


import engine.utils.Logger;
import engine.player.PlayerScore;
import engine.player.Player;


public class PlayerScoreOriginal extends PlayerScore {

	private static final Logger logger = new Logger("PlayerScoreOriginal", Logger.DEBUG);

	public int capturedPercent;
	public int k1;
	public int k2;

	public PlayerScoreOriginal(Player player) {
		super(player);
		capturedPercent = 0;
		k1 = player.room.settings.scorePerCapturedPercent;
		k2 = player.room.settings.scorePerKill;
	}

	@Override
	public void addCapture(int captured) {
		blocks += captured;
		capturedPercent = blocks * 10000 / player.room.field.space;
		if (player.isConnected) {
			player.connection.state.earnScoreCapture(captured);
		}
		recalcTotal();
	}

	@Override
	public void sendTop10() {
		if (top10Sending > 9 || top10Sending > player.room.players.size() - 1) {
			stopSendTop10();
			return;
		}
		Player top10player = player.room.players.get(top10Sending);
		player.connection.state.top10LineOriginal(top10player, top10player.score.compareTopValue());
		top10Sending += 1;
	}

	@Override
	public void recalcTotal() {
		total = capturedPercent * k1 / 100 + kills * k2;
	}

	@Override
	public int compareTopValue() {
		return capturedPercent;
	}

}