
package engine.games.original;


import engine.room.Room;
import engine.room.Players;
import engine.settings.Settings;


public class RoomOriginal extends Room {

	public RoomOriginal(Settings settings) throws Exception {
		super(settings);
		players = new PlayersOriginal(this);
	}

}