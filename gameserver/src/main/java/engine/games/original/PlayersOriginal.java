
package engine.games.original;


import java.util.Comparator;
import java.util.Collections;
import engine.utils.Logger;
import engine.room.Room;
import engine.player.Player;
import engine.server.PlayerConnection;
import engine.room.Players;


public class PlayersOriginal extends Players {

	private static final Logger logger = new Logger("PlayersOriginal");

	public PlayersOriginal(Room room) {
		super(room);
	}

	@Override
	public Player connectNew(PlayerConnection plconn) {
		Player player = new PlayerOriginal(index++, room);
		connected.add(player);
		plconn.onOpen(player);
		logger.log("created ", player);
		return player;
	}

	@Override
	public Player create() {
		Player player = new PlayerOriginal(index++, room);
		connected.add(player);
		logger.log("created ", player);
		return player;
	}

}