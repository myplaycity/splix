
package engine.games.original;


import engine.player.Player;
import engine.room.Room;


public class PlayerOriginal extends Player {

	public PlayerOriginal(int id, Room room) {
		super(id, room);
		score = new PlayerScoreOriginal(this);
	}

	@Override
	public void gameover() {
		if (isConnected) {
			PlayerScoreOriginal score = (PlayerScoreOriginal) this.score;
			connection.state.gameOverOriginal(score.capturedPercent, score.kills, score.total, score.k1, score.k2);
			connection.sendState();
		}
	}

}