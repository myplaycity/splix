
package engine.room;


import engine.player.Player;


public class Cell {

	public final int x;
	public final int y;
	public Player owner;
	public Player traveler;
	public int color;
	protected Quad quad;

	public Cell(int x, int y, Quad quad) {
		this.x = x;
		this.y = y;
		this.quad = quad;
	}

	public final String toString() {
		return "Cell(" + x + ", " + y + ", owner: " + this.owner + ", traveler: " + this.traveler + ")";
	}

	public void capture(Player newOwner) {
		owner = newOwner;
		color = newOwner.color;
		quad.fillChanged = true;
	}

	public final void captureTailPath(Player newOwner) {
		owner = newOwner;
		color = newOwner.color;
		traveler = null;
		quad.fillChanged = true;
	}

	public final void clearTraveler(Player diedPlayer) {
		if (traveler == diedPlayer) {
			traveler = null;
		}
	}

	public final void clearFill(Player diedPlayer) {
		if (owner == diedPlayer) {
			owner = null;
			color = 0;
			quad.fillChanged = true;
		}
	}

}
