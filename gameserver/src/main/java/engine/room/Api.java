
package engine.room;


import engine.utils.Logger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.concurrent.FutureCallback;

import engine.player.PlayerScore;


public class Api {

	// todo: debug undone!
	private static final Logger logger = new Logger("Api");

	private final Room room;

	private ExecutorService threadpool;
	private Async async;

	public Api(Room room) {
		this.room = room;
		threadpool = Executors.newFixedThreadPool(2);
		async = Async.newInstance().use(threadpool);
	}

	public void makeRequest(URI requestURL) {
		final Request request = Request.Get(requestURL);

		Future<Content> future = async.execute(request, new FutureCallback<Content>() {
			public void failed(final Exception e) {
				logger.log("Failed: ", e.getMessage(), request);
			}
			public void completed(final Content content) {
				logger.log("Received: ", content.asString());
			}
			public void cancelled () {
				logger.log("Cancelled");
			}
		});
	}

	public void saveRound(String userHash, String nick, PlayerScore score) {
		URIBuilder builder = new URIBuilder();
		builder.setScheme("http").setHost("local.splix.io").setPath("/api/saveRound");
		builder.setParameter("serverKey", "3utO3NECPt1yEyebvft0");
		builder.setParameter("userHash", userHash);
		builder.setParameter("name", nick);

		builder.setParameter("score", Integer.toString(score.total));
		builder.setParameter("capture", Integer.toString(score.blocks));
		builder.setParameter("kills", Integer.toString(score.kills));
		builder.setParameter("tail", Integer.toString(score.tail));
		builder.setParameter("timeAlive", Integer.toString(score.timeAlive));
		builder.setParameter("timeTop1", Integer.toString(score.timeTop1));

		URI requestURL = null;

		try {
			requestURL = builder.build();
		} catch (URISyntaxException exc) {
			logger.error("fail construct api request url", exc);
		}

		makeRequest(requestURL);
	}

}
