
package engine.room;


import engine.utils.Logger;
import engine.settings.Settings;
import java.io.PrintWriter;
import java.io.IOException;


public class Room {

	private static final Logger logger = new Logger("Room", Logger.NORMAL);

	public final Settings settings;
	public final Api api;

	public Field field;
	public Players players;
	public Bots bots;

	public final int fps;
	private GameLoop gameLoop;

	public Room(Settings settings) throws Exception {
		logger.log("create room: name = " + settings.name + ", type = " +  settings.type);

		this.settings = settings;
		fps = settings.fps;

		field = new Field(this, settings.sizeX, settings.sizeY, settings.quadSize);
		players = new Players(this);
		bots = new Bots(this, settings.bots);
		api = new Api(this);
	}

	public void start() {
		gameLoop = new GameLoop(fps, new GameLoopCallback() {
			public void run(long ticks, double time, double tickDelta) {
				if (!tick(ticks, time, tickDelta)) {
					gameLoop.stop();
				}
			}
		});
		gameLoop.start();
	}

	public int getFreeQuads() {
		int freeQuads = 0;
		for (int y = 0; y < field.quadsY; y++) {
			for (int x = 0; x < field.quadsX; x++) {
				if (field.getQuad(x, y).isFree) {
					freeQuads += 1;
				}
			}
		}
		return freeQuads;
	}

	public boolean tick(long ticks, double time, double tickDelta) {
		if (ticks % (fps * 5) == 0) {
			logger.log("tick: " +  ticks + ", free quads: " + getFreeQuads());
		}

		boolean isFirstSecondTick = ticks % fps == 0;

		try {
			players.includeNewPlayers();
			players.updateView();
			field.updated();
			bots.balance();
			players.tick(ticks, time, tickDelta);
			players.sendState();
			players.sortByScores();

			if (isFirstSecondTick) { // every second send player top status
				saveStatus();
			}

		} catch (Exception exc) {
			logger.error("room tick exception: ", exc);
		}

		return true;
	}

	public void saveStatus() {
		if (settings.statusPath == "") {
			return;
		}

		try {
			PrintWriter writer = new PrintWriter(settings.statusPath, "UTF-8");
			writer.println("name = " + settings.name);
			writer.println("type = " + settings.type);
			writer.println("ws = " + settings.wsPath);
			writer.println("players = " + players.size());
			writer.close();
		} catch (IOException exc) {
			logger.error("save status fail: " + settings.statusPath);
			logger.error("exception: ", exc);
		}
	}

}
