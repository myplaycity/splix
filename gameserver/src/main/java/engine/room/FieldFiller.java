
package engine.room;


import engine.utils.Logger;
import engine.math.Point;
import engine.math.Bounds;
import engine.player.Player;


public final class FieldFiller {

	private static final Logger logger = new Logger("FieldFiller", Logger.DEBUG);

	private final Field field;
	private int sizeX;
	private int sizeY;
	private int[][] buffer;

	public FieldFiller(Field field) {
		this.field = field;
	}

	// todo: use only TRAVEL tail bounds, not all!
	public final int fill(Bounds bounds, Player owner) {
		copy(bounds, owner);
		floodFill(0, 0);
		return copyResult(bounds, owner);
	}

	private final void copy(Bounds bounds, Player owner) {
		sizeX = bounds.x2 - bounds.x1 + 3;
		sizeY = bounds.y2 - bounds.y1 + 3;
		buffer = new int[sizeX][sizeY];
		Cell point;

		for (int y = 1; y < sizeY - 1; y++) {
			for (int x = 1; x < sizeX - 1; x++) {
				point = field.getPoint(bounds.x1 + x - 1, bounds.y1 + y - 1);
				buffer[x][y] = point.owner == owner ? 1 : 0;
			}
		}
	}

	private final void debugOutput() {
		for (int y = 0; y < sizeY; y++) {
			String s = "";
			for (int x = 0; x < sizeX; x++) {
				s += buffer[x][y];
			}
			logger.log("buffer: ", s);
		}
	}

	private final void floodFill(int x, int y) {
		if (buffer[x][y] != 0) return;
		buffer[x][y] = 2;
		if (x > 0) { floodFill(x - 1, y); }
		if (x < sizeX - 1) { floodFill(x + 1, y); }
		if (y > 0) { floodFill(x, y - 1); }
		if (y < sizeY - 1) { floodFill(x, y + 1); }
		return;
	}

	private final int copyResult(Bounds bounds, Player owner) {
		int filled = 0;

		for (int y = 1; y < sizeY - 1; y++) {
			for (int x = 1; x < sizeX - 1; x++) {
				if (buffer[x][y] == 0) {
					field.getPoint(bounds.x1 + x - 1, bounds.y1 + y - 1).capture(owner);
					filled += 1;
				}
			}
		}

		return filled;
	}

}


// todo: floodFill stack implementation?

// public static void floodFillImage(BufferedImage image,int x, int y, Color color)
// {
//     int srcColor = image.getRGB(x, y);
//     boolean[][] hits = new boolean[image.getHeight()][image.getWidth()];

//     Queue<Point> queue = new LinkedList<Point>();
//     queue.add(new Point(x, y));

//     while (!queue.isEmpty())
//     {
//         Point p = queue.remove();

//         if(floodFillImageDo(image,hits,p.x,p.y, srcColor, color.getRGB()))
//         {
//             queue.add(new Point(p.x,p.y - 1));
//             queue.add(new Point(p.x,p.y + 1));
//             queue.add(new Point(p.x - 1,p.y));
//             queue.add(new Point(p.x + 1,p.y));
//         }
//     }
// }

// private static boolean floodFillImageDo(BufferedImage image, boolean[][] hits,int x, int y, int srcColor, int tgtColor)
// {
//     if (y < 0) return false;
//     if (x < 0) return false;
//     if (y > image.getHeight()-1) return false;
//     if (x > image.getWidth()-1) return false;

//     if (hits[y][x]) return false;

//     if (image.getRGB(x, y)!=srcColor)
//         return false;

//     // valid, paint it

//     image.setRGB(x, y, tgtColor);
//     hits[y][x] = true;
//     return true;
// }