
package engine.room;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import engine.utils.Logger;
import engine.math.MathUtils;
import engine.server.PlayerConnection;
import engine.player.Player;


public class Players {

	private static final Logger logger = new Logger("Players");

	private static final Comparator<Player> SCORE_COMPARATOR = new Comparator<Player>() {
		public int compare(Player p1, Player p2) {
			return p2.score.compareTopValue() - p1.score.compareTopValue();
		}
	};

	protected final Room room;

	public final ArrayList<Player> list;
	protected final ArrayList<Player> connected;
	protected final ArrayList<Player> removed;

	public int index = 1;

	public Players(Room room) {
		this.room = room;
		list = new ArrayList<Player>();
		removed = new ArrayList<Player>();
		connected = new ArrayList<Player>();
	}

	public Player connectNew(PlayerConnection plconn) {
		Player player = new Player(index++, room);
		connected.add(player);
		plconn.onOpen(player);
		logger.log("connected ", player);
		return player;
	}

	public Player create() {
		Player player = new Player(index++, room);
		connected.add(player);
		logger.log("created ", player);
		return player;
	}

	public void includeNewPlayers() {
		synchronized (connected) {
			list.addAll(connected);
			connected.clear();
		}
	}

	public void updateView() {
		for (Player player : list) {
			if (player.isWatching) {
				player.view.update();
			}
		}
	}

	public void tick(long ticks, double time, double tickDelta) {
		for (Player player : list) {
			if (!player.tick(ticks, time, tickDelta)) {
				removed.add(player);
				if (player.isConnected) {
					player.connection.close();
				}
				logger.log("removed ", player);
			}
		}
		if (removed.size() > 0) {
			list.removeAll(removed);
			removed.clear();
		}
	}

	public void sendState() {
		for (Player player : list) {
			if (player.isConnected) {
				player.connection.sendState();
			}
		}
	}

	public String pickNick() {
		return "Игрок " + index;
	}

	public int pickColor() {
		return MathUtils.randInt(1, 100);
	}

	public void sortByScores() {
		int totalPlayers = list.size();

		if (totalPlayers > 0) {
			Collections.sort(list, SCORE_COMPARATOR);

			for (int i = 0; i < totalPlayers; i++) {
				Player player = list.get(i);
				player.score.setRating(i + 1, totalPlayers);
			}
		}
	}

	public final Player get(int index) {
		return list.get(index);
	}

	public final int size() {
		return list.size();
	}

}