
package engine.room;


import engine.utils.Logger;
import engine.player.Player;


public class Bots {

	private static final Logger logger = new Logger("Bots");

	private final Room room;
	private final int maxCount;

	public Bots(Room room, int maxCount) {
		this.room = room;
		this.maxCount = maxCount;
		logger.log("start balancing, maxBots: ", maxCount);
	}

	public void createBot() {
		Player bot = room.players.create();
		bot.setupBotIntellect();
		logger.log("created bot ", bot);
	}

	public final void balance() {
		if (getBotsCount() < maxCount) {
			createBot();
		}
	}

	public int getBotsCount() {
		int count = 0;
		for (Player player : room.players.list) {
			if (player.intellect != null) {
				count += 1;
			}
		}
		return count;
	}

}