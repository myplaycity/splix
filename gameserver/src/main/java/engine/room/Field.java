
package engine.room;


import java.util.ArrayList;
import engine.utils.Logger;
import engine.math.Point;
import engine.math.Bounds;
import engine.math.MathUtils;
import engine.player.Player;
import engine.room.Room;


public class Field {

	private static final Logger logger = new Logger("Field", Logger.NORMAL);

	public final Room room;

	public final int sizeX;
	public final int sizeY;
	public final int space;
	public final int quadSize;
	public final int quadsX;
	public final int quadsY;

	protected Cell[] points;
	protected Quad[] quads;
	protected final FieldFiller fillBuffer;

	protected final ArrayList<Quad> checkedQuads;

	public Field(Room room, int sizeX, int sizeY, int quadSize) throws Exception {
		this.room = room;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.space = sizeX * sizeY;
		this.quadSize = quadSize;
		quadsX = sizeX / quadSize;
		quadsY = sizeY / quadSize;

		if (sizeX % quadSize != 0 || sizeY % quadSize != 0) {
			throw new Exception("Wrong field size settings, quadSize dont match field size");
		}

		createQuads();
		createPoints();
		fillBuffer = new FieldFiller(this);
		checkedQuads = new ArrayList<Quad>();
	}

	public String toString() {
		return "Field(" + sizeX + ", " + sizeY + ")";
	}

	protected void createPoints() {
		points = new Cell[sizeX * sizeY];
		logger.log("generating points: ", sizeX * sizeY);
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				points[y * sizeX + x] = new Cell(x, y, getQuadByPoint(x, y));
			}
		}
	}

	public Cell getPoint(int x, int y) {
		if (x >= 0 && x < sizeX && y >= 0 && y < sizeY) {
			return points[y * sizeX + x];
		}
		return null;
	}

	public Cell getPoint(int index) {
		return points[index];
	}

	protected void createQuads() {
		quads = new Quad[quadsX * quadsY];
		logger.log("generating quads: ", quadsX * quadsY);
		for (int y = 0; y < quadsY; y++) {
			for (int x = 0; x < quadsX; x++) {
				int id = y * quadsX + x;
				quads[id] = new Quad(this, id, x, y, quadSize);
			}
		}
	}

	public Quad getQuadByPoint(Point point) {
		int qx = point.x / quadSize;
		int qy = point.y / quadSize;
		return quads[qy * quadsX + qx];
	}

	public Quad getQuadByPoint(int x, int y) {
		int qx = x / quadSize;
		int qy = y / quadSize;
		return quads[qy * quadsX + qx];
	}

	public Quad getQuad(int x, int y) {
		return quads[y * quadsX + x];
	}

	public void getQuadsToList(Bounds bounds, ArrayList<Quad> quadList) {
		int x1 = bounds.x1 / quadSize;
		int y1 = bounds.y1 / quadSize;
		int x2 = bounds.x2 / quadSize;
		int y2 = bounds.y2 / quadSize;

		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				quadList.add(quads[y * quadsX + x]);
			}
		}
	}

	public final void captureBounds(Player newOwner, Bounds bounds) {
		int x1 = MathUtils.clamp(bounds.x1, 0, sizeX);
		int x2 = MathUtils.clamp(bounds.x2, 0, sizeX);
		int y1 = MathUtils.clamp(bounds.y1, 0, sizeX);
		int y2 = MathUtils.clamp(bounds.y2, 0, sizeX);

		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				points[y * sizeX + x].capture(newOwner);
			}
		}
	}

	public final void clearTerritory(Player diedPlayer, Bounds bounds) {
		for (int y = bounds.y1; y <= bounds.y2; y++) {
			for (int x = bounds.x1; x <= bounds.x2; x++) {
				Cell point = points[y * sizeX + x];
				point.clearTraveler(diedPlayer);
				point.clearFill(diedPlayer);
			}
		}
	}

	public final int travelCapture(Player newOwner, Bounds bounds) {
		int captured = 0;
		captured += captureTail(newOwner, bounds);
		captured += fillBuffer.fill(bounds, newOwner);
		return captured;
	}

	private final int captureTail(Player newOwner, Bounds bounds) {
		Cell point;
		int captured = 0;
		for (int y = bounds.y1; y <= bounds.y2; y++) {
			for (int x = bounds.x1; x <= bounds.x2; x++) {
				point = points[y * sizeX + x];
				if (point.traveler == newOwner && point.owner != newOwner) {
					point.captureTailPath(newOwner);
					captured += 1;
				}
			}
		}
		return captured;
	}

	public final void updated() {
		for (int y = 0; y < quadsY; y++) {
			for (int x = 0; x < quadsX; x++) {
				quads[y * quadsX + x].updated();
			}
		}
	}

	public Point pickFreePosition() {
		int padding = quadsX / 4;
		Quad randomQuad = getQuad(
			MathUtils.randInt(padding, quadsX - padding),
			MathUtils.randInt(padding, quadsY - padding)
		);

		if (randomQuad.isFree) {
			return new Point(randomQuad.centerPoint);
		} else {
			checkedQuads.clear();
			Quad nearFreeQuad = findNearFreeQuad(checkedQuads, randomQuad.x, randomQuad.y);
			if (nearFreeQuad != null) {
				return new Point(nearFreeQuad.centerPoint);
			}
		}

		// if nothing found
		logger.error("fail find good place!");
		return new Point(randomQuad.centerPoint);
	}

	public Point pickFreePosition(int padding) {
		Quad randomQuad = getQuad(
			MathUtils.randInt(padding, quadsX - padding),
			MathUtils.randInt(padding, quadsY - padding)
		);

		if (randomQuad.isFree) {
			return new Point(randomQuad.centerPoint);
		} else {
			checkedQuads.clear();
			Quad nearFreeQuad = findNearFreeQuad(checkedQuads, randomQuad.x, randomQuad.y);
			if (nearFreeQuad != null) {
				return new Point(nearFreeQuad.centerPoint);
			}
		}

		// if nothing found
		logger.error("fail find good place!");
		return new Point(randomQuad.centerPoint);
	}

	public Quad findNearFreeQuad(ArrayList<Quad> checkedQuads, int x, int y) {
		Quad quad = getQuad(x, y);
		if (checkedQuads.contains(quad)) {
			return null;
		}
		if (quad.isFree) {
			return quad;
		}

		checkedQuads.add(quad);
		Quad nearQuad = null;
		int padding = 1;

		// todo: this works wrong!

		if (x > padding) {
			nearQuad = findNearFreeQuad(checkedQuads, x - 1, y);
			if (nearQuad != null) return nearQuad;
		}
		if (x < quadsX - 1 - padding) {
			nearQuad = findNearFreeQuad(checkedQuads, x + 1, y);
			if (nearQuad != null) return nearQuad;
		}
		if (y > padding) {
			nearQuad = findNearFreeQuad(checkedQuads, x, y - 1);
			if (nearQuad != null) return nearQuad;
		}
		if (y < quadsY - 1 - padding) {
			nearQuad = findNearFreeQuad(checkedQuads, x, y + 1);
			if (nearQuad != null) return nearQuad;
		}

		return null;
	}

}
