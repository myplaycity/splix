
package engine.room;


import engine.utils.Logger;


public class GameLoop {

	private static final Logger logger = new Logger("GameLoop");

	private int fps = 0;
	private long time = 0;
	private long ticks = 0;
	private long sampleTickDelta = 0;
	private long tickDelta = 0;
	private boolean enabled = false;

	private final GameLoopCallback callback;

	private static final int NANOSECS_IN_SECOND = 1000000000;

	public GameLoop(int fps, GameLoopCallback callback) {
		this.fps = fps;
		this.sampleTickDelta = NANOSECS_IN_SECOND / fps;
		this.callback = callback;
	}

	public final void start() {
		logger.log("start game loop with " + fps + " fps");

		enabled = true;
		time = System.nanoTime();
		double gameStart = (double) time * 1e-9;

		while (enabled) {
			long now = System.nanoTime();
			tickDelta = now - time;
			time = now;
			ticks++;
			double gameTime = (double) now * 1e-9 - gameStart;

			callback.run(ticks, gameTime, (double) tickDelta * 1e-9);

			try {
				long sleepTime = (time + sampleTickDelta - System.nanoTime());
				if (sleepTime > 0) {
					long sleepTimeMillis = sleepTime / 1000000;
					Thread.sleep(sleepTimeMillis);
				} else {
					throw new Exception("sleep time is " + sleepTime);
				}
			} catch (Exception exc) {
				logger.error("fail sleep thread in gameloop: ", exc);
			}
		}
	}

	public final void stop() {
		enabled = false;
	}

}
