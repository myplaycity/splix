
package engine.room;


interface GameLoopCallback {

    public void run(long ticks, double time, double tickDelta);

}