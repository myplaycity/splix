
package engine.room;


import java.util.ArrayList;
import engine.math.Point;
import engine.math.Bounds;
import engine.player.Player;


public class Quad {

	private final Field field;
	public final int id;
	public final int x;
	public final int y;
	public final Bounds bounds;
	public final ArrayList<Player> viewers;
	public final ArrayList<Player> players;
	public final Point centerPoint;
	public boolean fillChanged;
	public boolean playersChanged;
	public boolean isFree;
	public boolean isFilled;

	public Quad(Field field, int id, int x, int y, int quadSize) {
		this.field = field;
		this.id = id;
		this.x = x;
		this.y = y;
		fillChanged = false;
		playersChanged = false;
		isFree = true;
		isFilled = false;
		bounds = new Bounds(x * quadSize, y * quadSize, (x + 1) * quadSize - 1, (y + 1) * quadSize - 1);
		viewers = new ArrayList<Player>();
		players = new ArrayList<Player>();
		centerPoint = new Point((bounds.x2 + bounds.x1) / 2, (bounds.y2 + bounds.y1) / 2);
	}

	public String toString() {
		return "Quad(" + x + ", " + y + ", id: " + id + ", viewers: " + viewers.size() + ", players: " + players.size() + ", bounds: " + bounds + ")";
	}

	public final void updated() {
		if (fillChanged || playersChanged) {
			checkIsFree();
		}
		fillChanged = false;
		playersChanged = false;
	}

	private final void checkIsFree() {
		isFree = true;
		isFilled = false;
		if (players.size() > 0) {
			isFree = false;
		}
		for (int y = bounds.y1; y <= bounds.y2; y++) {
			for (int x = bounds.x1; x <= bounds.x2; x++) {
		 		Cell point = field.getPoint(x, y);
		 		if (point.color > 0) {
		 			isFree = false;
		 			isFilled = true;
		 			return;
		 		}
			}
		}
	}

	public final void addViewer(Player viewer) {
		viewers.add(viewer);
	}

	public final void removeViewer(Player viewer) {
		viewers.remove(viewer);
	}

	public final void addPlayer(Player player) {
		players.add(player);
		playersChanged = true;
	}

	public final void removePlayer(Player player) {
		players.remove(player);
		playersChanged = true;
	}

}