
package engine.utils;


import java.lang.StringBuilder;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Logger {

	private static final String ANSI_RESET = "\033[0m";
	private static final String ANSI_GRAY = "\033[1;30m";
	private static final String ANSI_RED = "\033[1;31m";
	private static final String ANSI_GREEN = "\033[1;32m";
	private static final String ANSI_YELLOW = "\033[1;33m";
	private static final String ANSI_BLUE = "\033[1;34m";

	public static final int HIDDEN = 0;
	public static final int NORMAL = 1;
	public static final int DEBUG = 2;

	public String name;
	public int level;
	public StringBuilder buffer;

	public Logger(String loggerName) {
		this.name = " [" + loggerName + "] ";
		this.level = Logger.HIDDEN;
		this.buffer = new StringBuilder();
	}

	public Logger(String loggerName, int level) {
		this.name = " [" + loggerName + "] ";
		this.level = level;
		this.buffer = new StringBuilder();
	}

	public void print(String message) {
		System.out.println(message);
	}

	public void print(StringBuilder message) {
		System.out.println(message);
	}

	private String date() {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private void makePrefix() {
		buffer.append(ANSI_GRAY);
		buffer.append(date());
		buffer.append(ANSI_BLUE);
		buffer.append(name);
	}

	public void log(String message, Object... args) {
		if (level > Logger.HIDDEN) {
			// todo: make lazy file open!

			String messColor = ANSI_RESET;
			if (level == Logger.DEBUG) {
				messColor = ANSI_YELLOW;
			}

			makePrefix();
			buffer.append(messColor);
			buffer.append(message);
			buffer.append(" ");

			if (args.length > 0) {
				for (int i = 0; i < args.length; i++) {
					buffer.append(args[i].toString());
					buffer.append(" ");
				}
			}

			print(buffer);
			buffer.setLength(0);
		}
	}

	public void error(String message) {
		print(ANSI_GRAY + date() + ANSI_BLUE + name + ANSI_RED + message + ANSI_RESET);
	}

	public void error(String message, Throwable exc) {
		StringWriter sw = new StringWriter();
		exc.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		print(ANSI_GRAY + date() + ANSI_BLUE + name + ANSI_RED + message + " " + exceptionAsString + ANSI_RESET);
	}

}