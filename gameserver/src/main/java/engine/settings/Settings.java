
package engine.settings;


import engine.utils.Logger;
import java.io.IOException;


public class Settings {

	private static final Logger logger = new Logger("Settings", Logger.NORMAL);

	// default settings
	public String name = "";
	public String type = "";
	public int wsPort = 8200;
	public String wsPath = "";
	public String statusPath = "";
	public int fps = 10;
	public int sizeX = 200;
	public int sizeY = 200;
	public int quadSize = 8;
	public int viewQuadPadding = 2;
	public int bots = 10;
	public int waitAuthTicks = 50;

	// for original game
	public int scorePerCapturedPercent = 1;
	public int scorePerKill = 1;

	// for towers game
	public int scorePerTower = 1;
	public int maxTowers = 0;

	// for mines game
	public int scorePerMine = 1;
	public int maxMines = 0;
	public int mineCharges = 1;
	public int mineTime = 1;

	public Settings(String settingsPath) {
		if (settingsPath != "") {
			readSettingsFile(settingsPath);
		}
	}

	public void readSettingsFile(String settingsPath) {
		IniReader ini = new IniReader(settingsPath);

		try {
			ini.load();

			type = ini.readString("type");
			wsPath = ini.readString("wsPath");
			statusPath = ini.readString("statusPath");
			fps = ini.readInt("fps", fps);
			sizeX = ini.readInt("sizeX", sizeX);
			sizeY = ini.readInt("sizeY", sizeY);
			quadSize = ini.readInt("quadSize", quadSize);
			viewQuadPadding = ini.readInt("viewQuadPadding", viewQuadPadding);
			bots = ini.readInt("bots", bots);
			waitAuthTicks = ini.readInt("waitAuthTicks", waitAuthTicks);

			// for original
			scorePerCapturedPercent = ini.readInt("scorePerCapturedPercent", scorePerCapturedPercent);
			scorePerKill = ini.readInt("scorePerKill", scorePerKill);

			// for towers
			scorePerTower = ini.readInt("scorePerTower", scorePerTower);
			maxTowers = ini.readInt("maxTowers", maxTowers);

			// for mines
			scorePerMine = ini.readInt("scorePerMine", scorePerMine);
			maxMines = ini.readInt("maxMines", maxMines);
			mineCharges = ini.readInt("mineCharges", mineCharges);
			mineTime = ini.readInt("mineTime", mineTime);

			ini.close();
			logger.log("read settings from: ", settingsPath);

		} catch (IOException exc) {
			logger.error("fail read settings file", exc);
		}
	}

	public void setMainArgs(String roomName, int wsPort) {
		this.name = roomName;
		this.wsPort = wsPort;
		wsPath += wsPort;
		statusPath += name;
	}

}
