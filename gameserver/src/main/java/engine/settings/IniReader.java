
package engine.settings;


import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public final class IniReader {

	private String filePath;
	private FileInputStream fileStream = null;
	private Properties props;

	public IniReader(String filePath) {
		this.filePath = filePath;
	}

	public final void load() throws IOException {
		props = new Properties();
		fileStream = new FileInputStream(new File(filePath));
		props.load(fileStream);
	}

	public final void close() throws IOException {
		fileStream.close();
	}

	public final int readInt(String name) {
		String readed = props.getProperty(name);
		if (readed != null) {
			return Integer.valueOf(readed);
		} else {
			return 0;
		}
	}

	public final int readInt(String name, int defaultValue) {
		String readed = props.getProperty(name);
		if (readed != null) {
			return Integer.valueOf(readed);
		} else {
			return defaultValue;
		}
	}

	public final String readString(String name) {
		String readed = props.getProperty(name);
		if (readed != null) {
			return readed;
		} else {
			return "";
		}
	}

	public final String readString(String name, String defaultValue) {
		String readed = props.getProperty(name);
		if (readed != null) {
			return readed;
		} else {
			return defaultValue;
		}
	}

}