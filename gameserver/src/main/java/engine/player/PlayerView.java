
package engine.player;


import java.util.HashSet;
import engine.utils.Logger;
import engine.math.Bounds;
import engine.server.ServerMessage;
import engine.room.Field;
import engine.room.Cell;
import engine.room.Quad;


public class PlayerView {

	private static final Logger logger = new Logger("PlayerView", Logger.DEBUG);

	protected final Player player;
	protected final HashSet<Player> viewedPlayers;
	protected final HashSet<Player> newViewedPlayers;
	protected final HashSet<Player> tailBoundsViewers;

	protected Quad centerQuad;
	protected boolean quadChanged;

	protected final Bounds oldViewQuadArea;
	protected final Bounds viewQuadArea;
	protected final Bounds tailBounds;
	protected final Bounds quadViewTailBounds;
	protected final int viewQuadPadding;

	public PlayerView(Player player) {
		this.player = player;
		viewedPlayers = new HashSet<Player>();
		newViewedPlayers = new HashSet<Player>();
		tailBoundsViewers = new HashSet<Player>();

		viewQuadPadding = player.room.settings.viewQuadPadding;
		oldViewQuadArea = new Bounds(-1, -1, -1, -1);
		viewQuadArea = new Bounds(-1, -1, -1, -1);
		tailBounds = new Bounds(-1, -1, -1, -1);
		quadViewTailBounds = new Bounds(-1, -1, -1, -1);

		quadChanged = false;
	}

	public final void setupViewer() {
		Field field = player.room.field;
		Quad newQuad = field.getQuadByPoint(player.position);

		if (centerQuad != newQuad) {
			if (centerQuad != null) {
				centerQuad.removeViewer(player);
			}
			newQuad.addViewer(player);
			centerQuad = newQuad;
			quadChanged = true;

			oldViewQuadArea.set(viewQuadArea);
			viewQuadArea.set(centerQuad.x, centerQuad.y, viewQuadPadding);
			viewQuadArea.clamp(0, 0, field.quadsX - 1, field.quadsY - 1);
		}

		tailBounds.set(player.tail.bounds);
	}

	public final void update() {
		revealField();

		if (player.isConnected) {
			if (quadChanged) {
				player.connection.state.changeQuad(centerQuad.id);
				quadChanged = false;
			}
			revealPlayers();
		}

		viewedPlayers.clear();
		viewedPlayers.addAll(newViewedPlayers);
	}

	public final void destroy() {
		if (centerQuad != null) {
			centerQuad.removeViewer(player);
		}
	}

	protected void revealField() {
		newViewedPlayers.clear();

		for (int y = viewQuadArea.y1; y <= viewQuadArea.y2; y++) {
			for (int x = viewQuadArea.x1; x <= viewQuadArea.x2; x++) {
				Quad quad = player.room.field.getQuad(x, y);
				if (player.isConnected) {
					boolean thisQuadWasUnseen = (quadChanged && !oldViewQuadArea.has(x, y));
					if (quad.fillChanged || thisQuadWasUnseen) {
						revealQuadFill(quad);
					}
				}
				newViewedPlayers.addAll(quad.players);
			}
		}
	}

	protected final void revealQuadFill(Quad quad) {
		Field field = player.room.field;
		ServerMessage message = player.connection.state;
		Bounds bounds = quad.bounds;
		Cell point;

		message.revealQuad(quad.id);
		int quadSize = player.room.field.quadSize;
		int scanerColor = 0;
		int scanerPos = 0;
		int colorStart = 0;
		boolean isLastPoint;

		for (int y = bounds.y1; y <= bounds.y2; y++) {
			for (int x = bounds.x1; x <= bounds.x2; x++) {
		 		point = field.getPoint(x, y);
		 		scanerPos = (y % quadSize) * quadSize + x % quadSize;
				if (point.color != scanerColor) {
					if (scanerColor > 0) {
						message.quadFill(scanerColor, colorStart, scanerPos - 1);
					}
					if (point.color > 0) {
						colorStart = scanerPos;
					}
					scanerColor = point.color;
				}
				isLastPoint = x == bounds.x2 && y == bounds.y2;
				if (isLastPoint && scanerColor > 0) {
					message.quadFill(scanerColor, colorStart, scanerPos);
				}
			}
		}
		message.revealQuadStop();
	}

	private final void revealPlayers() {
		for (Player player : newViewedPlayers) {
			if (!viewedPlayers.contains(player) && player.isPlaying && player != this.player) {
				revealPlayer(player);
			}
		}
		for (Player player : viewedPlayers) {
			if (!newViewedPlayers.contains(player) && player != this.player) {
				hidePlayer(player);
			}
		}
	}

	private final void revealPlayer(Player player) {
		ServerMessage message = this.player.connection.state;
		message.newPlayer(player);
		if (player.tail.started) {
			message.revealTail(player, player.tail);
		}
	}

	private final void hidePlayer(Player player) {
		ServerMessage message = this.player.connection.state;
		message.hidePlayer(player.id);
	}

	public final HashSet<Player> getViewers() {
		return viewedPlayers;
	}

	public final HashSet<Player> getTailViewers() {
		Field field = player.room.field;
		quadViewTailBounds.set(tailBounds).div(field.quadSize).indent(viewQuadPadding).clamp(0, 0, field.quadsX - 1, field.quadsY - 1);
		tailBoundsViewers.clear();

		for (int y = quadViewTailBounds.y1; y <= quadViewTailBounds.y2; y++) {
			for (int x = quadViewTailBounds.x1; x <= quadViewTailBounds.x2; x++) {
				tailBoundsViewers.addAll(field.getQuad(x, y).viewers);
			}
		}

		return tailBoundsViewers;
	}

}