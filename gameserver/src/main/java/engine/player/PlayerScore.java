
package engine.player;


import engine.utils.Logger;


public class PlayerScore {

	private static final Logger logger = new Logger("PlayerScore", Logger.DEBUG);

	protected final Player player;

	public int blocks;
	public int kills;
	public int tail;
	public int timeAlive;
	public int timeTop1;
	public int total;

	public int rating;
	public int totalPlayers;

	protected int ticks;
	protected int fps;

	protected int top10Sending;

	public static final int PER_KILL = 500;

	public PlayerScore(Player player) {
		this.player = player;

		blocks = 0;
		kills = 0;
		total = 0;
		tail = 0;
		timeAlive = 0;
		timeTop1 = 0;

		rating = 0;
		totalPlayers = 0;

		ticks = 0;
		fps = player.room.fps;

		top10Sending = -1;
	}

	public void addCapture(int captured) {
		blocks += captured;
		if (player.isConnected) {
			player.connection.state.earnScoreCapture(captured);
		}
		recalcTotal();
	}

	public final void addKill() {
		kills += 1;
		if (player.isConnected) {
			player.connection.state.earnScoreKill(PlayerScore.PER_KILL);
		}
		recalcTotal();
	}

	public void recalcTotal() {
		total = blocks + kills * PlayerScore.PER_KILL;
	}

	public final void setRating(int position, int total) {
		rating = position;
		totalPlayers = total;
	}

	public void tick() {
		ticks += 1;
		if (player.isConnected) {
			if (ticks % fps == 0) {
				sendRating();
			}
			if (ticks % (fps * 2) == 0) { // every 5 seconds start send top10 table
				startSendTop10();
			}
			if (top10Sending >= 0) {
				sendTop10();
			}
		}
	}

	protected final void sendRating() {
		if (rating > 0 && totalPlayers > 0) {
			player.connection.state.rating(rating, totalPlayers);
		}
	}

	public void startSendTop10() {
		top10Sending = 0;
	}

	public void sendTop10() {
		if (top10Sending > 9 || top10Sending > player.room.players.size() - 1) {
			stopSendTop10();
			return;
		}
		Player top10player = player.room.players.get(top10Sending);
		player.connection.state.top10Line(top10player);
		top10Sending += 1;
	}

	public void stopSendTop10() {
		player.connection.state.top10Stop();
		top10Sending = -1;
	}

	public int compareTopValue() {
		return total;
	}

}