
package engine.player;


import engine.utils.Logger;
import engine.math.Point;
import engine.math.Bounds;
import engine.math.MathUtils;
import engine.room.Room;
import engine.room.Cell;
import engine.room.Field;


/**
 * Bot intellect is a module of player.
 * If player has intellect, it will use it.
 */
public class BotIntellect {

	private static final Logger logger = new Logger("BotIntellect", Logger.DEBUG);

	private Player player;

	private int action;
	private Point actionTarget;
	private Point safeTarget;

	private static final int ACTION_NONE = 0;
	private static final int ACTION_MOVE = 1;
	private static final int ACTION_EXPLORE = 2;
	private static final int ACTION_RETURN = 3;

	public BotIntellect(Player player) {
		this.player = player;
		action = BotIntellect.ACTION_NONE;
	}

	public String toString() {
		return "BotIntellect(" + player.id + ")";
	}

	public void think() {
		if (action == BotIntellect.ACTION_NONE) {
			prepareStart();
		}
		if (action == BotIntellect.ACTION_MOVE && targetIsNear()) {
			prepareExplore();
		}
		if (action == BotIntellect.ACTION_EXPLORE && targetIsNear()) {
			prepareReturn();
		}
		if (action == BotIntellect.ACTION_RETURN && targetIsNear()) {
			prepareMove();
		}
		preventBorderCollision();
		moveToTarget();
	}

	private boolean targetIsNear() {
		return player.position.diffDist(actionTarget) == 0;
	}

	private void prepareStart() {
		action = BotIntellect.ACTION_MOVE;
		actionTarget = new Point(player.position);
		actionTarget.addRandom(-1, 1, -1, 1);
		safeTarget = new Point(player.position);
	}

	private void prepareMove() {
		action = BotIntellect.ACTION_MOVE;
		getSafeTarget();
	}

	private void prepareExplore() {
		action = BotIntellect.ACTION_EXPLORE;
		actionTarget.set(player.position).addRandom(-15, 15, -15, 15);
	}

	private void prepareReturn() {
		action = BotIntellect.ACTION_RETURN;
		getSafeTarget();
	}

	private void getSafeTarget() {
		Bounds terr = player.territoryBounds;
		Field field = player.room.field;
		int randX = MathUtils.randInt(terr.x1, terr.x2);
		int randY = MathUtils.randInt(terr.y1, terr.y2);
		Cell randCell = field.getPoint(randX, randY);

		if (randCell.owner == player) {
			actionTarget.set(randCell.x, randCell.y);
			return;
		}

		int range = 1;
		int maxRange = Math.max(terr.x2 - terr.x1 + 1, terr.y2 - terr.y1 + 1);
		Cell founded = null;
		while (range < maxRange && founded == null) {
			Cell c;

			c = field.getPoint(randX + range, randY);
			if (c != null && c.owner == player) { founded = c; break; }

			c = field.getPoint(randX - range, randY);
			if (c != null && c.owner == player) { founded = c; break; }

			c = field.getPoint(randX, randY + range);
			if (c != null && c.owner == player) { founded = c; break; }

			c = field.getPoint(randX, randY - range);
			if (c != null && c.owner == player) { founded = c; break; }

			range += 1;
		}
		if (founded != null) {
			actionTarget.set(founded.x, founded.y);
		} else {
			actionTarget.set(safeTarget);
		}
	}

	private void moveToTarget() {
		int dx = MathUtils.clamp(actionTarget.x - player.nowCell.x, -1, 1);
		int dy = MathUtils.clamp(actionTarget.y - player.nowCell.y, -1, 1);
		Point dir = player.wantDirection;

		if (dx != 0) {
			if (dx == -dir.x) {
				dir.set(0, dx);
			} else {
				dir.set(dx, 0);
			}
		} else if (dy != 0) {
			if (dy == -dir.y) {
				dir.set(dy, 0);
			} else {
				dir.set(0, dy);
			}
		}
	}

	private void preventBorderCollision() {
		Field field = player.room.field;
		Point pos = player.position;
		Point dir = player.direction;
		if (pos.x <= 2 && dir.x < 0) {
			prepareReturn();
		}
		if (pos.y <= 2 && dir.y < 0) {
			prepareReturn();
		}
		if (pos.x >= field.sizeX - 3 && dir.x > 0) {
			prepareReturn();
		}
		if (pos.y >= field.sizeY - 3 && dir.y > 0) {
			prepareReturn();
		}
	}

}
