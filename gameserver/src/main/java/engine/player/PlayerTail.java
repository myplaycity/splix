
package engine.player;


import java.util.ArrayList;
import engine.utils.Logger;
import engine.math.Point;
import engine.math.Bounds;
import engine.room.Cell;


public final class PlayerTail {

	private static final Logger logger = new Logger("PlayerTail", Logger.DEBUG);

	private final Player player;

	public final ArrayList<Point> points;
	public final Bounds bounds;
	public boolean started;
	public int size;

	public PlayerTail(Player player) {
		this.player = player;
		points = new ArrayList<Point>();
		bounds = new Bounds(-1, -1, -1, -1);
		started = false;
		size = 0;
	}

	public final void move(Cell nowCell) {
		if (!started) {
			addPoint(new Point(nowCell.x, nowCell.y));
			player.broadcast.tailStart(player, this);
			bounds.set(nowCell.x, nowCell.y, 0);
		} else {
			bounds.extend(nowCell.x, nowCell.y);
		}
	}

	public final void turn() {
		Point lastPoint = points.get(points.size() - 1);
		if (!lastPoint.equals(player.position)) {
			addPoint(player.position.clone());
		}
	}

	private final void addPoint(Point point) {
		points.add(point);
		started = true;
		size += 1;
	}

	public final void clear() {
		if (size > 0) {
			player.broadcast.tailClear(player, this);
			points.clear();
			started = false;
			size = 0;
			bounds.set(-1, -1, -1, -1);
		}
	}

}