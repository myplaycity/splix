
package engine.player;


import java.util.ArrayList;
import java.lang.StringBuilder;
import engine.utils.Logger;
import engine.math.Point;
import engine.math.Bounds;
import engine.math.MathUtils;
import engine.server.PlayerConnection;
import engine.room.Room;
import engine.room.Field;
import engine.room.Quad;
import engine.room.Cell;


public class Player {

	private static final Logger logger = new Logger("Player");

	public static final int STATUS_DESTROYED = 0;
	public static final int STATUS_CONNECTING = 1;
	public static final int STATUS_PLAYING = 2;
	public static final int STATUS_DIED = 3;

	public static final int TICKS_AFTER_DESTROY = 5;
	public static final int HALF_MOVE = 1;
	public static final int ALL_MOVE = 2;

	public Room room;
	public PlayerConnection connection;
	public PlayerView view;
	public PlayerBroadcast broadcast;

	public int id;
	public boolean isAuthed;
	public boolean isConnected;
	public boolean isPlaying;
	public boolean isWatching;
	public boolean isDestroyed;
	public boolean isTurned;

	public int status;
	public int statusTicks;

	public Point position;
	public Point direction;
	public int cellMoveValue;
	public Point wantDirection;
	public Bounds territoryBounds;
	public PlayerTail tail;
	public PlayerScore score;
	public PlayerMinimap minimap;

	public BotIntellect intellect;

	public int color;
	public String nick = null;
	public String hash = null;

	protected Cell nowCell;
	protected Cell targetCell;

	private final ArrayList<Quad> quads;

	public Player(int id, Room room) {
		this.id = id;
		this.room = room;

		connection = null;
		isConnected = false;
		isAuthed = false;
		isTurned = false;

		updateStatus(Player.STATUS_CONNECTING);
		statusTicks = room.settings.waitAuthTicks;

		broadcast = new PlayerBroadcast(this);
		view = new PlayerView(this);
		tail = new PlayerTail(this);
		score = new PlayerScore(this);
		minimap = new PlayerMinimap(this);

		quads = new ArrayList<Quad>();
	}

	public String toString() {
		return "Player(" + id + ", color: " + color + ")";
	}

	public void setUserInfo(String nick, String userHash) {
		if ((nick != null && !nick.isEmpty())) {
			int nickLen = nick.length();
			if (nickLen > 12) {
				this.nick = nick.substring(0, 11);
			} else {
				this.nick = nick;
			}
		} else {
			this.nick = room.players.pickNick();
		}
		this.hash = userHash;
		isAuthed = true;
	}

	public void startPlay() {
		updateStatus(Player.STATUS_PLAYING);
		position = room.field.pickFreePosition();
		direction = new Point(0, -1);
		wantDirection = new Point(0, -1);
		territoryBounds = new Bounds(position, 2);
		color = room.players.pickColor();

		cellMoveValue = 0;
		nowCell = room.field.getPoint(position.x, position.y);
		targetCell = room.field.getPoint(position.x + direction.x, position.y + direction.y);

		if (isConnected) {
			connection.state.handshake(room);
			connection.state.setupPlayer(this);
		}

		view.setupViewer();
		broadcast.newPlayer(this);
		room.field.captureBounds(this, territoryBounds);
	}

	public void setupBotIntellect() {
		intellect = new BotIntellect(this);
		setUserInfo("", "");
		// startPlay();
	}

	public void wantMove(int x, int y) {
		if (isPlaying) {
			if (direction.x != -x && direction.y != -y) {
				wantDirection.set(x, y);
			}
		}
	}

	public void updateStatus(int newStatus) {
		status = newStatus;
		isPlaying = status == Player.STATUS_PLAYING;
		isWatching = status == Player.STATUS_PLAYING || status == Player.STATUS_DIED;
		isDestroyed = status == Player.STATUS_DESTROYED;
	}

	public boolean tick(long ticks, double time, double tickDelta) {
		if (status == Player.STATUS_CONNECTING) {
			tickConnecting();
		} else if (status == Player.STATUS_PLAYING) {
			if (intellect != null) {
				intellect.think();
			}
			if (move()) {
				attachQuads();
				view.setupViewer();
			}
			travel();
			checkCollisions();
			score.tick();
			minimap.tick();
		} else if (status == Player.STATUS_DIED) {
			tickDied();
		}
		return status != Player.STATUS_DESTROYED;
	}

	private boolean move() {
		cellMoveValue += 1;

		if (cellMoveValue <= Player.HALF_MOVE && !isTurned) {
			if (!direction.equals(wantDirection)) {
				direction.set(wantDirection);
				isTurned = true;
				targetCell = room.field.getPoint(position.x + direction.x, position.y + direction.y);
				broadcast.moveDirection(this, tail);

				if (nowCell.owner != this) {
					tail.turn();
				}
			}
		}

		if (cellMoveValue == Player.ALL_MOVE) {
			position.add(direction);
			cellMoveValue = 0;
			isTurned = false;
			nowCell = room.field.getPoint(position.x, position.y);
			targetCell = room.field.getPoint(position.x + direction.x, position.y + direction.y);
			if (nowCell.owner != this) {
				tail.move(nowCell);
			}
			return true;
		}
		return false;
	}

	private void attachQuads() {
		for (Quad quad : quads) {
			quad.removePlayer(this);
		}
		quads.clear();

		if (tail.started) {
			room.field.getQuadsToList(tail.bounds, quads);
		} else {
			Quad quad = room.field.getQuadByPoint(position);
			quads.add(quad);
		}

		for (Quad quad : quads) {
			quad.addPlayer(this);
		}
	}

	private void clearQuads() {
		for (Quad quad : quads) {
			quad.removePlayer(this);
		}
		quads.clear();
	}

	protected void travel() {
		if (cellMoveValue == Player.HALF_MOVE && targetCell != null) {
			if (targetCell.traveler != null) {
				if (targetCell.traveler != this) {
					targetCell.traveler.die();
					score.addKill();
				} else if (targetCell.traveler == this) {
					die();
				}
			}

			if (targetCell.owner == this && nowCell.owner != this) {
				int capturedSpace = room.field.travelCapture(this, territoryBounds);
				score.addCapture(capturedSpace);
				tail.clear();

			} else if (targetCell.owner != this) {
				targetCell.traveler = this;
				territoryBounds.extend(targetCell.x, targetCell.y);
			}
		}
	}

	private void checkCollisions() {
		boolean collided = false;
		Field field = room.field;

		if (cellMoveValue == Player.HALF_MOVE) {
			if (position.x == 0 && direction.x < 0) {
				collided = true;
			}
			if (position.x == field.sizeX - 1 && direction.x > 0) {
				collided = true;
			}
			if (position.y == 0 && direction.y < 0) {
				collided = true;
			}
			if (position.y == field.sizeY - 1 && direction.y > 0) {
				collided = true;
			}
		}

		if (collided) {
			die();
		}
	}

	public void die() {
		logger.log("died", this);
		tail.clear();
		room.field.clearTerritory(this, territoryBounds);
		broadcast.diePlayer(this);

		updateStatus(Player.STATUS_DIED);
		statusTicks = Player.TICKS_AFTER_DESTROY;

		if (isConnected) {
			saveRound();
		}
	}

	protected void tickDied() {
		statusTicks -= 1;
		if (statusTicks == 0) {
			gameover();
			destroy();
		}
	}

	protected void tickConnecting() {
		statusTicks -= 1;
		if (statusTicks == 0) {
			logger.log("fail wait for auth message", this);
			destroy();
		}
		if (isAuthed) {
			logger.log("player authed, start play: ", nick, hash);
			startPlay();
		}
	}

	public void gameover() {
		if (isConnected) {
			connection.state.gameOver();
			connection.sendState();
		}
	}

	public void destroy() {
		logger.log("destroyed", this);
		clearQuads();
		view.destroy();
		updateStatus(Player.STATUS_DESTROYED);
	}

	public boolean hasUserInfo(Player player) {
		// todo: undone!
		// use this to determine, if this player seen another player
		return false;
	}

	public void saveRound() {
		room.api.saveRound(hash, nick, score);
	}

}
