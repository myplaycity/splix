
package engine.player;


import java.util.HashSet;


public class PlayerBroadcast {

	private final Player player;

	public PlayerBroadcast(Player player) {
		this.player = player;
	}

	public final void newPlayer(Player player) {
		HashSet<Player> viewedPlayers = this.player.view.getViewers();
		for (Player viewer : viewedPlayers) {
			if (viewer.isConnected && viewer != this.player) {
				viewer.connection.state.newPlayer(player);
			}
		}
	}

	public final void moveDirection(Player player, PlayerTail tail) {
		HashSet<Player> viewedPlayers;
		if (tail.started) {
			viewedPlayers = this.player.view.getTailViewers();
		} else {
			viewedPlayers = this.player.view.getViewers();
		}
		for (Player viewer : viewedPlayers) {
			if (viewer.isConnected) {
				viewer.connection.state.moveDirection(player);
			}
		}
	}

	public final void diePlayer(Player player) {
		HashSet<Player> viewedPlayers = this.player.view.getViewers();
		for (Player viewer : viewedPlayers) {
			if (viewer.isConnected) {
				viewer.connection.state.diePlayer(player);
			}
		}
	}

	public final void tailStart(Player player, PlayerTail tail) {
		HashSet<Player> viewedPlayers = this.player.view.getViewers();
		for (Player viewer : viewedPlayers) {
			if (viewer.isConnected) {
				viewer.connection.state.tailStart(player, tail);
			}
		}
	}

	public final void tailClear(Player player, PlayerTail tail) {
		HashSet<Player> viewedPlayers = this.player.view.getTailViewers();
		for (Player viewer : viewedPlayers) {
			if (viewer.isConnected) {
				viewer.connection.state.tailClear(player);
			}
		}
	}

}