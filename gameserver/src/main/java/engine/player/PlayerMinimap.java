
package engine.player;


import engine.utils.Logger;
import engine.server.ServerMessage;
import engine.room.Field;
import engine.room.Quad;


public class PlayerMinimap {

	private static final Logger logger = new Logger("PlayerScore", Logger.DEBUG);

	private final Player player;
	private int ticks;
	private int fps;
	private int sending;
	private int mapLines;

	public PlayerMinimap(Player player) {
		this.player = player;
		fps = player.room.fps;
		ticks = 0;
		sending = -1;
		mapLines = player.room.field.quadsY;
	}

	public final void tick() {
		ticks += 1;
		if (player.isConnected) {
			if (ticks % (fps * 5) == 0) { // every 5 seconds start send minimap
				startSend();
			}
			if (sending >= 0) {
				sendPart();
			}
		}
	}

	private final void startSend() {
		sending = 0;
	}

	private final void sendPart() {
		scanAndSendLine(sending);
		sending += 1;

		if (sending == mapLines) {
			stopSend();
		}
	}

	private final void stopSend() {
		player.connection.state.minimapStop();
		sending = -1;
	}

	private final void scanAndSendLine(int lineIndex) {
		ServerMessage state = player.connection.state;
		Field field = player.room.field;
		state.minimapLine();

		int fillStart = 0;
		boolean scanerFilled = false;
		boolean isLastPoint;

		for (int x = 0; x < field.quadsX; x++) {
			Quad quad = field.getQuad(x, lineIndex);
			if (quad.isFilled != scanerFilled) {
				if (scanerFilled == true) {
					state.minimapFill(fillStart, x - 1);
				}
				if (quad.isFilled) {
					fillStart = x;
				}
				scanerFilled = quad.isFilled;
			}
			isLastPoint = x == (field.quadsX - 1);
			if (isLastPoint && scanerFilled) {
			 	state.minimapFill(fillStart, x);
			}
		}
	}

}