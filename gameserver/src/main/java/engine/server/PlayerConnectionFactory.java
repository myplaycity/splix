
package engine.server;


import java.net.Socket;
import java.util.List;

import org.java_websocket.WebSocketAdapter;
import org.java_websocket.drafts.Draft;
import org.java_websocket.server.DefaultWebSocketServerFactory;


public class PlayerConnectionFactory extends DefaultWebSocketServerFactory {

	@Override
	public PlayerConnection createWebSocket(WebSocketAdapter a, Draft d, Socket s) {
		return new PlayerConnection(a, d);
	}

	@Override
	public PlayerConnection createWebSocket(WebSocketAdapter a, List<Draft> d, Socket s) {
		return new PlayerConnection(a, d);
	}

}