
package engine.server;


import java.nio.ByteBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;
import java.nio.CharBuffer;
import java.io.UnsupportedEncodingException;

import engine.utils.Logger;
import engine.room.Room;
import engine.player.Player;
import engine.player.PlayerTail;
import engine.games.original.PlayerScoreOriginal;
import engine.math.Point;
import engine.games.towers.Tower;
import engine.games.mines.Mine;


public class ServerMessage {

	private static final Logger logger = new Logger("ServerMessage", Logger.DEBUG);

	public ByteBuffer buffer = null;
	public int length = 0;

	private final int BUFFER_SIZE = 128 * 8 * 8;

	// message types
	public static final byte PONG = 1;
	public static final byte HANDSHAKE = 2;
	public static final byte SETUP_PLAYER = 3;
	public static final byte MOVE_DIRECTION = 4;
	public static final byte NEW_PLAYER = 5;
	public static final byte DIE_PLAYER = 6;
	public static final byte REVEAL_QUAD = 7;
	public static final byte QUAD_FILL = 8;
	public static final byte REVEAL_TAIL = 9;
	public static final byte TAIL_START = 10;
	public static final byte TAIL_CLEAR = 11;
	public static final byte CHANGE_QUAD = 13;
	public static final byte HIDE_PLAYER = 14;
	public static final byte EARN_SCORE_CAPTURE = 15;
	public static final byte EARN_SCORE_KILL = 16;
	public static final byte RATING = 17;
	public static final byte REVEAL_QUAD_STOP = 18;
	public static final byte TOP10_LINE = 19;
	public static final byte TOP10_STOP = 20;
	public static final byte MINIMAP_LINE = 21;
	public static final byte MINIMAP_FILL = 22;
	public static final byte MINIMAP_STOP = 23;
	public static final byte GAME_OVER = 24;
	public static final byte NEW_TOWER = 25;
	public static final byte REMOVE_TOWER = 26;
	public static final byte EARN_SCORE_TOWER_CAPTURE = 27;
	public static final byte NEW_MINE = 28;
	public static final byte REMOVE_MINE = 29;
	public static final byte EARN_SCORE_MINE_CAPTURE = 30;
	public static final byte MINE_CHARGE = 31;

	public ServerMessage() {
		buffer = ByteBuffer.allocate(BUFFER_SIZE);
	}

	public void clear() {
		buffer.clear();
		length = 0;
	}

	// todo: make autocalculating length ?
	public ByteBuffer getData() {
		buffer.position(0);
		buffer.limit(length);
		return buffer;
	}

	// messages

	public void pong() {
		buffer.put(ServerMessage.PONG);
		length += 1;
	}

	public void handshake(Room room) {
		buffer.put(ServerMessage.HANDSHAKE);
		buffer.putShort((short) room.field.sizeX);
		buffer.putShort((short) room.field.sizeY);
		buffer.putShort((short) room.field.quadSize);
		buffer.putShort((short) room.settings.viewQuadPadding);
		length += 1 + 4 + 4;
	}

	public void setupPlayer(Player player) {
		buffer.put(ServerMessage.SETUP_PLAYER);
		buffer.putInt(player.id);
		buffer.put((byte) player.color);
		buffer.putShort((short) player.position.x);
		buffer.putShort((short) player.position.y);
		buffer.put((byte) player.direction.x);
		buffer.put((byte) player.direction.y);

		int nickLen = player.nick.length();
		buffer.put((byte) nickLen);
		writeStringUTF16(player.nick, buffer);

		length += 1 + 4 + 1 + 4 + 2 + 1 + nickLen * 2;
	}

	public void moveDirection(Player player) {
		buffer.put(ServerMessage.MOVE_DIRECTION);
		buffer.putInt(player.id);

		buffer.putShort((short) player.position.x);
		buffer.putShort((short) player.position.y);

		Point dir = player.direction;
		byte angle = 1;
		if (dir.x == 1) {
			angle = 2;
		} else if (dir.y == 1) {
			angle = 3;
		} else if (dir.x == -1) {
			angle = 4;
		}

		buffer.put(angle);
		length += 1 + 4 + 4 + 1;
	}

	public void newPlayer(Player player) {
		buffer.put(ServerMessage.NEW_PLAYER);
		buffer.putInt(player.id);
		buffer.put((byte) player.color);
		buffer.putShort((short) player.position.x);
		buffer.putShort((short) player.position.y);
		buffer.put((byte) player.direction.x);
		buffer.put((byte) player.direction.y);

		int nickLen = player.nick.length();
		buffer.put((byte) nickLen);
		writeStringUTF16(player.nick, buffer);

		length += 1 + 4 + 1 + 4 + 2 + 1 + nickLen * 2;
	}

	private static final void writeStringUTF16(String string, ByteBuffer outBuffer) {
		try {
			if (string.length() > 0) {
				byte[] stringAsBytes = string.getBytes("UTF-16");

				// miss first 2 bytes
				for (int i = 2; i < stringAsBytes.length; i++) {
					outBuffer.put(stringAsBytes[i]);
				}
			}

		} catch (UnsupportedEncodingException exc) {
			logger.error("fail encode string", exc);
		}
	}

	public void revealTail(Player player, PlayerTail tail) {
		buffer.put(ServerMessage.REVEAL_TAIL);
		buffer.putInt(player.id);
		buffer.putShort((short) tail.size);

		for (Point p : tail.points) {
			buffer.putShort((short) p.x);
			buffer.putShort((short) p.y);
		}
		length += 1 + 4 + 2 + 4 * tail.size;
	}

	public void diePlayer(Player player) {
		buffer.put(ServerMessage.DIE_PLAYER);
		buffer.putInt(player.id);
		length += 1 + 4;
	}

	public void revealQuad(int quadId) {
		buffer.put(ServerMessage.REVEAL_QUAD);
		buffer.putShort((short) quadId);
		length += 1 + 2;
	}

	public void revealQuadStop() {
		buffer.put(ServerMessage.REVEAL_QUAD_STOP);
		length += 1;
	}

	public void quadFill(int color, int start, int end) {
		buffer.put(ServerMessage.QUAD_FILL);
		buffer.put((byte) color);
		buffer.put((byte) start);
		buffer.put((byte) end);
		length += 1 + 3;
	}

	public void tailStart(Player player, PlayerTail tail) {
		Point p = tail.points.get(0);
		buffer.put(ServerMessage.TAIL_START);
		buffer.putInt(player.id);
		buffer.putShort((short) p.x);
		buffer.putShort((short) p.y);
		length += 1 + 4 + 4;
	}

	public void tailClear(Player player) {
		buffer.put(ServerMessage.TAIL_CLEAR);
		buffer.putInt(player.id);
		length += 1 + 4;
	}

	public void changeQuad(int quadId) {
		buffer.put(ServerMessage.CHANGE_QUAD);
		buffer.putShort((short) quadId);
		length += 1 + 2;
	}

	public void hidePlayer(int playerId) {
		buffer.put(ServerMessage.HIDE_PLAYER);
		buffer.putInt(playerId);
		length += 1 + 4;
	}

	public void earnScoreCapture(int score) {
		buffer.put(ServerMessage.EARN_SCORE_CAPTURE);
		buffer.putShort((short) score);
		length += 1 + 2;
	}

	public void earnScoreKill(int score) {
		buffer.put(ServerMessage.EARN_SCORE_KILL);
		buffer.putShort((short) score);
		length += 1 + 2;
	}

	public void rating(int rating, int totalPlayers) {
		buffer.put(ServerMessage.RATING);
		buffer.putShort((short) rating);
		buffer.putShort((short) totalPlayers);
		length += 1 + 2 + 2;
	}

	public void top10Line(Player player) {
		buffer.put(ServerMessage.TOP10_LINE);
		buffer.putInt(player.score.total);
		buffer.put((byte) player.color);

		int nickLen = player.nick.length();
		buffer.put((byte) nickLen);
		writeStringUTF16(player.nick, buffer);

		length += 1 + 4 + 1 + 1 + nickLen * 2;
	}

	public void top10Stop() {
		buffer.put(ServerMessage.TOP10_STOP);
		length += 1;
	}

	public void minimapLine() {
		buffer.put(ServerMessage.MINIMAP_LINE);
		length += 1;
	}

	public void minimapFill(int start, int end) {
		buffer.put(ServerMessage.MINIMAP_FILL);
		buffer.put((byte) start);
		buffer.put((byte) end);
		length += 3;
	}

	public void minimapStop() {
		buffer.put(ServerMessage.MINIMAP_STOP);
		length += 1;
	}

	// todo: remove this doubling func
	public void top10LineOriginal(Player player, int capturedPercent) {
		buffer.put(ServerMessage.TOP10_LINE);
		buffer.putInt(capturedPercent);
		buffer.put((byte) player.color);

		int nickLen = player.nick.length();
		buffer.put((byte) nickLen);
		writeStringUTF16(player.nick, buffer);

		length += 1 + 4 + 1 + 1 + nickLen * 2;
	}

	public void gameOver() {
		buffer.put(ServerMessage.GAME_OVER);
		buffer.put((byte) 0);
		length += 2;
	}

	public void gameOverOriginal(int fieldPercent, int kills, int total, int k1, int k2) {
		buffer.put(ServerMessage.GAME_OVER);
		buffer.put((byte) 1);
		buffer.putInt(fieldPercent);
		buffer.putInt(kills);
		buffer.putInt(total);
		buffer.putInt(k1);
		buffer.putInt(k2);
		length += 2 + 20;
	}

	// TOWERS

	public void gameOverTowers(int fieldPercent, int kills, int towers, int total, int k1, int k2, int k3) {
		buffer.put(ServerMessage.GAME_OVER);
		buffer.put((byte) 2);
		buffer.putInt(fieldPercent);
		buffer.putInt(kills);
		buffer.putInt(towers);
		buffer.putInt(total);
		buffer.putInt(k1);
		buffer.putInt(k2);
		buffer.putInt(k3);
		length += 2 + 28;
	}

	public void newTower(Tower tower) {
		buffer.put(ServerMessage.NEW_TOWER);
		buffer.putInt(tower.id);
		buffer.putInt(tower.quad.id);
		buffer.putShort((short) tower.x);
		buffer.putShort((short) tower.y);
		length += 1 + 4 + 4 + 2 + 2;
	}

	public void removeTower(Tower tower) {
		buffer.put(ServerMessage.REMOVE_TOWER);
		buffer.putInt(tower.id);
		length += 1 + 4;
	}

	public void earnScoreTowerCapture(int score) {
		buffer.put(ServerMessage.EARN_SCORE_TOWER_CAPTURE);
		buffer.putShort((short) score);
		length += 1 + 2;
	}

	// MINES

	public void gameOverMines(int fieldPercent, int kills, int mined, int total, int k1, int k2, int k3) {
		buffer.put(ServerMessage.GAME_OVER);
		buffer.put((byte) 3);
		buffer.putInt(fieldPercent);
		buffer.putInt(kills);
		buffer.putInt(mined);
		buffer.putInt(total);
		buffer.putInt(k1);
		buffer.putInt(k2);
		buffer.putInt(k3);
		length += 2 + 28;
	}

	public void newMine(Mine mine) {
		buffer.put(ServerMessage.NEW_MINE);
		buffer.putInt(mine.id);
		buffer.putInt(mine.quad.id);
		buffer.putShort((short) mine.x);
		buffer.putShort((short) mine.y);
		buffer.putShort((short) mine.charges);
		length += 1 + 4 + 4 + 2 + 2 + 2;
	}

	public void removeMine(Mine mine) {
		buffer.put(ServerMessage.REMOVE_MINE);
		buffer.putInt(mine.id);
		length += 1 + 4;
	}

	public void earnScoreMineCapture(int score) {
		buffer.put(ServerMessage.EARN_SCORE_MINE_CAPTURE);
		buffer.putShort((short) score);
		length += 1 + 2;
	}

	public void mineCharge(int mineId, int chargesRamain) {
		buffer.put(ServerMessage.MINE_CHARGE);
		buffer.putInt(mineId);
		buffer.putShort((short) chargesRamain);
		length += 1 + 4 + 2;
	}

}