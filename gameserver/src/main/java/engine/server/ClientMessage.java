
package engine.server;


import java.nio.ByteBuffer;
import engine.utils.Logger;
import engine.player.Player;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.Charset;
import java.nio.charset.CharacterCodingException;


public class ClientMessage {

	private static final Logger logger = new Logger("ClientMessage", Logger.DEBUG);

	// message types
	public static final byte Pong = 0;
	public static final byte MoveTop = 1;
	public static final byte MoveRight = 2;
	public static final byte MoveBottom = 3;
	public static final byte MoveLeft = 4;
	public static final byte UserInfo = 5;

	public static final void decodeAndRun(ByteBuffer message, Player player) {
		int bytePos = 0;
		int length = message.remaining();
		int messType = 0;

		while (bytePos < length) {
			messType = message.get();

			if (messType == ClientMessage.Pong) {
				ServerMessage pongMessage = new ServerMessage();
				pongMessage.pong();
				player.connection.send(pongMessage);
				bytePos += 1;
			} else if (messType == ClientMessage.MoveTop) {
				player.wantMove(0, -1);
				bytePos += 1;
			} else if (messType == ClientMessage.MoveRight) {
				player.wantMove(1, 0);
				bytePos += 1;
			} else if (messType == ClientMessage.MoveBottom) {
				player.wantMove(0, 1);
				bytePos += 1;
			} else if (messType == ClientMessage.MoveLeft) {
				player.wantMove(-1, 0);
				bytePos += 1;
			} else if (messType == ClientMessage.UserInfo) {
				bytePos += readUserInfo(message, player);
			} else {
				throw new IllegalArgumentException("cant decode server message with type " + messType);
			}
		}
	}

	private static final int readUserInfo(ByteBuffer message, Player player) {
		String userName = "";
		int nameLen = message.get();
		if (nameLen > 0) {
			userName = readStringUtf16(message, nameLen);
		}

		int hashLen = 32;
		String userHash = readStringUtf16(message, hashLen);

		player.setUserInfo(userName, userHash);
		return 2 + nameLen * 2 + 32 * 2;
	}

	private static final String readStringUtf16(ByteBuffer bytes, int strLen) {
		CharsetDecoder decode = Charset.forName("UTF16").newDecoder();
		String s = "";
		try {
			int startLimit = bytes.limit();
			bytes.limit(bytes.position() + strLen * 2);
			s = decode.decode(bytes).toString();
			bytes.limit(startLimit);

		} catch (CharacterCodingException exc) {
			logger.error("Fail string: ", exc);
		}
		return s;
	}

}
