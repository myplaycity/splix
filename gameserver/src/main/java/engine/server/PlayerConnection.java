
package engine.server;


import java.util.List;
import java.nio.ByteBuffer;

import org.java_websocket.WebSocketImpl;
import org.java_websocket.WebSocketListener;
import org.java_websocket.drafts.Draft;

import engine.utils.Logger;
import engine.player.Player;


public class PlayerConnection extends WebSocketImpl {

	protected static Logger logger = new Logger("PlayerConnection");
	public static int connIndex = 0;

	public Player player;
	public int id;

	public ServerMessage state = new ServerMessage();

	// base

	public PlayerConnection(WebSocketListener listener, List<Draft> drafts) {
		super(listener, drafts);
		this.id = connIndex++;
		logger.log("create new", this);
	}

	public PlayerConnection(WebSocketListener listener, Draft draft) {
		super(listener, draft);
		this.id = connIndex++;
		logger.log("create new", this);
	}

	public String toString() {
		return "PlayerConnection(id: " + id + ")";
	}

	// event reaction

	public void onOpen(Player player) {
		logger.log("on open", this);
		this.player = player;
		player.connection = this;
		player.isConnected = true;
	}

	public void onMessage(String message) {
		logger.log("on text message", message);
	}

	public void onMessage(ByteBuffer message) {
		try {
			ClientMessage.decodeAndRun(message, this.player);
		} catch (Exception exc) {
			logger.error("fail decode client message", exc);
		}
	}

	public void onClose(int code, String reason, boolean remote) {
		logger.log("on close", this);
		if (player.isPlaying) {
			player.die();
		}
		if (!player.isDestroyed) {
			player.destroy();
		}
		player.connection = null;
		player.isConnected = false;
		player = null;
	}

	public void onError(Exception exc) {
		logger.error("on connecion error", exc);
	}

	// sending

	public void send(ServerMessage message) {
		send(message.getData());
	}

	public void sendState() {
		if (isOpen()) {
			send(state.getData());
			state.clear();
		}
	}

}