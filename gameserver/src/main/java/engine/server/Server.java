
package engine.server;


import java.nio.ByteBuffer;
import java.net.InetSocketAddress;

import org.java_websocket.server.WebSocketServer;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.handshake.ClientHandshake;

import engine.utils.Logger;
import engine.room.Room;


public class Server extends WebSocketServer {

	private static final Logger logger = new Logger("Server", Logger.NORMAL);

	int port;
	Room room;

	public Server(int port, Room room) {
		super(new InetSocketAddress(port));
		PlayerConnectionFactory wsf = new PlayerConnectionFactory();
		setWebSocketFactory(wsf);

		this.port = port;
		this.room = room;
	}

	@Override
	public void start() {
		super.start();
		logger.log("start websocket server on " + port);
	}

	@Override
	public void stop(int timeout) throws InterruptedException {
		super.stop(timeout);
		logger.log("stop websocket server on " + port);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		PlayerConnection plconn = (PlayerConnection) conn;
		room.players.connectNew(plconn);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		PlayerConnection plconn = (PlayerConnection) conn;
		plconn.onMessage(message);
	}

	@Override
	public void onMessage(WebSocket conn, ByteBuffer message) {
		PlayerConnection plconn = (PlayerConnection) conn;
		plconn.onMessage(message);
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		PlayerConnection plconn = (PlayerConnection) conn;
		plconn.onClose(code, reason, remote);
	}

	@Override
	public void onError(WebSocket conn, Exception exc) {
		PlayerConnection plconn = (PlayerConnection) conn;
		plconn.onError(exc);
	}

}
