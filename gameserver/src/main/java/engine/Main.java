
package engine;


import engine.utils.Logger;
import engine.settings.Settings;
import engine.room.Room;
import engine.games.original.RoomOriginal;
import engine.games.towers.RoomTowers;
import engine.games.mines.RoomMines;
import engine.server.Server;


public class Main {

	private static final Logger logger = new Logger("Main", Logger.NORMAL);

	private static Settings settings;
	private static Room room;
	private static Server server;

	public static void main(String[] args) {
		readSettings(args);
		createRoom();
		createWebSocketServer();
		run();
	}

	private static void readSettings(String[] args) {
		String settingsPath = "";
		String roomName = "room";
		int wsPort = 8200;

		if (args.length == 3) {
			settingsPath = args[0];
			wsPort = Integer.valueOf(args[1]);
			roomName = args[2];
		} else {
			logger.error("setup args to start: settingsPath, wsPort, roomName");
			return;
		}

		settings = new Settings(settingsPath);
		settings.setMainArgs(roomName, wsPort);
	}

	private static boolean createRoom() {
		try {
			if (settings.type.equals("original")) {
				room = new RoomOriginal(settings);
			} else if (settings.type.equals("towers")) {
				room = new RoomTowers(settings);
			} else if (settings.type.equals("mines")) {
				room = new RoomMines(settings);
			} else {
				room = new Room(settings);
			}
		} catch (Exception exc) {
			logger.error("Fail run gameserver", exc);
			return false;
		}
		return true;
	}

	private static void createWebSocketServer() {
		server = new Server(settings.wsPort, room);
		server.start();
	}

	private static void run() {
		logger.print("-------------------");
		room.start();

		try {
			server.stop();
		} catch (Exception exc) {
			logger.error("fail stop ws server", exc);
		}
	}

}