
package engine.math;


import java.lang.Math;


public final class Point {

	public int x = 0;
	public int y = 0;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point(Point p) {
		this.x = p.x;
		this.y = p.y;
	}

	public final Point clone() {
		return new Point(this);
	}

	public final Point set(int x, int y) {
		this.x = x;
		this.y = y;
		return this;
	}

	public final Point set(Point p) {
		this.x = p.x;
		this.y = p.y;
		return this;
	}

	public final String toString() {
		return "Point(" + x + ", " + y + ")";
	}

	public final boolean equals(int x, int y) {
		return this.x == x && this.y == y;
	}

	public final boolean equals(Point p) {
		return x == p.x && y == p.y;
	}

	public final Point add(Point p) {
		x += p.x;
		y += p.y;
		return this;
	}

	public final Point add(int x, int y) {
		this.x += x;
		this.y += y;
		return this;
	}

	public final Point addRandom(int x1, int x2, int y1, int y2) {
		x += MathUtils.randInt(x1, x2);
		y += MathUtils.randInt(y1, y2);
		return this;
	}

	public final int diffDist(Point p) {
		return Math.abs(p.x - x) + Math.abs(p.y - y);
	}

	public final Point clamp(int minX, int minY, int maxX, int maxY) {
		if (x < minX) { x = minX; }
		if (y < minY) { y = minY; }
		if (x > maxX) { x = maxX; }
		if (y > maxY) { y = maxY; }
		return this;
	}

}
