
package engine.math;


import java.util.Random;


public final class MathUtils {

	private static final Random random = new Random();

	public final static int clamp(int value, int min, int max) {
		if (value < min) return min;
		if (value > max) return max;
		return value;
	}

	public final static int clampMax(int value, int max) {
		if (value > max) return max;
		return value;
	}

	public final static int clampMin(int value, int min) {
		if (value < min) return min;
		return value;
	}

	public final static int randInt(int min, int max) {
		return MathUtils.random.nextInt((max - min) + 1) + min;
	}

}
