
package engine.math;


public final class Bounds {

	public int x1;
	public int y1;
	public int x2;
	public int y2;

	@FunctionalInterface
	public interface PointCallback {
		public void run(int x, int y);
	}

	public void eachPoint(PointCallback callback) {
		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				callback.run(x, y);
			}
		}
	}

	public Bounds(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public Bounds(Point point) {
		x1 = point.x;
		y1 = point.y;
		x2 = point.x;
		y2 = point.y;
	}

	public Bounds(Point point, int indent) {
		this(point);
		indent(indent);
	}

	public Bounds(Bounds bounds) {
		x1 = bounds.x1;
		y1 = bounds.y1;
		x2 = bounds.x2;
		y2 = bounds.y2;
	}

	public final String toString() {
		return "Bounds(" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + ")";
	}

	public final boolean equals(Bounds bounds) {
		return bounds.x1 == x1 && bounds.y1 == y1 && bounds.x2 == x2 && bounds.y2 == y2;
	}

	public final Bounds set(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		return this;
	}

	public final Bounds set(Bounds bounds) {
		x1 = bounds.x1;
		y1 = bounds.y1;
		x2 = bounds.x2;
		y2 = bounds.y2;
		return this;
	}

	public final Bounds set(int x, int y, int indent) {
		x1 = x;
		y1 = y;
		x2 = x;
		y2 = y;
		return indent(indent);
	}

	public final Bounds indent(int value) {
		x1 -= value;
		y1 -= value;
		x2 += value;
		y2 += value;
		return this;
	}

	public final Bounds div(int value) {
		if (value > 0) {
			x1 /= value;
			y1 /= value;
			x2 /= value;
			y2 /= value;
		}
		return this;
	}

	public final Bounds extend(int x, int y) {
		if (x < x1) { x1 = x; }
		if (y < y1) { y1 = y; }
		if (x > x2) { x2 = x; }
		if (y > y2) { y2 = y; }
		return this;
	}

	public final boolean has(int x, int y) {
		return (x >= x1 && x <= x2) && (y >= y1 && y <= y2);
	}

	public final Bounds clamp(int minX, int minY, int maxX, int maxY) {
		if (x1 < minX) { x1 = minX; }
		if (y1 < minY) { y1 = minY; }
		if (x2 > maxX) { x2 = maxX; }
		if (y2 > maxY) { y2 = maxY; }
		return this;
	}

}
