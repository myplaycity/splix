#!/bin/bash

clear;
echo "";
echo "--- Build gameserver and restart -------------------------------";
echo "";
cd /home/suharev/work/splix/gameserver && gradle buildJar && supervisorctl restart splix:* && tail -16f /var/log/splix/game-8202.log;