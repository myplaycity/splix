<?php

// mb_internal_encoding("UTF-8");
define('DOCUMENT_ROOT', __DIR__);
require_once 'app/lib/Functions.php';
require_once 'app/Autoload.php';

use \app\MigrationApp;
use \app\lib\Logger;
use \app\lib\Debug;

if ($logFilePath = $_SERVER['LOG_FILE_PATH'] ?? null) {
	Logger :: init($logFilePath);
}

Debug :: init($_SERVER['SHOW_DEBUG'] ?? false);

$app = new MigrationApp($_SERVER['ENVIRONMENT'] ?? 'local');
$app -> start();
$app -> run();
$app -> end();

exit;
