<?php

namespace app;


use \Exception;
use \app\lib\Memcache;
use \app\lib\HttpClient;
use \app\lib\Logger;
use \app\lib\LoggableClass;


class Servers extends LoggableClass {

	public $memcache;
	public $settings;
	public $httpClient;

	const MAX_PLAYERS = 100;

	public function __construct(Memcache $memcache, array $settings) {
		$this -> memcache = $memcache;
		$this -> settings = $settings;
	}

	public function getBest(string $serverType = 'base') {
		$servers = $this -> memcache -> servers;
		$firstOptimalServer = null;

		if (is_array($servers)) {
			foreach ($servers as $server) {
				$players = $server['players'];
				if ($players < self :: MAX_PLAYERS && $server['type'] === $serverType) {
					$firstOptimalServer = $server;
					break;
				}
			}
		}

		if (!$firstOptimalServer) {
			self :: $logger -> log("cant find optimal server!", $servers);
		}
		return $firstOptimalServer;
	}

	public function getList() {
		$servers = $this -> memcache -> servers;
		if (is_array($servers)) {
			return $servers;
		}
		return null;
	}

	public function update() {
		$this -> httpClient = new HttpClient();
		$adresses = $this -> settings;
		$servers = [];
		$playingUsers = 0;

		for ($i = 0; $i < count($adresses); $i++) {
			try {
				$infoText = $this -> httpClient -> get($adresses[$i]);
				$info = parse_ini_string($infoText);
				if ($info === false) {
					throw new Exception("fail parse room status: " . $infoText);
				}
				$servers[] = [
					'adress' => $adresses[$i],
					'ws' => $info['ws'],
					'name' => $info['name'],
					'type' => $info['type'],
					'players' => $info['players']
				];
				$playingUsers += (int) $info['players'];
			} catch (Exception $e) {
				self :: $logger -> error("fail get room info:", $e);
			}
		}

		$this -> memcache -> servers = $servers;

		$load = (int) ($playingUsers / (self :: MAX_PLAYERS * count($servers)) * 100);
		self :: $logger -> log("updated: " . count($servers) . " servers, $load% load");

		return $servers;
	}

}
