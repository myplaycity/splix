<?php

spl_autoload_register('appAutoload');


function appAutoload($class) {
	$path = explode('\\', $class);
	$file = DOCUMENT_ROOT . "/" . implode('/', $path) . ".php";
	if ($file and is_readable($file)) {
		include_once $file;
	}

	// check if class exists
	if (class_exists($class, false) or interface_exists($class, false)) {
		if (method_exists($class, '__init')) {
			$class :: __init();
		}
		return true;
	} else {
		throw new AutoloadException("unable to autoload class: '$class' from '$file'");
	}
}