<?php

namespace app;


use \app\lib\App;
use \app\lib\Logger;
use \app\lib\Settings;
use \app\lib\Request;
use \app\lib\Response;
use \app\lib\Router;
use \app\lib\Memcache;
use \app\lib\Mysql;
use \app\lib\MysqlModel;
use \app\models\RecordModel;
use \app\models\UserModel;
use \Exception;
use \PublicException;


class MigrationApp extends App {

	// init static logging

	protected static $logger = null;

	public static function __init() {
		self :: $logger = new Logger(__CLASS__, Logger :: NORMAL);
	}

	// app

	public $settings;
	public $mysql;

	public function start() {
		try {
			$this -> settings = new Settings("/app/settings/{$this->env}.php");
			$this -> mysql = new Mysql($this -> settings -> mysql);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail start application.", $e, $this);
		}
	}

	public function run() {
		try {
			$this -> install();

		} catch (PublicException $e) {
			$this -> response -> set(['error' => $e -> getMessage()]);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail execute request.", $e, $this);
		}
	}

	public function install() {
		self :: $logger -> log("Installing migrations...");
		$mm = new lib\MysqlMigrationManager($this -> mysql, 'migrations', DOCUMENT_ROOT . '/app/migrations');
		$mm -> installAll();
	}

}
