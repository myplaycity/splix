<?php

use \app\lib\AbstractMysqlMigration;


class FirstMigration extends AbstractMysqlMigration {

	public function up() {
		$records = new lib\MysqlTable($this -> mysql, 'records');
		$records -> create([
			'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			'userId' => 'INT(11) NOT NULL',
			'name' => 'VARCHAR(10)',
			'score' => 'INT(11) NOT NULL',
			'capture' => 'INT(11) NOT NULL',
			'kills' => 'INT(11) NOT NULL',
			'tail' => 'INT(11) NOT NULL',
			'timeAlive' => 'INT(11) NOT NULL',
			'timeTop1' => 'INT(11) NOT NULL'
		]);
		$records -> addIndex('userId', ['userId']);

		$users = new lib\MysqlTable($this -> mysql, 'users');
		$users -> create([
			'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			'name' => 'VARCHAR(10) NOT NULL',
			'hash' => 'CHAR(32) NOT NULL'
		]);
		$users -> addUnique('hash', ['hash']);
	}

	public function down() {

	}

}
