<?php

namespace app\lib;


/*

	Debug class

*/
class Debug {

	public static $show = false;

	/**
	 * Initialize debug, set errors handlers
	 * If logfile is set, returns true
	 *
	 * @return bool
	 */
	public static function init($show = false) {
		// enable debug mode
		self :: $show = $show;

		// init error handlers
		error_reporting(E_ALL);
		// ini_set('display_errors', false);

		// set handlers
		set_error_handler(__CLASS__ . '::handleError');
		set_exception_handler(__CLASS__ . '::handleException');
		register_shutdown_function(__CLASS__ . '::handleFatal');
	}

	/**
	 * Removes custom error handlers
	 */
	public static function stop() {
		restore_error_handler();
		restore_exception_handler();
	}

	/**
	 * Handles application shutdown, and it last error, if exist
	 *
	 * @return bool
	 */
	public static function handleFatal() {
		if ($error = error_get_last()) {
			extract($error);
			self :: handleError($type, $message, $file, $line);
		}
		return true;
	}

	/**
	 * Handles application errors.
	 *
	 * @param int $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int $errline
	 * @return bool
	 */
	public static function handleError($errno, $errstr, $errfile, $errline) {
		// error was suppressed with the @-operator
		if (error_reporting() === 0)
			return false;

		// log error
		$errfile = str_replace('\\', '/', $errfile);
		$errfileShort = str_replace(DOCUMENT_ROOT, '', $errfile);
		$errtype = $errno;

		switch ($errno) {
			case E_ERROR: $errtype = "ERROR"; break;
			case E_WARNING: $errtype = "WARNING"; break;
			case E_PARSE: $errtype = "PARSE"; break;
			case E_NOTICE: $errtype = "NOTICE"; break;
			case E_USER_ERROR: $errtype = "USER_ERROR"; break;
			case E_USER_WARNING: $errtype = "USER_WARNING"; break;
			case E_USER_NOTICE: $errtype = "USER_NOTICE"; break;
	    }

		$message = "[$errtype] $errstr in $errfileShort ($errline)\n";

		// show and die (except notice)
		// todo: make decision, hide notice or not
		// if ((E_ALL ^ (E_NOTICE | E_USER_NOTICE)) & $errno) {
			if (self :: $show) {
				self :: showError($message, $errfile, $errline, debug_backtrace());
			}
			exit;
		// }

		return true;
	}

	/**
	 * Handles exceptions
	 * Uncaught exceptions will close application
	 *
	 * @param Exception $ex
	 * @param bool $fatal
	 * @return bool
	 */
	public static function handleException($ex, $fatal = true) {
		$mess = $ex -> getMessage();
		$errFile = $ex -> getFile();
		$errLine = $ex -> getLine();
		$fileData = $errFile ? ", in $errFile ($errLine)" : '';
		$fullTrace = $ex -> getTrace();
		$message = $mess . $fileData . "\n";

		// show error message
		if (self :: $show) {
			self :: showError('[EXCEPTION] ' . $message, $errFile, $errLine, $fullTrace);
		}

		// unhandled exception ar fatals by default
		if ($fatal)
			exit;

		return true;
	}

	/**
	 * Echoes message to output with nice red-block-html wrap
	 *
	 * @param string $msg
	 */
	private static function showError($msg, $file = null, $line = null, $trace = null) {
		echo '<div style="margin: 10px; font: normal 14px Consolas,Courier;">';
		echo '<pre style="margin: 0; padding: 20px; background: #fbb; white-space: pre-wrap;">' . $msg . '</pre>';
		if ($file)
			echo self :: showSourceLines($file, $line);
		if ($trace)
			echo self :: showTrace($trace);
		echo '</div>';
	}

	/**
	 * Prints file lines, where error was occured
	 *
	 * @param string $file
	 * @param int $errLine
	 * @return string
	 */
	private static function showSourceLines($file, $errLine) {
		$content = file_get_contents($file);
		$content = explode("\n", $content);
		$lines = array();
		$errLine--;

		foreach ($content as $i => $str) {
			if (($i >= $errLine - 8) && ($i <= $errLine + 8)) {
				$str = htmlspecialchars($str);
				$errStyleLine = $i == $errLine ? 'background: #FF8A8A;' : '';
				$errStyleCode = $i == $errLine ? 'background: #fbb;' : '';

				$lines[] = "<tr>
					<td style=\"width: 1px; padding: 2px 20px; background: #E2E2E2; white-space: nowrap; font-size:14px;$errStyleLine\">" . ($i + 1) . "</td>
					<td><pre style=\"font-size:14px;padding: 2px 20px;white-space:pre-wrap; $errStyleCode\">$str</pre></td>
				</tr>";
			}
		}

		if (count($lines) > 0) {
			$lines = implode("\n", $lines);
			return '<table style="background: #ECEFF0; width:100%; margin: 0;" cellpadding="0" cellspacing="0">' . $lines . '</table>';
		}

		return '';
	}

	private static function showTrace(array $trace) {
		foreach ($trace as $i => $step) {
			$fileData = $functionData = $arguments = '';
			if (isset($step['file'])) {
				$fileData = $step['file'] . ' (' . $step['line'] . ')';
			}
			if (isset($step['function'])) {
				$functionData = $step['function'] . '()';
				if (isset($step['class'])) {
					$functionData = $step['class'] . ' ' . $step['type'] . ' ' . $functionData;
				}
			}
			if (isset($step['args'])) {
				$args = [];
				foreach ($step['args'] as $a) {
					$args[] = varToHtml($a);
				}
				$arguments = implode("\n", $args);
			}
			$lines[] = "<tr>
				<td style=\"width: 1px; padding: 10px 20px; background: #E2E2E2; white-space: nowrap; font-size:14px;vertical-align:top\">" . ($i + 1) . "</td>
				<td style=\"vertical-align:top\"><pre style=\"font-size:14px;padding: 10px 20px;\">" . $functionData . "<br><span style=\"color: #888;\">" . $fileData . "</span></pre></td>
				<td style=\"vertical-align:top\"><pre style=\"font-size:14px;padding: 10px 20px;white-space:pre-wrap;\">" . $arguments . "</pre></td>
			</tr>";
		}

		if (count($lines) > 0) {
			$lines = implode("\n", $lines);
			return '<table style="background: #ECEFF0; width:100%; margin: 0;" cellpadding="0" cellspacing="0">'
				. '<tr><td colspan="3" style="width: 1px; padding: 10px 20px; background: #C2C2C2; white-space: nowrap; font-size:14px;vertical-align:top">Trace</td></tr>'
				. $lines . '</table>';
		}

		return '';
	}

}
