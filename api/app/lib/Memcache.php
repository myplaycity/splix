<?php

namespace app\lib;


use \Debugable;
use \PrivateException;


class Memcache {

	public $host;
	public $port;
	public $prefix;
	private $memcache;

	public function __construct(array $connectionSettings) {
		$this -> host = $connectionSettings['host'];
		$this -> port = $connectionSettings['port'];
		$this -> prefix = $connectionSettings['prefix'];
		$memcache = null;
	}

	private function lazyConnect() {
		if (!$this -> memcache) {
			$this -> memcache = new \Memcache;
			if (!$this -> memcache -> connect($this -> host, $this -> port)) {
				throw new PrivateException("fail connect to memcache server");
			}
		}
	}

	public function __get(string $name) {
		$this -> lazyConnect();
		return $this -> memcache -> get($this -> prefix . $name);
	}

	public function __set(string $name, $value) {
		$this -> lazyConnect();
		$this -> memcache -> set($this -> prefix . $name, $value);
	}

}