<?php

namespace app\lib;


abstract class AbstractMysqlMigration {

	abstract public function up();

	abstract public function down();

}
