<?php

namespace app\lib;


use \MysqlException;


/**
 * Class for manipulating MYSQL tables
 */
class MysqlTable {

	private $mysql;
	public $name;
	public $fullName;

	public function __construct(Mysql $mysql, string $tableName) {
		$this -> mysql = $mysql;
		$this -> name = $tableName;
		$this -> fullName = $mysql -> prefix . $tableName;
	}

	public function showColumns() {
		return $this -> mysql -> selectArray("SHOW COLUMNS FROM `{$this->fullName}`");
	}

	public function showCreateTable() {
		$result = $this -> mysql -> selectArrayOne("SHOW CREATE TABLE `{$this->fullName}`");
		if (is_array($result)) {
			return $result["Create Table"];
		}
		return false;
	}

	public function create(array $columns, string $primaryKey = 'id', string $engine = 'InnoDB', int $autoIncrement = 0, string $charset = 'utf8') {
		$parts = [];
		foreach ($columns as $name => $type) {
			$parts[] = "`$name` $type";
		}
		$parts[] = "PRIMARY KEY (`$primaryKey`)";

		$partsString = implode(", ", $parts);
		$sql = "CREATE TABLE `{$this->fullName}` ($partsString)";
		$sql .= " ENGINE=$engine AUTO_INCREMENT=$autoIncrement DEFAULT CHARSET=$charset";
		return $this -> mysql -> query($sql);
	}

	public function drop() {
		$sql = "DROP TABLE `{$this->fullName}`";
		return $this -> mysql -> query($sql);
	}

	public function addColumn(string $name, string $type, string $after = '') {
		$sql = "ALTER TABLE `{$this->fullName}` ADD `$name` $type $after";
		return $this -> mysql -> query($sql);
	}

	public function changeColumn(string $name, string $type) {
		$sql = "ALTER TABLE `{$this->fullName}` MODIFY `$name` $type";
		return $this -> mysql -> query($sql);
	}

	public function dropColumn(string $name) {
		$sql = "ALTER TABLE `{$this->fullName}` DROP COLUMN `$name`";
		return $this -> mysql -> query($sql);
	}

	public function addIndex(string $name, array $columns) {
		$columnsString = "";
		foreach ($columns as $col) {
			$columnsString .= "`$col`,";
		}
		$columnsString = trim($columnsString, ',');
		$sql = "ALTER TABLE `{$this->fullName}` ADD INDEX `$name` ($columnsString)";
		return $this -> mysql -> query($sql);
	}

	public function addUnique(string $name, array $columns) {
		$columnsString = "";
		foreach ($columns as $col) {
			$columnsString .= "`$col`,";
		}
		$columnsString = trim($columnsString, ',');
		$sql = "ALTER TABLE `{$this->fullName}` ADD UNIQUE `$name` ($columnsString)";
		return $this -> mysql -> query($sql);
	}

	public function dropIndex(string $name) {
		return $this -> mysql -> query("ALTER TABLE `{$this->fullName}` DROP INDEX `$name`");
	}

	public function fetchAll(): array {
		return $this -> mysql -> selectRows($this -> name);
	}

	public function insert(array $data) {
		return $this -> mysql -> insert($this -> name, $data);
	}

}
