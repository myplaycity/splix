<?php

namespace app\lib;


use \Debugable;
use \Jsonable;
use \ModelException;


class MysqlModel implements Debugable, Jsonable {

	// mysql driver linkage

	protected static $mysql = null;

	public static function init($mysql) {
		self :: $mysql = $mysql;
	}

	// implement Debugable

	public function __debug() {
		return get_object_vars($this);
	}

	public function __toJson() {
		return get_object_vars($this);
	}

	// model

	public $id = 0;
	protected $__created = false;

	/**
	 * Returns table name for this model
	 */
	public static function table(): string {}

	/**
	 * Returns fields, what mysql table contains.
	 * This need to make correct SQL for UPDATE and INSERT queries
	 */
	public static function describe(): array {
		return ['id'];
	}

	/**
	 * Construct model with $fields
	 */
	public function __construct(array $fields = null) {
		if ($fields) {
			$this -> setFields($fields);
		}
	}

	/**
	 * Sets fields from array
	 * fields filtered by describe() - to make them SQL correct for this table
	 */
	public function setFields(array $fields) {
		foreach ($fields as $k => $v) {
			if (is_numeric($v)) {
				$this -> {$k} = $v + 0;
			} else {
				$this -> {$k} = $v;
			}
		}
	}

	/**
	 * Get fields to use in sql INSERT and UPDATE messages
	 */
	public function getFields() {
		$describe = array_flip(static :: describe());
		return array_intersect_key(get_object_vars($this), $describe);
	}

	/**
	 * Basic find models, uses $where as array of conditions
	 * $order and $limit to make query
	 */
	public static function find(array $where, array $order = null, int $limit = 0, int $skip = 0) {
		return self :: $mysql
			-> where($where)
			-> order($order)
			-> limit($skip, $limit)
			-> selectModels(static :: table(), get_called_class());
	}

	/**
	 * Basic find one model by conditions
	 */
	public static function findOne(array $where, array $order = null) {
		return self :: $mysql
			-> where($where)
			-> order($order)
			-> selectModel(static :: table(), get_called_class());
	}

	/**
	 * Find model by id
	 */
	public static function get(int $id) {
		return static :: findOne(['id' => $id], []);
	}

	/**
	 * Get count of models by $where condition
	 */
	public static function count(array $where = null) {
		$result = self :: $mysql
			-> fields(['COUNT(*)' => 'count'])
			-> where($where)
			-> selectRow(static :: table());
		return $result['count'];
	}

	/**
	 * Saves model to table. makes insert or update query, depends on id exists
	 */
	public function save() {
		$fields = $this -> getFields();
		if (!is_array($fields) or count($fields) === 0) {
			throw new ModelException('Invalid fields to save model: ' . strvar($fields));
		}

		if ($this -> id === 0) {
			$this -> __created = true;
			self :: $mysql -> insert(static :: table(), $fields);
			$this -> id = self :: $mysql -> insertId();
		} else {
			self :: $mysql -> where(['id' => $this -> id]) -> update(static :: table(), $fields);
			$this -> __created = false;
		}
	}

	/**
	 * Deletes this model from table
	 */
	public function delete() {
		if (!$this -> id)
			throw new ModelException("Can not delete model with no id");

		self :: $mysql -> where(['id' => $this -> id]) -> delete(static :: table());
		return $this;
	}

}