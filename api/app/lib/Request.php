<?php

namespace app\lib;


use \BadRequestException;


class Request {

	public $uri = '';
	public $path = '';
	public $query = '';

	public function __construct() {
		$this -> uri = $_SERVER['REQUEST_URI'] ?? '';

		if ($u = parse_url($this -> uri)) {
			$this -> path = $u['path'] ?? '/';
			$this -> query = $u['query'] ?? '';
		} else {
			throw new BadRequestException('fail parse request uri' . $this -> uri);
		}
	}

	public function __get($name) {
		if (isset($_REQUEST[$name])) {
			$value = $_REQUEST[$name];
			if (is_numeric($value)) {
				return $value + 0;
			} else {
				return $value;
			}
		}
		return null;
	}

	public function get(string $what, $default = null) {
		return $_GET[$what] ?? $default;
	}

	public function post(string $what, $default = null) {
		return $_POST[$what] ?? $default;
	}

}