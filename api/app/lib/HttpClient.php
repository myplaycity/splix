<?php

namespace app\lib;


use \PrivateException;


class HttpClient {

	public $url = '';
	public $response = '';
	public $httpCode = 0;

	public function get(string $url): string {
		$this -> url = $url;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this -> url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$this -> response = curl_exec($curl);
		$this -> httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		if ($this -> httpCode !== 200) {
			throw new PrivateException("fail make http request");
		}

		return $this -> response;
	}

}