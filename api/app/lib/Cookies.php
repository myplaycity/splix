<?php

namespace app\lib;


use \Debugable;


class Cookies {

	const DEF_COOKIE_LIFETIME = 2592000; // 1 month

	public function __get(string $name): string {
		if (isset($_COOKIE[$name])) {
			$value = $_COOKIE[$name];
			if (is_numeric($value)) {
				return $value + 0;
			} else {
				return $value;
			}
		}
		return null;
	}

	public function get(string $name, $default = null): string {
		return $_COOKIE[$name] ?? $default;
	}

	public function has(string $name): bool {
		return isset($_COOKIE[$name]);
	}

	public function set(string $name, $value, string $path = '/'): bool {
		return setcookie($name, $value, time() + self :: DEF_COOKIE_LIFETIME, $path);
	}

	public function remove(string $name): bool {
		return setcookie($name, '', time() - self :: DEF_COOKIE_LIFETIME, '/');
	}

	public function __debug() {
		return $_COOKIE;
	}

}