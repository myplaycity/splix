<?php

namespace app\lib;


class Template {

	public $fileName;
	public $__data;

	public function __construct(string $fileName) {
		$this -> fileName = DOCUMENT_ROOT . $fileName;
		$this -> __data = [];
	}

	public function import(array $data) {
		foreach ($data as $key => $value) {
			$this -> __data[$key] = $value;
		}
	}

	public function __get(string $name) {
		if (isset($this -> __data[$name])) {
			return $this -> __data[$name];
		}
		return '';
	}

	public function render(array $data = null): string {
		if (is_array($data)) {
			$this -> import($data);
		}
		ob_start();
		include($this -> fileName);
		return ob_get_clean();
	}

}