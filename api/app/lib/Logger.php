<?php

namespace app\lib;


use \Exception;
use \PrivateException;


class Logger {

	const ANSI_RESET = "\033[0m";
	const ANSI_GRAY = "\033[1;30m";
	const ANSI_RED = "\033[1;31m";
	const ANSI_GREEN = "\033[1;32m";
	const ANSI_YELLOW = "\033[1;33m";
	const ANSI_BLUE = "\033[1;34m";

	const HIDDEN = 0;
	const NORMAL = 1;
	const DEBUG = 2;

	private $name;
	private $level;

	private static $logFilePath = null;
	private static $fileHandle = null;

	public static function init(string $logFilePath) {
		self :: $logFilePath = $logFilePath;
	}

	private static function openHandle() {
		self :: $fileHandle = fopen(self :: $logFilePath, 'a');
		if (self :: $fileHandle === false) {
			throw new PrivateException('The log file handle is not initialized.');
		}
	}

	public function __construct(string $loggerName, int $level = Logger :: HIDDEN) {
		$this -> name = " [" . $loggerName . "] ";
		$this -> level = $level;
	}

	public function print(string $message) {
		self :: openHandle();
		fwrite(self :: $fileHandle, $message . "\n");
	}

	private function date(): string {
		return date("Y-m-d H:i:s");
	}

	public function log(string $message) {
		if ($this -> level > Logger :: HIDDEN) {
			$messColor = Logger :: ANSI_RESET;
			if ($this -> level === Logger :: DEBUG) {
				$messColor = Logger :: ANSI_YELLOW;
			}

			$line = Logger :: ANSI_GRAY
				. $this -> date()
				. Logger :: ANSI_BLUE
				. $this -> name
				. $messColor
				. $message . ' ';

			if (($numArgs = func_num_args()) > 1) {
				$args = func_get_args();
				for ($i = 1; $i < $numArgs; $i++) {
					$line .= strvar($args[$i]) . ' ';
				}
			}

			$this -> print($line);
		}
	}

	public function error(string $message, Exception $exc) {
		$line = Logger :: ANSI_GRAY
			. $this -> date()
			. Logger :: ANSI_BLUE
			. $this -> name
			. Logger :: ANSI_RED
			. $message . ' '
			. $exc -> getMessage() . "\n"
			. "-------------------\n"
			. $this -> formatExceptionTrace($exc -> getTrace());

		if (($numArgs = func_num_args()) > 2) {
			$args = func_get_args();
			$line .= "-------------------\n";
			for ($i = 2; $i < $numArgs; $i++) {
				$line .= strvar($args[$i]) . ' ';
			}
			$line .= "\n-------------------\n";
		}

		$line .= Logger :: ANSI_RESET;
		$this -> print($line);
	}

	private function formatExceptionTrace(array $trace): string {
		foreach ($trace as $i => $step) {
			$fileData = '';
			$functionData = '';
			if (isset($step['file'])) {
				$file = str_replace(DOCUMENT_ROOT, "", $step['file']);
				$fileData = 'in ' . $file . ' (' . $step['line'] . ')';
			}
			if (isset($step['function'])) {
				$functionData = $step['function'] . '()';
				if (isset($step['class'])) {
					$functionData = $step['class'] . ' :: ' . $functionData;
				}
			}
			$lines[] = "- $functionData $fileData";
		}

		if (count($lines) > 0) {
			return "  " . implode("\n  ", $lines) . "\n";
		}

		return '';
	}

}