<?php

namespace app\lib;


use \MysqlException;
use \PrivateException;
use \Exception;


class MysqlMigrationManager {

	// logging

	protected static $logger = null;

	public static function __init() {
		self :: $logger = new Logger(__CLASS__, Logger :: NORMAL);
	}

	// data

	private $mysql;
	public $dir = '';
	public $table = null;
	public $installed = null;
	public $processing = null;

	public function __construct(Mysql $mysql, string $migrationsTable, string $migrationsDir) {
		$this -> mysql = $mysql;
		$this -> table = new MysqlTable($mysql, $migrationsTable);
		$this -> installed = [];
		$this -> dir = $migrationsDir;
	}

	public function installAll(): int {
		$all = $this -> getAllMigrations();
		$allInstalled = $this -> getBaseMigrations();
		$new = $this -> findNewMigrations($all, $allInstalled);

		if (count($new) > 0) {
			foreach ($new as $migr) {
				if ($this -> installMigration($migr)) {
					$this -> installed[] = $migr;
				}
			}
			// todo: log installed result
			return count($this -> installed);
		}
		return 0;
	}

	public function installMigration(AbstractMysqlMigration $migration): bool {
		try {
			$this -> processing = $migration;
			$migration -> up();
			// todo: save to migr table
			// $this -> table -> insert();
			self :: $logger -> log("Installed", get_class($migration));
			return true;
		} catch (MysqlException $e) {
			// todo: do some
			return false;
		} catch (Exception $e) {
			// todo: do some
			return false;
		}
	}

	private function getAllMigrations(): array {
		$files = scandir($this -> dir);
		$migrations = [];
		foreach ($files as $file) {
			if ($file !== '.' && $file !== '..') {
				require_once($this -> dir . '/' . $file);
				$migrClass = explode('.php', $file)[0];
				if (class_exists($migrClass, false)) {
					$migrations[] = new $migrClass();
				} else {
					throw new PrivateException("Migration class '$migrClass' not found in file '$file'.");
				}
			}
		}
		return $migrations;
	}

	private function getBaseMigrations(): array {
		try {
			$list = $this -> table -> fetchAll();
			$ids = [];
			foreach ($list as $row) {
				$ids[] = $row['id'];
			}
			return $ids;
		} catch (MysqlException $e) {
			return [];
		}
	}

	private function findNewMigrations($all, $installed): array {
		$new = [];
		foreach ($all as $migr) {
			$id = get_class($migr);
			if (array_search($id, $installed) === false) {
				$new[] = $migr;
			}
		}
		return $new;
	}

	// public function rollbackMigration(string $migrId) {
		// $downMigrations = [];
		// foreach ($downMigrations as $mig) {
		// 	$mig -> down();
		// }
	// }

}
