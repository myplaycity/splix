<?php

namespace app\lib;


use \Exception;
use \PublicException;


class App {

	public $env = '';
	public $startTime = 0;
	public $responseTimeMs = 0;
	public $host = '';
	public $request;
	public $response;

	public function __construct(string $env) {
		$this -> env = $env;
		$this -> startTime = microtime(true);
		$this -> host = $_SERVER['HTTP_HOST'] ?? '';
		$this -> request = new Request();
		$this -> response = new Response();
	}

	public function start() {}

	public function run() {}

	public function end() {
		$this -> response -> give();
		$this -> responseTimeMs = (microtime(true) - $this -> startTime) * 1000;
	}

}