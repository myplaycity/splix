<?php

/**
 * Makes stylish output of var on screen.
 */
function varToHtml($mixedVar, string $indent = ''): string {
	if (is_array($mixedVar)) {
		$string = '';
		foreach ($mixedVar as $i => $v) {
			$string .= "  $indent" . wrapSpan('7F7F7F', $i) . ': ' . varToHtml($v, $indent . '  ') . "\n";
		}
		$string = $string ? "\n" . $string . $indent : '';
		return wrapSpan('0B78B9', 'array') . ' [' . $string . ']';

	} elseif (is_object($mixedVar)) {
		$objVars = $mixedVar instanceof Debugable ? $mixedVar -> __debug() : get_object_vars($mixedVar);
		$content = '';
		foreach ($objVars as $i => $v) {
			$val = varToHtml($v, $indent . '  ');
			$content .= "  $indent" . wrapSpan('7F7F7F', $i) . ": $val\n";
		}
		$objContent = $content ? "\n" . $content . $indent : '';
		return wrapSpan('0B78B9', get_class($mixedVar)) . ' {' . $objContent . '}';

	} elseif (is_int($mixedVar)) {
		return wrapSpan('EA0058', $mixedVar);

	} elseif (is_float($mixedVar)) {
		return wrapSpan('FF090F', $mixedVar);

	} elseif (is_bool($mixedVar)) {
		return wrapSpan('FF090F', ($mixedVar ? 'true' : 'false'));

	} elseif (is_null($mixedVar)) {
		return wrapSpan('FF090F', 'null');

	} else {
		return wrapSpan('379556', htmlspecialchars($mixedVar));
	}
}

function wrapSpan(string $color, string $content): string {
	return "<span style=\"color: #$color\">$content</span>";
}

function debugVars() {
	$args = func_get_args();
	if (count($args) > 0) {
		foreach ($args as $var) {
			$msg = varToHtml($var);
			echo '<pre style="padding: 20px; margin: 10px; background: #E9F3EE; white-space: pre-wrap;">' . $msg . '</pre>';
		}
	}
}


/**
 * Makes string output of var (special style for logs)
 */
function strvar($mixedVar, string $indent = ''): string {
	if (is_array($mixedVar)) {
		$content = '';
		foreach ($mixedVar as $i => $v) {
			$val = strvar($v, $indent . '  ');
			if (is_numeric($i)) {
				$line = $val;
			} else {
				$line = $i . ': ' . $val;
			}
			$content .= $indent . '  ' . $line . ",\n";
		}
		$content = rtrim($content, ",\n");
		return $content ? "[\n" . $content . "\n" . $indent . "]" : "[]";

	} elseif (is_object($mixedVar)) {
		$objVars = $mixedVar instanceof Debugable ? $mixedVar -> __debug() : get_object_vars($mixedVar);
		$content = '';
		foreach ($objVars as $i => $v) {
		 	$val = strvar($v, $indent . '  ');
		 	$content .= $indent . '  ' . $i . ': ' . $val . ",\n";
		}
		return get_class($mixedVar) . " {\n" . rtrim($content, ",\n") . "\n" . $indent . "}";

	} elseif (is_int($mixedVar)) {
		return (string) $mixedVar;
	} elseif (is_float($mixedVar)) {
		return (string) $mixedVar;
	} elseif (is_bool($mixedVar)) {
		return $mixedVar === true ? 'true' : 'false';
	} elseif (is_null($mixedVar)) {
		return 'null';
	} else {
		return '"' . (string) $mixedVar . '"';
	}
}


class PrivateException extends \Exception {}
class PublicException extends \Exception {}

class AutoloadException extends PrivateException {}
class MysqlException extends PrivateException {}
class ModelException extends PrivateException {}
class BadResponseException extends PrivateException {}

class BadRequestException extends PublicException {}
class NotFoundException extends PublicException {}
class ForbiddenException extends PublicException {}


interface Debugable {
	public function __debug();
}

interface Jsonable {
	public function __toJson();
}
