<?php

namespace app\lib;


use \Debugable;


class Session implements Debugable {

	public function __construct(string $name = '') {
		if ($name) {
			session_name($name);
		}
		session_start();
	}

	public function __get(string $name) {
		if (isset($_SESSION[$name])) {
			return $_SESSION[$name];
		}
		return null;
	}

	public function __set(string $name, $value) {
		$_SESSION[$name] = $value;
	}

	public function __debug() {
		return $_SESSION;
	}

}