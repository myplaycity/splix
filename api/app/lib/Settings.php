<?php

namespace app\lib;


class Settings {

	public $loadedFiles = [];
	private $__settings = [];

	public function __construct(string $settingsPath) {
		$this -> load($settingsPath);
	}

	public function load(string $settingsPath) {
		$loaded = require_once(DOCUMENT_ROOT . $settingsPath);
		$this -> __settings = array_merge($this -> __settings, $loaded);
		$this -> loadedFiles[] = $settingsPath;
	}

	public function __get(string $name) {
		if (isset($this -> __settings[$name])) {
			return $this -> __settings[$name];
		}
		return null;
	}

}