<?php

namespace app\lib;


use \Iterator;
use \ArrayAccess;
use \Countable;
use \Debugable;


/*

	Collection class replaces standart array
	to simplify data-objects manipulating

*/
class Collection implements Iterator, ArrayAccess, Countable, Debugable {

	private $container = [];

	/**
	 * Creates Collection from input array
	 */
	public function __construct(array $items = []) {
		$this -> container = $items;
	}

	/**
	 * Returns container array of collection
	 */
	public function toArray(): array {
		return $this -> container;
	}

	/**
	 * Returns collection's keys as new instance of collection
	 */
	public function keys(): Collection {
		return new static(array_keys($this -> container));
	}

	/**
	 * Returns collection's values as new instance of collection
	 */
	public function values(): Collection {
		return new static(array_values($this -> container));
	}

	/**
	 * Returns if collection's container is associative array
	 */
	public function isAssoc(): bool {
	    return array_keys($this -> container) !== range(0, count($this -> container) - 1);
	}

	/**
	 * Push value to collection
	 */
	public function push($mixedVar): Collection {
		array_push($this -> container, $mixedVar);
		return $this;
	}

	/**
	 * Pop value from collection
	 */
	public function pop() {
		if (count($this -> container) > 0) {
			return array_pop($this -> container);
		}
		return null;
	}

	/**
	 * Extract first value from collection, and return it
	 */
	public function shift() {
		if (count($this -> container) > 0) {
			return array_shift($this -> container);
		}
		return null;
	}

	/**
	 * Insert value to collection on index position.
	 * Returns self.
	 */
	public function insert($index = 0, $mixedValue = null): Collection {
		if (is_int($index)) {
			array_splice($this -> container, $index, 0, $mixedValue);
		} else {
			$this -> container[$index] = $mixedValue;
		}
		return $this;
	}

	/**
	 * Remove value with index position from collection.
	 * Returns self.
	 */
	public function remove($index = 0): Collection {
		if (is_int($index)) {
			array_splice($this -> container, $index, 1);
		} elseif (isset($this -> container[$index])) {
			unset($this -> container[$index]);
		}
		return $this;
	}

	/**
	 * Append values to end of collection.
	 * Returns self
	 */
	public function append($values = null): Collection {
		if ($values instanceof Collection) {
		 	$this -> container = array_merge($this -> container, $values -> toArray());
		} else if (is_array($values)) {
			$this -> container = array_merge($this -> container, $values);
		}
		return $this;
	}

	/**
	 * Map collection with user-function.
	 * Returns new collection instance.
	 */
	public function map(callable $callback): Collection {
		return new static(array_map($callback, $this -> container));
	}

	/**
	 * Walk collection with user-function.
	 * Returns self.
	 */
	public function walk(callable $callback): Collection {
		array_walk($this -> container, $callback);
		return $this;
	}

	/**
	 * Sort collection with user-function.
	 * Returns self.
	 */
	public function sort(callable $criteria): Collection {
		if ($this -> isAssoc()) {
			uasort($this -> container, $criteria);
		} else {
			usort($this -> container, $criteria);
		}
		return $this;
	}

	/**
	 * Slice collection, return new collection.
	 */
	public function slice(int $offset = 0, int $length = 0): Collection {
		return new static(array_slice($this -> container, $offset, $length));
	}

	/**
	 * Make chunks from collection, and return it as new collection
	 */
	public function chunk(int $size): Collection {
		$chunks = [];
		foreach (array_chunk($this -> container, $size, true) as $chunk) {
			$chunks[] = new static($chunk);
		}
		return new static($chunks);
	}

	/**
	 * Group collection items by user-function.
	 * Returns groupped items as new collection.
	 */
	public function group(callable $callback): Collection {
		$groups = [];

		foreach ($this -> container as $i => $item) {
			$group = $callback($item);
			if (!array_key_exists($group, $groups)) {
				$groups[$group] = new self;
			}
			$groups[$group][] = $item;
		}

		return new static($groups);
	}

	/**
	 * Returns new Collection with items in revers order
	 */
	public function reverse(): Collection {
		return new static(array_reverse($this -> container));
	}

	// Interfaces realisation

	public function count(): int {
		return count($this -> container);
	}

	public function rewind() {
		return reset($this -> container);
	}

	public function current() {
		return current($this -> container);
	}

	public function key() {
		return key($this -> container);
	}

	public function next() {
		return next($this -> container);
	}

	public function valid() {
		return key($this -> container) !== null;
	}

	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this -> container[] = $value;
		} else {
			$this -> container[$offset] = $value;
		}
	}

	public function offsetExists($offset) {
		return isset($this -> container[$offset]);
	}

	public function offsetUnset($offset) {
		unset($this -> container[$offset]);
	}

	public function offsetGet($offset) {
		return isset($this -> container[$offset]) ? $this -> container[$offset] : null;
	}

	// implementation of Debugable

	public function __debug() {
		return $this -> toArray();
	}

}