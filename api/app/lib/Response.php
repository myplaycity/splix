<?php

namespace app\lib;


use \Jsonable;
use \BadResponseException;


class Response {

	public $content = null;

	public function set($content, int $status = 200) {
		$this -> content = $content;
		if ($status !== 200) {
			$this -> status($status);
		}
	}

	public function header(string $name, string $value = '') {
		$content = $value ? $name .= ': ' . $value : $name;
		header($content);
	}

	public function redirect(string $to) {
		$this -> header('HTTP/1.1 301 Moved Permanently');
		$this -> header('Location', $to);
		exit;
	}

	public function status($status = 200) {
		if ($status == 400) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
		} elseif ($status == 403) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
		} elseif ($status == 404) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
		} elseif ($status == 500) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
		}
	}

	public function give() {
		$content = $this -> content;

		if (is_string($content)) {
			echo $content;

		} elseif (is_array($content)) {
			$this -> header('Content-Type', 'application/json');
			echo json_encode($content);

		} elseif ($content instanceof Jsonable) {
			$this -> header('Content-Type', 'application/json');
			echo $content -> __toJson();

		} else if ($content !== null) {
			throw new BadResponseException("Cannot output resposne: " . strvar($content));
		}
		flush();
	}

}
