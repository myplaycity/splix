<?php

namespace app\lib;


use \mysqli;
use \MysqlException;


class Mysql {

	private $mysqli;
	private $host;
	private $user;
	private $password;
	private $dbname;

	public $prefix;
	public $lastError = '';
	public $queries = null;

	public $table = '';
	public $where = '';
	public $fields = '*';
	public $order = '';
	public $limit = '';

	public function __construct(array $connectionSettings) {
		$this -> host = $connectionSettings['host'];
		$this -> user = $connectionSettings['user'];
		$this -> password = $connectionSettings['password'];
		$this -> dbname = $connectionSettings['dbname'];
		$this -> prefix = $connectionSettings['prefix'];
		$this -> mysqli = null;
		$this -> queries = [];
	}

	public function lazyConnect() {
		if (!$this -> mysqli) {
			$this -> mysqli = @new mysqli($this -> host, $this -> user, $this -> password, $this -> dbname);
			if ($this -> mysqli -> connect_error !== null) {
				throw new MysqlException('Fail connect to mysql: '. $this -> mysqli -> connect_error);
			}
		}
	}

	public function query(string $sql) {
		$this -> lazyConnect();
		$this -> queries[] = $sql;
		$result = $this -> mysqli -> query($sql);

		if ($this -> mysqli -> error !== '') {
			$this -> lastError = $this -> mysqli -> error;
			throw new MysqlException('Mysql query error: "' . $this -> mysqli -> error . '", sql was: "' . $sql . '"');
		}

		return $result;
	}

	public function escape(string $str) {
		$this -> lazyConnect();
		return $this -> mysqli -> escape_string($str);
	}

	public function escapeField(string $name) {
		$this -> lazyConnect();
		$name = $this -> mysqli -> escape_string($name);
		if (preg_match('/^[a-zA-Z0-9_]+$/', $name))
			$name = "`$name`";
		return $name;
	}

	public function insertId() {
		if (!$this -> mysqli) {
			throw new MysqlException('Can not get insertId with no connection to Mysql.');
		}
		return $this -> mysqli -> insert_id;
	}

	private function composeFields(array $fields): string {
		if (count($fields) == 0)
			return '*';

		$result = array();
		foreach ($fields as $i => $name) {
			if (is_numeric($i)) {
				$field = $this -> escapeField($name);
				$result[] = $field;
			} else {
				$field = $this -> escapeField($i);
				$result[] = $field . ' AS `' .  $this -> escape($name) . '`';
			}
		}
		return implode(', ', $result);
	}

	private function composeOrder(array $order): string {
		if (count($order) == 0)
			return '';

		$result = array();
		foreach ($order as $name => $dir) {
			if (is_int($name)) {
				$name = $dir;
				$dir = ' ASC';
			} else {
				$name = $this -> escape($name);
				$dir = $dir > 0 ? ' ASC' : ' DESC';
			}
			$name = $this -> escapeField($name);
			$result[] = $name . $dir;
		}
		return 'ORDER BY ' . implode(', ', $result);
	}

	private function composeLimits($limit = 0, $skip = 0): string {
		if ($skip > 0 && $limit > 0) {
			return "LIMIT $skip, $limit";
		} elseif ($limit > 0) {
			return "LIMIT $limit";
		}
		return '';
	}

	private function composeWhere(array $where = null): string {
		if (!$where)
			return '';

		$result = [];

		foreach ($where as $key => $value) {

			// 'field ' => [1, 2, 3]
			if (is_array($value) and count($value) > 0) {
				$tmp = [];
				foreach ($value as $v) {
					$tmp[] = "'" . $this -> escape($v) . "'";
				}
				$values = "'" . implode("', '", $value) . "'";
				$key = $this -> escapeField($key);
				$result[] = "$key IN ($values)";

			// 'field LIKE' => '%somevalue%'
			} elseif (preg_match('/^(\w+) LIKE$/ui', $key, $m)) {
				$key = $m[1];
				$key = $this -> escapeField($key);
				$value = $this -> escape($value);
				$result[] = "$key LIKE ('$value')";

			// 'field' => null
			} elseif ($value === null) {
				$key = $this -> escapeField($key);
				$result[] = "$key IS NULL";

			// 'field' => 'some value'
			} else {
				$key = $this -> escapeField($key);
				$value = $this -> escape($value);
				$result[] = "$key='$value'";
			}
		}

		return 'WHERE ' . implode(' AND ', $result);
	}

	private function composeSet(array $fields): string {
		if (isset($fields['id'])) {
			unset($fields['id']);
		}

		$result = [];

		foreach ($fields as $field => $value) {
			if (!preg_match('/^[a-zA-Z_][a-zA-Z0-9\._]*$/', $field))
				throw new MysqlException('wrong field name to compose MYSQL SET: ' . strvar($field));

			$field = $this -> escapeField($field);

			if ($value === null) {
				$result[] = "$field=NULL";

			} elseif ($value === '') {
				$result[] = "$field=''";

			} elseif (is_string($value) || is_numeric($value)) {
				$value = $this -> escape($value);
				$result[] = "$field='$value'";

			} else {
				throw new MysqlException('wrong value to compose MYSQL SET: ' . strvar($value));
			}
		}
		return $result ? 'SET '. implode(', ', $result) : '';
	}

	public function datetime($time = 0): string {
		if ($time && !is_int($time)) {
			$time = strtotime($time);
		}
		return date('Y-m-d H:i:s', $time ? $time : time());
	}

	public function date($time = 0): string {
		if ($time && !is_int($time)) {
			$time = strtotime($time);
		}
		return date("Y-m-d", $time ? $time : time());
	}

	public function reset() {
		$this -> table = '';
		$this -> where = '';
		$this -> fields = '*';
		$this -> order = '';
		$this -> limit = '';
	}

	public function table(string $tableName) {
		$this -> table = $this -> prefix . $tableName;
		return $this;
	}

	public function where(array $criteria = null) {
		$this -> where = $this -> composeWhere($criteria);
		return $this;
	}

	public function fields(array $fields) {
		$this -> fields = $this -> composeFields($fields);
		return $this;
	}

	public function order(array $order) {
		$this -> order = $this -> composeOrder($order);
		return $this;
	}

	public function limit(int $skip, int $limit) {
		$this -> limit = '';
		if ($skip > 0 && $limit > 0) {
			$this -> limit = "LIMIT $skip, $limit";
		} elseif ($limit > 0) {
			$this -> limit = "LIMIT $limit";
		}
		return $this;
	}

	public function delete(string $tableName) {
		$this -> table($tableName);
		$query = "DELETE FROM `{$this->table}` {$this->where}";
		$this -> reset();
		return $this -> query($query);
	}

	public function update(string $tableName, array $fields) {
		$this -> table($tableName);
		$set = $this -> composeSet($fields);
		$query = "UPDATE `{$this->table}` {$set} {$this->where}";
		$this -> reset();
		return $this -> query($query);
	}

	public function insert(string $tableName, array $fields) {
		$this -> table($tableName);
		$set = $this -> composeSet($fields);
		$query = "INSERT INTO `{$this->table}` {$set}";
		$this -> reset();
		return $this -> query($query);
	}

	public function selectRow(string $tableName) {
		$this -> table($tableName);
		$query = "SELECT {$this->fields} FROM `{$this->table}` {$this->where} {$this->order} LIMIT 1";
		$this -> reset();
		$result = $this -> query($query);

		if ($result -> num_rows > 0) {
			$row = $result -> fetch_assoc();
			$result -> free();
			return $row;
		}
		return null;
	}

	public function selectRows(string $tableName) {
		$this -> table($tableName);
		$query = "SELECT {$this->fields} FROM `{$this->table}` {$this->where} {$this->order} {$this->limit}";
		$this -> reset();
		$result = $this -> query($query);

		if ($result -> num_rows > 0) {
			$list = [];
			while ($row = $result -> fetch_assoc()) {
				$list[] = $row;
			}
			$result -> free();
			return $list;
		}
		return null;
	}

	public function selectModel(string $tableName, string $modelClass) {
		if (!class_exists($modelClass))
			throw new MysqlException("model class '$modelClass' does not exists");

		$this -> table($tableName);
		$query = "SELECT {$this->fields} FROM `{$this->table}` {$this->where} {$this->order} LIMIT 1";
		$this -> reset();
		$result = $this -> query($query);

		if ($result -> num_rows > 0) {
			$model = new $modelClass($result -> fetch_assoc());
			$result -> free();
			return $model;
		}
		return null;
	}

	public function selectModels(string $tableName, string $modelClass) {
		if (!class_exists($modelClass))
			throw new MysqlException("model class '$modelClass' does not exists");

		$this -> table($tableName);
		$query = "SELECT {$this->fields} FROM `{$this->table}` {$this->where} {$this->order} {$this->limit}";
		$this -> reset();
		$result = $this -> query($query);

		if ($result -> num_rows > 0) {
			$list = [];
			while ($fields = $result -> fetch_assoc()) {
				$list[] = new $modelClass($fields);
			}
			return new Collection($list);
			$result -> free();
		}
		return null;
	}

	public function autocommit(bool $value) {
		$this -> lazyConnect();
		$this -> mysqli -> autocommit($value);
	}

	public function commit() {
		$this -> lazyConnect();
		$this -> mysqli -> commit();
	}

	public function rollback() {
		$this -> lazyConnect();
		$this -> mysqli -> rollback();
	}

	public function selectArray(string $query, string $keyName = 'id') {
		if ($result = $this -> query($query)) {
			$list = [];
			while ($row = $result -> fetch_assoc()) {
				if (isset($row[$keyName]) ) {
					$key = $row[$keyName];
					$list[$key] = $row;
				} else {
					$list[] = $row;
				}
			}
			$result -> free();
			return $list;
		}
		return null;
	}

	public function selectArrayOne(string $query) {
		if ($result = $this -> query($query)) {
			$row = $result -> fetch_assoc();
			$result -> free();
			return $row;
		}
		return null;
	}

	public function getFoundRows() {
		if ($rows = $this -> selectArrayOne('SELECT FOUND_ROWS() as `c`')) {
			return (int) $rows['c'];
		}
		return 0;
	}

}