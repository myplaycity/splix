<?php

namespace app\lib;


class Router {

	private $request;
	private $response;
	public $routePath = '';

	public function __construct(Request $request, Response $response) {
		$this -> request = $request;
		$this -> response = $response;
	}

	public function map(array $routes, $defaultCallback): bool {
		foreach ($routes as $path => $callback) {
			if ($path === $this -> request -> path) {
				$this -> processRoute($path, $callback);
				return true;
			}
		}
		$this -> default($defaultCallback);
		return false;
	}

	private function processRoute(string $path, callable $callback) {
		$this -> routePath = $path;
		$result = call_user_func($callback, $this -> request, $this -> response);
		if ($result !== null) {
			$this -> response -> set($result);
		}
	}

	private function default($callback) {
		call_user_func($callback, $this -> request, $this -> response);
	}

}