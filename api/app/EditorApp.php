<?php

namespace app;


use \app\lib\App;
use \app\lib\Logger;
use \app\lib\Settings;
use \app\lib\Request;
use \app\lib\Response;
use \app\lib\Router;
use \app\lib\Template;
use \Exception;
use \PublicException;


class EditorApp extends App {

	public $settings;
	public $router;
	public $template;
	public $serverType;
	public $configName;
	public $configData;

	public function start() {
		try {
			$this -> settings = new Settings("/app/settings/{$this->env}.php");
			$this -> router = new Router($this -> request, $this -> response);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail start application.", $e, $this);
		}
	}

	public function run() {
		try {

			$this -> router -> map([
				"/editor/edit" => [$this, 'edit'],
				"/editor/save" => [$this, 'save'],
			], function(Request $request, Response $response) {
				$response -> set('Page not found', 404);
			});

		} catch (PublicException $e) {
			$this -> response -> set(['error' => $e -> getMessage()]);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail execute request.", $e, $this);
		}
	}

	private function loadServerConfig(Request $request) {
		$serverType = $request -> serverType;

		if ($serverType === 'base') {
			$configName = 'local';
		} elseif ($serverType === 'original') {
			$configName = 'local-original';
		} elseif ($serverType === 'towers') {
			$configName = 'local-towers';
		} elseif ($serverType === 'mines') {
			$configName = 'local-mines';
		} elseif ($serverType === 'pvp') {
			$configName = 'local-pvp';
		} else {
			throw new PublicException("Unknown server type.");
		}

		$configText = file_get_contents("../gameserver/settings/$configName.ini");
		$this -> serverType = $serverType;
		$this -> configName = $configName;
		$this -> configData = parse_ini_string($configText);
	}

	private function saveRoomConfig(Request $request) {
		foreach ($this -> configData as $key => $value) {
			$postValue = $request -> post($key);
			if ($postValue !== null) {
				$this -> configData[$key] = $postValue;
			}
		}
		$this -> writeIniFile("../gameserver/settings/{$this->configName}.ini", $this -> configData);
		$this -> restartGameServers();
	}

	public function restartGameServers() {
		debugVars(shell_exec("/home/suharev/work/splix/bin/restart-rooms.sh"));
	}

	private function writeIniFile(string $fileName, array $data) {
		$content = '';
		foreach ($data as $key => $value) {
			$content .= $key . " = " . $value . "\n";
		}
		file_put_contents($fileName, $content);
	}

	public function edit(Request $request, Response $response) {
		$this -> loadServerConfig($request);
		$this -> template = new Template("/app/templates/editor.php");
		$this -> template -> import(['serverType' => $this -> serverType]);
		return $this -> template -> render($this -> configData);
	}

	public function save(Request $request, Response $response) {
		$this -> loadServerConfig($request);
		$this -> saveRoomConfig($request);
		$this -> template = new Template("/app/templates/editor.php");
		$this -> template -> import(['serverType' => $this -> serverType]);
		return $this -> template -> render($this -> configData);
	}

}
