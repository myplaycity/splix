<?php

namespace app\models;


use \BadRequestException;
use \app\lib\Request;
use \app\lib\MysqlModel;


class RecordModel extends MysqlModel {

	public $userId = 0;
	public $name = '';
	public $score = 0;
	public $capture = 0;
	public $kills = 0;
	public $tail = 0;
	public $timeAlive = 0;
	public $timeTop1 = 0;

	public static function table(): string {
		return 'records';
	}

	public static function describe(): array {
		return [
			'id',
			'userId',
			'name',
			'score',
			'capture',
			'kills',
			'tail',
			'timeAlive',
			'timeTop1',
		];
	}

	public static function findByUser(int $userId) {
		return self :: findOne(['userId' => $userId]);
	}

	public function updateWithRequest(Request $request) {
		$score = $request -> score;
		$name = $request -> name;
		$capture = $request -> capture;
		$kills = $request -> kills;
		$tail = $request -> tail;
		$timeAlive = $request -> timeAlive;
		$timeTop1 = $request -> timeTop1;

		if (!$name) throw new BadRequestException('Invalid user name to update record: ' . $name);

		$this -> name = $name;
		$this -> score = max($score, $this -> score);
		$this -> capture = max($capture, $this -> capture);
		$this -> kills = max($kills, $this -> kills);
		$this -> tail = max($tail, $this -> tail);
		$this -> timeAlive = max($timeAlive, $this -> timeAlive);
		$this -> timeTop1 = max($timeTop1, $this -> timeTop1);

		return $this -> save();
	}

	public static function createFromRequest(Request $request, int $userId) {
		$record = new self();
		$score = $request -> score;
		$name = $request -> name;

		if ($userId === 0) throw new BadRequestException('Invalid userId to create record: ' . $userId);
		if (!$name) throw new BadRequestException('Invalid user name to create record: "' . $name . '"');

		$record -> userId = $userId;
		$record -> name = $name;
		$record -> score = $score;
		$record -> capture = $request -> capture;
		$record -> kills = $request -> kills;
		$record -> tail = $request -> tail;
		$record -> timeAlive = $request -> timeAlive;
		$record -> timeTop1 = $request -> timeTop1;

		return $record -> save();
	}

	public static function getTop() {
		if ($founded = self :: find([], ['score' => -1], 20, 0)) {
			return $founded -> toArray();
		} else {
			return [];
		}
	}

}