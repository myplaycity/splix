<?php

namespace app\models;


use \app\lib\MysqlModel;


class UserModel extends MysqlModel {

	public $name = "";
	public $hash = "";

	public static function table(): string {
		return 'users';
	}

	public static function describe(): array {
		return [
			'id',
			'name',
			'hash'
		];
	}

	public function makeUniqueHash() {
		$this -> hash = md5(uniqid(rand(), true));
	}

	public static function findByHash(string $hash) {
		return self :: findOne(['hash' => $hash]);
	}

}