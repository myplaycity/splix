<?php

$settings = [
	"mysql" => [
		"host" => "localhost",
		"user" => "admin",
		"password" => "kv57rq10",
		"dbname" => "splix",
		"prefix" => ""
	],
	"memcache" => [
		"host" =>  "localhost",
		"port" =>  11211,
		"prefix" =>  "splix"
	],
	"serverKey" => "3utO3NECPt1yEyebvft0",
	"servers" => [
		"local.splix.io/info/room1",
		"local.splix.io/info/room2",
		"local.splix.io/info/room3",
		"local.splix.io/info/room4",
		"local.splix.io/info/room5"
	]
];

return $settings;