<?php

$settings = [
	"mysql" => [
		"host" => "localhost",
		"user" => "admin",
		"password" => "kv57rq10",
		"dbname" => "splix",
		"prefix" => ""
	],
	"memcache" => [
		"host" =>  "localhost",
		"port" =>  11211,
		"prefix" =>  "splix"
	],
	"serverKey" => "3utO3NECPt1yEyebvft0",
	"servers" => [
		"dev.splix.sliz.io/info/room1",
		"dev.splix.sliz.io/info/room2",
		"dev.splix.sliz.io/info/room3",
		"dev.splix.sliz.io/info/room4",
		"dev.splix.sliz.io/info/room5"
	]
];

return $settings;
