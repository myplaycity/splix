<!doctype html>
<html lang="ru-RU">
<head>
	<title>PLIX EDITOR</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<style>
		html, body {
			padding: 0;
			margin: 0;
		}
	</style>

</head>
<body>

	<h3>PLIX EDITOR</h3>

	<form action="/editor/save?serverType=<?=$this->serverType?>" method="post">
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>sizeX</td>
				<td><input class="text" type="text" name="sizeX" value="<?=$this->sizeX?>" /></td>
			</tr>
			<tr>
				<td>sizeY</td>
				<td><input class="text" type="text" name="sizeY" value="<?=$this->sizeY?>" /></td>
			</tr>
			<tr>
				<td>quadSize</td>
				<td><input class="text" type="text" name="quadSize" value="<?=$this->quadSize?>" /></td>
			</tr>
			<tr>
				<td>viewQuadPadding</td>
				<td><input class="text" type="text" name="viewQuadPadding" value="<?=$this->viewQuadPadding?>" /></td>
			</tr>
			<tr>
				<td>bots</td>
				<td><input class="text" type="text" name="bots" value="<?=$this->bots?>" /></td>
			</tr>
		</table>
		<input class="submit" type="submit" value="SAVE" />
	</form>

</body>
</html>