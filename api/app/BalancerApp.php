<?php

namespace app;


use \app\lib\App;
use \app\lib\Logger;
use \app\lib\Settings;
use \app\lib\Request;
use \app\lib\Response;
use \app\lib\Router;
use \app\lib\Memcache;
use \app\Servers;
use \Exception;
use \PublicException;


class BalancerApp extends App {

	// init static logging

	protected static $logger = null;

	public static function __init() {
		self :: $logger = new Logger(__CLASS__, Logger :: DEBUG);
	}

	// app

	public $settings;
	public $router;
	public $memcache;
	public $servers;

	public function start() {
		try {
			$this -> settings = new Settings("/app/settings/{$this->env}.php");
			$this -> router = new Router($this -> request, $this -> response);
			$this -> memcache = new Memcache($this -> settings -> memcache);
			$this -> servers = new Servers($this -> memcache, $this -> settings -> servers);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail start application.", $e, $this);
		}
	}

	public function run() {
		try {

			$this -> router -> map([
				"/balancer/bestServer" => [$this, 'bestServer'],
				"/balancer/serverList" => [$this, 'serverList'],
				"/balancer/updateServers" => [$this, 'updateServers'],
			], function(Request $request, Response $response) {
				$response -> set('Page not found', 404);
			});

		} catch (PublicException $e) {
			$this -> response -> set(['error' => $e -> getMessage()]);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail execute request.", $e, $this);
		}
	}

	public function bestServer(Request $request, Response $response) {
		$serverType = $request -> serverType;
		if (!$serverType)
			throw new PublicException("Undefined serverType.");

		return $this -> servers -> getBest($serverType);
	}

	public function serverList(Request $request, Response $response) {
		return $this -> servers -> getList();
	}

	public function updateServers() {
		return $this -> servers -> update();
	}

}
