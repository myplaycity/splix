<?php

namespace app;


use \app\lib\App;
use \app\lib\Logger;
use \app\lib\Settings;
use \app\lib\Request;
use \app\lib\Response;
use \app\lib\Router;
use \app\lib\Memcache;
use \app\lib\Mysql;
use \app\lib\MysqlModel;
use \app\models\RecordModel;
use \app\models\UserModel;
use \Exception;
use \PublicException;


class ApiApp extends App {

	// init static logging

	protected static $logger = null;

	public static function __init() {
		self :: $logger = new Logger(__CLASS__, Logger :: DEBUG);
	}

	// app

	public $settings;
	public $router;
	public $mysql;

	const MD5_REGEX_PATTERN = "/^[a-f0-9]{32}$/i";

	public function start() {
		try {
			$this -> settings = new Settings("/app/settings/{$this->env}.php");
			$this -> router = new Router($this -> request, $this -> response);
			$this -> mysql = new Mysql($this -> settings -> mysql);
			MysqlModel :: init($this -> mysql);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail start application.", $e, $this);
		}
	}

	public function run() {
		try {

			$this -> router -> map([
				"/api/auth" => [$this, 'auth'],
				"/api/saveRound" => [$this, 'saveRound'],
				"/api/records" => [$this, 'records'],
			], function(Request $request, Response $response) {
				$response -> set('Page not found', 404);
			});

		} catch (PublicException $e) {
			$this -> response -> set(['error' => $e -> getMessage()]);
		} catch (Exception $e) {
			$this -> response -> set(['error' => 'Internal server error.'], 500);
			self :: $logger -> error("Fail execute request.", $e, $this);
		}
	}

	public function auth(Request $request, Response $response) {
		$userHash = $request -> userHash;
		if ($userHash && !preg_match(self :: MD5_REGEX_PATTERN, $userHash))
			throw new PublicException("Invalid userHash.");

		if (!$userHash) {
			$newUser = new UserModel();
			$newUser -> makeUniqueHash();
			$newUser -> save();
			return ['user' => $newUser];
		}

		if ($foundUser = UserModel :: findByHash($userHash)) {
			return ['user' => $foundUser];
		} else {
			throw new PublicException("Invalid userHash.");
		}
	}

	public function saveRound(Request $request, Response $response) {
		$this -> checkGameserverKey($request);

		$userHash = $request -> userHash;
		if ($userHash && !preg_match(self :: MD5_REGEX_PATTERN, $userHash))
			throw new PublicException("Invalid userHash.");

		if (!$user = UserModel :: findByHash($userHash))
			throw new PublicException("User not exist.");

		if ($record = RecordModel :: findByUser($user -> id)) {
			$record -> updateWithRequest($request);
			return ['status' => 'updated', 'record' => $record];
		} else {
			$record = RecordModel :: createFromRequest($request, $user -> id);
			return ['status' => 'created', 'record' => $record];
		}
	}

	public function records(Request $request, Response $response) {
		return RecordModel :: getTop();
	}

	private function checkGameserverKey(Request $request) {
		if ($request -> serverKey !== $this -> settings -> serverKey) {
			throw new PublicException("Wrong server auth key.");
		}
	}

	public function end() {
		parent :: end();
		self :: $logger -> log("response in " . number_format($this -> responseTimeMs, 2) . 'ms');
	}

}
