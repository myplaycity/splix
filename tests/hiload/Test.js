
global.rootRequire = function(name) {
    return require(__dirname + "/" + name);
}

var HiloadTest = rootRequire("src/HiloadTest");

var test = new HiloadTest();
test.start();
