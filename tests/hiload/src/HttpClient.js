
"use strict";

var http = require("http");
var Class = rootRequire("src/Class");


function HttpClient(host) {
	this.host = host;
}


Class(HttpClient).implements({

	get: function(path, onSuccess, onFail) {
		var options = {
			host: this.host,
			path: path
		};
		http.request(options, (response) => {
			var str = "";
			response.on("data", (chunk) => {
				str += chunk;
			});
			response.on("end", () => {
				onSuccess(str);
			});
			response.on("error", (err) => {
				console.error(err);
				onFail();
			});
		}).end();
	}

});


module.exports = HttpClient;
