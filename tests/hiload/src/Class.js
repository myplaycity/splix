
"use strict";


/**
 * Prepare function for being class constructor
 * Attach utils Extends & Implements functions to function object
 *
 * @param Function constructor
 */
function Class(constructor) {
	constructor.prototype = {};
	constructor.prototype.constructor = constructor;

	// import special class functions
	constructor.extends = Class.extends.bind(constructor);
	constructor.implements = Class.implements.bind(constructor);
	return constructor;
}


/**
 * Make this function proto-inheritance from parent
 *
 * @param Function parent
 */
Class.extends = function(parent) {
	this.prototype = Object.create(parent.prototype);
	this.prototype.constructor = parent;
	this.prototype.super = parent.prototype;
	return this;
};


/**
 * Import properties from object to this prototype
 *
 * @param Object protoObject
 */
Class.implements = function(protoObject) {
	for (var key in protoObject) {
		if (protoObject.hasOwnProperty(key)) {
			this.prototype[key] = protoObject[key];
		}
	}
	return this;
};


module.exports = Class;
