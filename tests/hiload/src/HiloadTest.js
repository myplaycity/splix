
"use strict";


var Class = rootRequire("src/Class");
var HiloadPlayer = rootRequire("src/HiloadPlayer");


function HiloadTest() {
	this.maxPlayers = 200;
	this.players = [];
	this.maxTicks = 1000;
	this.ticks = 0;
	this.playerIndex = 1;

	this.fps = 6;
	this.tickDelta = 1000 / this.fps;

	this.tick = this.tick.bind(this);
}


Class(HiloadTest).implements({

	start: function() {
		setTimeout(this.tick, this.tickDelta);
	},

	tick: function() {
		this.ticks++;
		this.addPlayers();
		this.tickPlayers();

		if (this.ticks % this.fps === 0) {
			this.showStatus();
		}

		if (this.ticks < this.maxTicks) {
			setTimeout(this.tick, this.tickDelta);
		} else {
			this.stop();
		}
	},

	addPlayers: function() {
		if (this.players.length < this.maxPlayers) {
			var delta = Math.min(this.maxPlayers - this.players.length, 4);

			for (var i = 0; i < delta; i++) {
				this.players.push(new HiloadPlayer(this.playerIndex, this));
				this.playerIndex += 1;
			}
		}
	},

	removePlayer: function(player) {
		var index = this.players.indexOf(player);
		if (index > -1) this.players.splice(index, 1);
	},

	tickPlayers: function() {
		for (var i = this.players.length - 1; i >= 0; i--) {
			this.players[i].tick(this.ticks);
		}
	},

	showStatus: function() {
		var ticks = this.ticks + " / " + this.maxTicks + " ticks";
		var players = this.players.length + " / " + this.maxPlayers + " players";
		console.log(ticks + " " + players);
	},

	stop: function() {
		for (var i = this.players.length - 1; i >= 0; i--) {
			this.players[i].stop();
		}
		this.players.length = 0;
	}

});


module.exports = HiloadTest;