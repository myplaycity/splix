
"use strict";


var WebSocket = require("ws");
var Class = rootRequire("src/Class");
var HttpClient = rootRequire("src/HttpClient");


function HiloadPlayer(id, test) {
	this.test = test;
	this.id = id;
	this.name = "player" + id;
	this.moveTicks = randInt(5, 10);
	this.hash = "";
	this.server = "";
	this.httpClient = new HttpClient("dev.splix.sliz.io");

	this.authApi();
};


HiloadPlayer.moves = {
	1: (new Int8Array(1)).fill(1).buffer,
	2: (new Int8Array(1)).fill(2).buffer,
	3: (new Int8Array(1)).fill(3).buffer,
	4: (new Int8Array(1)).fill(4).buffer
};


Class(HiloadPlayer).implements({

	authApi: function() {
		this.httpClient.get("/api/auth", (response) => {
			var parsed = JSON.parse(response);
			this.hash = parsed.user.hash;
			this.getBestServer();
		});
	},

	getBestServer: function() {
		this.httpClient.get("/balancer/bestServer", (response) => {
			var parsed = JSON.parse(response);
			this.server = parsed.ws;
			this.startWebSocket();
		});
	},

	startWebSocket: function() {
		this.ws = new WebSocket(this.server);
		this.ws.on("open", this.onOpen.bind(this));
		this.ws.on("close", this.onClose.bind(this));
		this.ws.on("error", this.onError.bind(this));
	},

	onOpen: function() {
		var userInfoMessage = this.makeUserInfo(this.name, this.hash);
		this.send(userInfoMessage);
	},

	makeUserInfo: function(userName, userHash) {
		var nameLen = userName.length;
		var buffer = new ArrayBuffer(1 + 4 + 1 + nameLen * 2 + 1 + 32 * 2);
		var view = new DataView(buffer);

		view.setInt8(0, 5);
		view.setInt8(1, nameLen);

		var start = 2;
		for (var i = 0; i < nameLen; i++) {
			view.setUint16(start, userName.charCodeAt(i) || 0);
			start += 2;
		}
		for (var i = 0; i < 32; i++) {
			view.setUint16(start, userHash.charCodeAt(i) || 0);
			start += 2;
		}
		return buffer;
	},

	onClose: function() {
		this.test.removePlayer(this);
	},

	onError: function() {
		console.error("web socket error!");
		this.test.removePlayer(this);
	},

	stop: function() {
		this.ws.close();
	},

	tick: function(ticks) {
		this.moveTicks -= 1;
		if (this.moveTicks === 0) {
			this.sendMove();
		}
	},

	sendMove: function() {
		var randMove = randInt(1, 4);
		this.send(HiloadPlayer.moves[randMove])
		this.moveTicks = randInt(5, 12);
	},

	send: function (msg) {
		if (this.ws && this.ws.readyState == WebSocket.OPEN) {
			this.ws.send(msg, { binary: true, mask: true });
		}
	}

});


function randInt(a, c) {
	return Math.floor(Math.random() * (a - c + 1)) + c;
}


module.exports = HiloadPlayer;